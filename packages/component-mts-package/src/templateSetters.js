const convert = require('xml-js')

const { get, set } = require('lodash')

module.exports = {
  setMetadata: ({ meta, jsonTemplate, options, fileName }) => {
    const section = get(meta, 'section') || get(meta, 'specialIssue.section')
    if (section) {
      const sectionTemplate = {
        'subject-area': {
          _text: section.name,
        },
      }
      set(jsonTemplate, 'article.front.article-meta', sectionTemplate)
    }

    if (meta.specialIssue) {
      const specialIssueCustomId = {
        _text: meta.specialIssue.customId,
      }
      const specialIssueTitle = {
        _text: meta.specialIssue.name,
      }
      set(
        jsonTemplate,
        'article.front.article-meta.special-issue-id',
        specialIssueCustomId,
      )
      set(
        jsonTemplate,
        'article.front.article-meta.special-issue-title',
        specialIssueTitle,
      )
    }

    set(
      jsonTemplate,
      'article.front.article-meta.abstract._text',
      meta.abstract,
    )
    set(
      jsonTemplate,
      'article.front.article-meta.article-version._text',
      meta.version,
    )

    const titleGroup = {
      'article-title': {
        _text: parseHtml(meta.title, options),
      },
    }
    set(jsonTemplate, 'article.front.article-meta.title-group', titleGroup)

    const articleId = [
      {
        _attributes: {
          'pub-id-type': 'publisher-id',
        },
        _text: fileName,
      },
      {
        _attributes: {
          'pub-id-type': 'manuscript',
        },
        _text: fileName,
      },
    ]
    set(jsonTemplate, 'article.front.article-meta.article-id', articleId)

    const articleType = {
      'subj-group': [
        {
          _attributes: {
            'subj-group-type': 'heading',
          },
          subject: {
            _text: meta.articleType.name,
          },
        },
      ],
    }
    set(
      jsonTemplate,
      'article.front.article-meta.article-categories',
      articleType,
    )

    const date = new Date()
    const pubDate = {
      _attributes: {
        'pub-type': 'publication-year',
      },
      year: {
        _text: date.getFullYear(),
      },
    }

    set(jsonTemplate, 'article.front.article-meta.pub-date', pubDate)

    return jsonTemplate
  },
  setHistory: (submitted, finalRevision, status, jsonTemplate) => {
    const date = new Date(submitted)
    const finalRevisionDate = new Date(finalRevision)
    let dates

    const received = [
      {
        _attributes: {
          'date-type': 'received',
        },
        day: {
          _text: date.getDate(),
        },
        month: {
          _text: date.getMonth() + 1,
        },
        year: {
          _text: date.getFullYear(),
        },
      },
    ]
    const productionDates = [
      {
        _attributes: {
          'date-type': 'rev-recd',
        },
        day: {
          _text: finalRevisionDate.getDate(),
        },
        month: {
          _text: finalRevisionDate.getMonth() + 1,
        },
        year: {
          _text: finalRevisionDate.getFullYear(),
        },
      },
      {
        _attributes: {
          'date-type': 'accepted',
        },
        day: {
          _text: date.getDate(),
        },
        month: {
          _text: date.getMonth() + 1,
        },
        year: {
          _text: date.getFullYear(),
        },
      },
    ]

    if (status === 'published') {
      dates = [...received, ...productionDates]
      set(jsonTemplate, 'article.front.article-meta.history.date', dates)
    } else
      set(jsonTemplate, 'article.front.article-meta.history.date', received)

    return jsonTemplate
  },
  setCounts: jsonTemplate => {
    const figCount = {
      _attributes: {
        count: '000',
      },
    }
    set(jsonTemplate, 'article.front.article-meta.counts.fig-count', figCount)
    const refCount = {
      _attributes: {
        count: '000',
      },
    }
    set(jsonTemplate, 'article.front.article-meta.counts.ref-count', refCount)

    const pageCount = {
      _attributes: {
        count: '000',
      },
    }
    set(jsonTemplate, 'article.front.article-meta.counts.page-count', pageCount)
    return jsonTemplate
  },
  setFiles: (files, jsonTemplate) => {
    const jsonFiles = files.map(file => ({
      item_type: {
        _text: file.type,
      },
      item_description: {
        _text: file.originalName,
      },
      item_name: {
        _text: file.fileName,
      },
    }))

    set(jsonTemplate, 'article.front.files.file', jsonFiles)

    return jsonTemplate
  },
  setContacts: (journalTeams, screeners, jsonTemplate) => {
    let eaMembers
    let eaTeam
    if (journalTeams) {
      eaTeam = journalTeams.find(team => team.role === 'editorialAssistant')
    }
    if (eaTeam) {
      eaMembers = eaTeam.members.map(member => ({ ...member }))
    }

    screeners = (screeners || []).map(e => ({
      alias: {
        surname: e.surname,
        email: e.email,
        givenNames: e.givenNames,
      },
      team: {
        role: e.role,
      },
    }))
    const members = [...eaMembers, ...screeners]

    const contacts = members.map(member => {
      const roles = {
        editorialAssistant: member.isCorresponding ? 'EALeader' : 'EA',
        esLeader: 'ESLeader',
        es: 'ES',
        eaLeader: 'EALeader',
        mcLeader: 'MCLeader',
        eqa: 'EQA',
        qc: 'QC',
        qcLeader: 'QCLeader',
      }
      const role = roles[member.team.role]

      return {
        _attributes: { role },
        name: {
          surname: {
            _text: member.alias.surname,
          },
          'given-names': {
            _text: member.alias.givenNames,
          },
        },
        email: {
          _text: member.alias.email,
        },
      }
    })
    set(jsonTemplate, 'article.front.contacts.contact-person', contacts)

    return jsonTemplate
  },
  setDeclarations: (
    { conflictOfInterest, dataAvailability, fundingStatement },
    jsonTemplate,
  ) => {
    const questions = [
      {
        _attributes: {
          type: 'COI',
        },
        answer: {
          _text: conflictOfInterest ? 'Yes' : 'No',
        },
        statement: {
          _text:
            conflictOfInterest ||
            'The authors for this paper did not provide a conflict of interest statement',
        },
      },
      {
        _attributes: {
          type: 'DA',
        },
        answer: {
          _text: dataAvailability ? 'Yes' : 'No',
        },
        statement: {
          _text:
            dataAvailability ||
            'The authors for this paper did not provide a data availability statement',
        },
      },
      {
        _attributes: {
          type: 'Fund',
        },
        answer: {
          _text: fundingStatement ? 'Yes' : 'No',
        },
        statement: {
          _text:
            fundingStatement ||
            'The authors for this paper did not provide a funding statement',
        },
      },
    ]

    set(jsonTemplate, 'article.front.questions.question', questions)
    return jsonTemplate
  },
  setContributors: (teams, jsonTemplate) => {
    const authorTeam = teams.find(t => t.role === 'author')
    const authors = authorTeam.members.map(member => ({
      ...member,
      contribType: 'Author',
    }))

    const contributors = authors

    const academicEditorTeam = teams.find(t => t.role === 'academicEditor')
    if (academicEditorTeam) {
      const academicEditor = academicEditorTeam.members.find(member =>
        ['accepted', 'submitted'].includes(member.status),
      )
      if (academicEditor) {
        academicEditor.contribType = 'Academic Editor'
        contributors.push(academicEditor)
      }
    }
    const contrib = contributors.map((contributor, i) => {
      let orcidIdentity
      let contribId

      if (contributor.user) {
        orcidIdentity = contributor.user.identities.filter(
          identity => identity.type === 'orcid',
        )
      }
      if (orcidIdentity && orcidIdentity.length > 0) {
        contribId = {
          _attributes: {
            'contrib-id-type': orcidIdentity[0].type,
          },
          _text: `https://orcid.org/${orcidIdentity[0].identifier}`,
        }
      }

      const _attributes = {
        'contrib-type': contributor.contribType,
      }
      if (contributor.contribType === 'Author') {
        _attributes.corresp = contributor.isCorresponding ? 'Yes' : 'No'
        _attributes.submitting = contributor.isSubmitting ? 'Yes' : 'No'
      }
      if (contribId) {
        return {
          _attributes,
          'contrib-id': contribId,
          role: {
            _attributes: {
              'content-type': '1',
            },
          },
          name: {
            surname: {
              _text: contributor.alias.surname,
            },
            'given-names': {
              _text: contributor.alias.givenNames,
            },
            prefix: {
              _text: contributor.alias.title || 'Dr.',
            },
          },
          email: {
            _text: contributor.alias.email,
          },
          xref: {
            _attributes: {
              'ref-type': 'aff',
              rid: `I${i + 1}`,
            },
            sup: {
              _text: i + 1,
            },
          },
        }
      }
      return {
        _attributes,
        role: {
          _attributes: {
            'content-type': '1',
          },
        },
        name: {
          surname: {
            _text: contributor.alias.surname,
          },
          'given-names': {
            _text: contributor.alias.givenNames,
          },
          prefix: {
            _text: contributor.alias.title || 'Dr.',
          },
        },
        email: {
          _text: contributor.alias.email,
        },
        xref: {
          _attributes: {
            'ref-type': 'aff',
            rid: `I${i + 1}`,
          },
          sup: {
            _text: i + 1,
          },
        },
      }
    })
    const aff = contributors.map((contributor, i) => ({
      _attributes: {
        id: `I${i + 1}`,
      },
      sup: {
        _text: i + 1,
      },
      'addr-line': {
        _text: contributor.alias.aff || '',
      },
      country: contributor.alias.country || 'UK',
    }))

    set(
      jsonTemplate,
      'article.front.article-meta.contrib-group.contrib',
      contrib,
    )
    set(jsonTemplate, 'article.front.article-meta.aff', aff)

    return jsonTemplate
  },
  setEmptyReviewers: jsonTemplate => {
    set(jsonTemplate, 'article.front.rev-group', '')
    return jsonTemplate
  },
  setReviewers: ({ reviews = [], jsonTemplate, authorsLength = 0 }) => {
    const xmlReviewers = reviews.map((review, i) => {
      const assignmentDate = new Date(review.member.responded)
      const submissionDate = new Date(review.submitted)

      const mtsRoles = {
        author: 'author',
        reviewer: 'reviewer',
        academicEditor: 'editor',
        triageEditor: 'triageEditor',
      }
      const revType = mtsRoles[review.member.team.role]

      const revObj = {
        _attributes: {
          'rev-type': revType,
        },
        name: {
          surname: {
            _text: review.member.alias.surname,
          },
          'given-names': {
            _text: review.member.alias.givenNames,
          },
          prefix: {
            _text: review.member.alias.title || 'Dr.',
          },
        },
        email: {
          _text: review.member.alias.email,
        },
        xref: {
          _attributes: {
            'ref-type': 'aff',
            rid: `I${authorsLength + i + 1}`,
          },
          sup: {
            _text: i + 1,
          },
        },
        date: [
          {
            _attributes: {
              'date-type': 'assignment',
            },
            day: {
              _text: assignmentDate.getDate(),
            },
            month: {
              _text: assignmentDate.getMonth() + 1,
            },
            year: {
              _text: assignmentDate.getFullYear(),
            },
          },
        ],
        files: { file: [] },
        recommendation: review.recommendation || '',
      }

      revObj.date.push({
        _attributes: {
          'date-type': 'submission',
        },
        day: {
          _text: submissionDate.getDate(),
        },
        month: {
          _text: submissionDate.getMonth() + 1,
        },
        year: {
          _text: submissionDate.getFullYear(),
        },
      })
      revObj.comment = review.comments.map(comm => ({
        _attributes: {
          'comment-type': comm.type === 'public' ? 'comment' : 'confidential',
        },
        _text: comm.content,
      }))

      if (['reviewer', 'author'].includes(revType)) {
        const reviewComments = review.comments
        revObj.files.file = reviewComments
          .find(comm => comm.type === 'public')
          .files.map(file => {
            const fileType =
              file.type === 'reviewComment' ? 'review' : file.type

            return {
              item_type: {
                _text: fileType,
              },
              item_description: {
                _text: file.originalName,
              },
              item_name: {
                _text: file.fileName,
              },
            }
          })
      }

      return revObj
    })

    const aff = reviews.map((review, i) => ({
      _attributes: {
        id: `I${authorsLength + i + 1}`,
      },
      country: review.member.alias.country || 'UK',
      'addr-line': {
        _text: review.member.alias.aff || '',
      },
    }))

    set(jsonTemplate, 'article.front.rev-group.rev', xmlReviewers)
    set(jsonTemplate, 'article.front.rev-group.aff', aff)

    return jsonTemplate
  },
  createFileName: ({ id = Date.now(), prefix }) => `${prefix}${id}`,
}

const parseHtml = (content = '', options) => {
  if (/<\/?[^>]*>/.test(content)) {
    return convert.xml2js(content, options)
  }
  return content
}
