import React, { useEffect } from 'react'
import { useQuery, useMutation } from 'react-apollo'
import { get } from 'lodash'
import { Spinner } from '@pubsweet/ui'

import { mutations, queries } from '../graphql'

const SubmissionFromURL = ({ history, match }) => {
  const { data } = useQuery(queries.getActiveJournals)
  const [createDraft] = useMutation(mutations.createDraftManuscript)

  const activeJournals = get(data, 'getActiveJournals', [])
  useEffect(() => {
    if (activeJournals.length > 0) {
      const journalCode = get(match, 'params.code')
      const journal = activeJournals.find(
        journal => journal.code === journalCode,
      )
      const journalId = journal ? journal.id : null
      createDraft({ variables: { input: { journalId } } }).then(
        ({ data: { createDraftManuscript } }) => {
          history.replace(
            `/submit/${createDraftManuscript.submissionId}/${createDraftManuscript.id}`,
          )
        },
      )
    }
  }, [activeJournals, createDraft, history, match])
  return <Spinner />
}

export default SubmissionFromURL
