import moize from 'moize'
import {
  chain,
  cloneDeep,
  get,
  omit,
  isEqual,
  debounce,
  set,
  last,
} from 'lodash'

export const parseError = e => last(e.message.split(':')).trim()

export const parseAuthor = a =>
  omit({ ...a, ...get(a, 'alias'), ...get(a, 'alias.name') }, [
    '__typename',
    'id',
    'name',
    'alias',
    'user',
    'status',
    'invited',
    'responded',
    'reviewerNumber',
  ])

export const parseFormValues = values => {
  const manuscriptId = get(values, 'id', '')
  const sectionId = get(values, 'sectionId', '')
  const meta = omit(get(values, 'meta', {}), [
    '__typename',
    'hasConflictOfInterest',
  ])
  const journalId = get(values, 'journalId', '')
  const specialIssueId = get(values, 'specialIssueId', '')

  const authors = chain(values)
    .get('authors', [])
    .filter(a => a.id !== 'unsaved-author')
    .map(parseAuthor)
    .value()

  const files = chain(values)
    .get('files', {})
    .flatMap()
    .map(({ mimeType, originalName, filename, __typename, ...f }) => ({
      ...f,
      name: originalName,
    }))
    .value()

  return {
    manuscriptId,
    autosaveInput: {
      ...(journalId && { journalId }),
      ...(sectionId && { sectionId }),
      ...(specialIssueId && { specialIssueId }),
      authors,
      files,
      meta,
    },
  }
}

export const autosaveRequest = ({ values, updateDraft, updateAutosave }) => {
  const variables = parseFormValues(values)
  updateAutosave({
    variables: {
      params: {
        error: null,
        inProgress: true,
        updatedAt: null,
      },
    },
  })
  updateDraft({
    variables,
  }).then(r => {
    updateAutosave({
      variables: {
        params: {
          error: null,
          inProgress: false,
          updatedAt: Date.now(),
        },
      },
    })
  })
}

const memoizedAutosaveRequest = moize(autosaveRequest, {
  maxSize: 1,
  equals: ({ values: valuesOne }, { values: valuesTwo }) =>
    isEqual(valuesOne, valuesTwo),
})

export const autosaveForm = debounce(memoizedAutosaveRequest, 1000)

export const validateWizard = step => values => {
  const errors = {}
  if (values.isEditing) {
    errors.isEditing = 'An author is being edited.'
  }

  if (step === 3 && get(values, 'files.manuscript').length === 0) {
    set(errors, 'fileError', 'At least one manuscript is required.')
  }

  return errors
}

export const parseManuscriptFiles = (files = []) =>
  files.reduce(
    (acc, file) => ({
      ...acc,
      [file.type]: [...acc[file.type], file],
    }),
    {
      manuscript: [],
      coverLetter: [],
      supplementary: [],
      figure: [],
    },
  )

export const removeTypename = (inputObject = {}) => {
  const o = cloneDeep(inputObject)
  Object.keys(inputObject).forEach(key => {
    if (key === '__typename') delete o[key]
    if (o[key] !== null && typeof o[key] === 'object') {
      o[key] = removeTypename(o[key])
    }
  })
  return o
}

export const setInitialValues = values => {
  const id = get(values, 'id')
  const journalId = get(values, 'journalId')
  const sectionId = get(values, 'section.id')
  const specialIssueId = get(values, 'specialIssue.id')
  const meta = omit(get(values, 'meta', {}), '__typename')

  const authors = chain(values)
    .get('authors', [])
    .map(removeTypename)
    .value()

  const files = chain(values)
    .get('files', [])
    .map(removeTypename)
    .value()

  const issueType = specialIssueId ? 'specialIssue' : 'regularIssue'

  return {
    id,
    journalId,
    sectionId,
    specialIssueId,
    authors,
    issueType,
    files: parseManuscriptFiles(files),
    meta: {
      ...meta,
      hasConflictOfInterest: meta.conflictOfInterest ? 'yes' : 'no',
    },
  }
}
