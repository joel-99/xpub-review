module.exports = {
  async uploadManuscriptToMTS({ Manuscript, manuscriptId, sendPackage }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      `[
      files,
      section,
      articleType,
      teams.members,
      specialIssue.section,
      journal.peerReviewModel,
    ]`,
    )
    sendPackage({
      manuscript,
    })
  },
}
