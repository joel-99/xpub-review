const { includes } = require('lodash')
const articleTypeWithPeerReview = require('./strategies/articleTypeWithPeerReview')
const articleTypeWithRIPE = require('./strategies/articleTypeWithRIPE')

const initialize = ({
  logEvent,
  sendPackage,
  eventsService,
  notificationService,
  models: { Journal, Manuscript, Team, TeamMember, ArticleType },
}) => ({
  execute: async ({ manuscriptId, userId }) => {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[articleType, journal]',
    )
    const { journal, articleType: currentArticleType } = manuscript

    const editorialAssistant = await TeamMember.findCorrespondingEditorialAssistant(
      journal.id,
    )
    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId,
      sectionId: manuscript.sectionId,
    })

    const authorTeamMembers = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
      eagerLoadRelations: 'user',
    })
    const submittingAuthor = authorTeamMembers.find(tm => tm.isSubmitting)

    const strategy = includes(
      ArticleType.TypesWithRIPE,
      currentArticleType.name,
    )
      ? articleTypeWithRIPE
      : articleTypeWithPeerReview

    await strategy.execute({
      Team,
      journal,
      manuscript,
      TeamMember,
      Manuscript,
      sendPackage,
      triageEditor,
      eventsService,
      submittingAuthor,
      authorTeamMembers,
      editorialAssistant,
      notificationService,
    })

    notificationService.sendSubmittingAuthorConfirmation({
      journal,
      manuscript,
      submittingAuthor,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.manuscript_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
