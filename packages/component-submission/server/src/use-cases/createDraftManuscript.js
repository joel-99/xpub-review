const MAX_CUSTOM_ID_TRIES = 100

const initialize = ({ Manuscript, Team, User }) => ({
  async execute({ input: { journalId, customId }, userId }) {
    const author = await User.find(userId, 'identities')

    const isStaffAuthor = await author.hasStaffRole()

    if (!customId) {
      customId = await Manuscript.generateUniqueCustomId(
        Manuscript,
        MAX_CUSTOM_ID_TRIES,
      )
    }

    if (!customId) {
      throw new Error(
        `Unable to generate a customId after ${MAX_CUSTOM_ID_TRIES} tries.`,
      )
    }

    const manuscript = new Manuscript({
      journalId,
      customId,
    })

    await manuscript.save()

    if (!isStaffAuthor) {
      const authorTeam = new Team({
        role: Team.Role.author,
        manuscriptId: manuscript.id,
      })
      manuscript.assignTeam(authorTeam)
      await authorTeam.save()

      const authorTeamMember = authorTeam.addMember(author, {
        isSubmitting: true,
        isCorresponding: true,
      })
      authorTeamMember.userId = author.id
      authorTeamMember.teamId = authorTeam.id
      await authorTeamMember.save()
    }

    await manuscript.save()
    return manuscript.toDTO()
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
