import React from 'react'

import JournalContext from './JournalInfoContext'

function JournalProvider({ children, journal, publisher }) {
  const parsedStatuses = Object.keys(journal.statuses).reduce(
    (acc, k) => ({ ...acc, [k]: k }),
    {},
  )
  return (
    <JournalContext.Provider
      value={{ ...journal, ...publisher, parsedStatuses }}
    >
      {children}
    </JournalContext.Provider>
  )
}

export default JournalProvider
