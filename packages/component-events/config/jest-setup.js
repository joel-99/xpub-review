import { configure } from '@testing-library/react'

configure({
  testIdAttribute: 'data-test-id',
})

Object.assign(global, require('@pubsweet/errors'))
