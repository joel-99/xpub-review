// const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

// const chance = new Chance()
const events = require('../src')

const {
  User,
  Team,
  Journal,
  Identity,
  ArticleType,
  PeerReviewModel,
  JournalArticleType,
} = models

const eventsService = events.initialize({ models })
describe('Events service', () => {
  beforeEach(() => {
    global.applicationEventBus = {}
    global.applicationEventBus = {
      publishMessage: jest.fn(),
    }
  })
  it('calls the publish message function on the journal event', async () => {
    await fixtures.generateArticleTypes(ArticleType)
    const articleType = fixtures.getArticleTypeByPeerReview(true)

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    const journalArticleType = fixtures.generateJournalArticleType({
      properties: {
        journalId: journal.id,
        articleTypeId: articleType.id,
      },
      JournalArticleType,
    })
    journalArticleType.articleType = articleType
    journal.journalArticleTypes.push(journalArticleType)

    await eventsService.publishJournalEvent({
      journalId: journal.id,
      eventName: 'JournalUpdated',
    })
    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(1)
  })
  it('calls the publish message function on the user event', async () => {
    const user = fixtures.generateUser({ User, Identity })

    await eventsService.publishUserEvent({
      userId: user.id,
      eventName: 'UserUpdated',
    })
    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(1)
  })
})
