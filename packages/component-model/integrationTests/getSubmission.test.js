const logger = require('@pubsweet/logger')
const models = require('@pubsweet/models')
const { db, migrate } = require('@pubsweet/db-manager')
const { groupBy } = require('lodash')
const { getSubmissionUseCase } = require('../src/useCases')
const config = require('config')

describe('get manuscripts use case', () => {
  let submissionWithMostVersions
  let submissionId
  let journalId
  beforeAll(async () => {
    logger.info(config['pubsweet-server'].db)
    await dbCleaner()
    await db.seed.run({
      directory: '../app/seeds',
    })
    await db.seed.run({
      directory: '../app/testSeeds',
    })

    const { Manuscript } = models
    const manuscripts = await Manuscript.all()
    const groupedSubmissions = groupBy(manuscripts, 'submissionId')
    submissionWithMostVersions = Object.values(groupedSubmissions).find(
      submission => submission.length > 1,
    )
    const [manuscript] = submissionWithMostVersions
    ;({ submissionId, journalId } = manuscript)
  })

  afterAll(done => {
    db.destroy()
    done()
  })

  describe('as an admin', () => {
    let userId
    beforeAll(async () => {
      const { Team, TeamMember } = models
      ;({ userId } = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      }))
    })
    it('returns all versions of a manuscript', async () => {
      const { Team } = models
      const submission = await getSubmissionUseCase
        .initialize(models)
        .execute({ submissionId, userId })

      expect(submission.length).toBeGreaterThan(1)
      expect(submission[0].role).toEqual(Team.Role.admin)
    })
  })
  describe('as an editorial assistant', () => {
    let userId
    beforeAll(async () => {
      const { Team, TeamMember } = models
      ;({ userId } = await TeamMember.findOneByJournalAndRole({
        role: Team.Role.editorialAssistant,
        journalId,
      }))
    })
    it('returns all versions of a manuscript', async () => {
      const { Team } = models
      const submission = await getSubmissionUseCase
        .initialize(models)
        .execute({ submissionId, userId })

      expect(submission.length).toBeGreaterThan(1)
      expect(submission[0].role).toEqual(Team.Role.editorialAssistant)
    })
  })
})

const dbCleaner = async options => {
  await db.raw('DROP SCHEMA public CASCADE;')
  await db.raw('CREATE SCHEMA public;')
  await db.raw('GRANT ALL ON SCHEMA public TO public;')
  await migrate(options)
  logger.info('Dropped all tables and ran all migrations')

  return true
}
