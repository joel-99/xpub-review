const uuid = require('uuid')
const config = require('config')
const { raw, ref } = require('objection')
const logger = require('@pubsweet/logger')
const { chain, get, pick, sortBy, groupBy, last, omit } = require('lodash')

const statuses = config.get('statuses')

const HindawiBaseModel = require('../src/hindawiBaseModel')
const Review = require('./review')

class Manuscript extends HindawiBaseModel {
  static get tableName() {
    return 'manuscript'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        submissionId: { type: 'string', format: 'uuid' },
        status: {
          enum: Object.values(Manuscript.Statuses),
          default: Manuscript.Statuses.draft,
        },
        customId: { type: ['string', 'null'] },
        version: { type: 'string', default: '1' },
        title: { type: 'string', default: '' },
        abstract: { type: 'string', default: '' },
        publicationDates: { type: ['array', 'null'], default: [] },
        technicalCheckToken: { type: ['string', 'null'], format: 'uuid' },
        hasPassedEqa: { type: ['boolean', 'null'] },
        hasPassedEqs: { type: ['boolean', 'null'] },
        journalId: { type: ['string', null], format: 'uuid' },
        sectionId: { type: ['string', null], format: 'uuid' },
        agreeTc: { type: 'boolean', default: false },
        conflictOfInterest: { type: ['string', 'null'] },
        dataAvailability: { type: ['string', 'null'] },
        fundingStatement: { type: ['string', 'null'] },
        articleTypeId: { type: ['string', 'null'], format: 'uuid' },
        specialIssueId: { type: ['string', 'null'], format: 'uuid' },
        isPostAcceptance: { type: 'boolean', default: false },
      },
    }
  }

  static get relationMappings() {
    return {
      files: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./file'),
        join: {
          from: 'manuscript.id',
          to: 'file.manuscriptId',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./team'),
        join: {
          from: 'manuscript.id',
          to: 'team.manuscriptId',
          extra: ['role'],
        },
      },
      reviews: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./review'),
        join: {
          from: 'manuscript.id',
          to: 'review.manuscriptId',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./journal'),
        join: {
          from: 'manuscript.journalId',
          to: 'journal.id',
        },
      },
      section: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./section'),
        join: {
          from: 'manuscript.sectionId',
          to: 'section.id',
        },
      },
      logs: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./auditLog'),
        join: {
          from: 'manuscript.id',
          to: 'audit_log.manuscriptId',
        },
      },
      reviewerSuggestions: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./reviewerSuggestion'),
        join: {
          from: 'manuscript.id',
          to: 'reviewer_suggestion.manuscriptId',
        },
      },
      articleType: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./articleType'),
        join: {
          from: 'manuscript.articleTypeId',
          to: 'article_type.id',
        },
      },
      specialIssue: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./specialIssue'),
        join: {
          from: 'manuscript.specialIssueId',
          to: 'special_issue.id',
        },
      },
    }
  }

  static get Statuses() {
    return {
      draft: 'draft',
      technicalChecks: 'technicalChecks',
      submitted: 'submitted',
      academicEditorInvited: 'academicEditorInvited',
      academicEditorAssigned: 'academicEditorAssigned',
      reviewersInvited: 'reviewersInvited',
      underReview: 'underReview',
      reviewCompleted: 'reviewCompleted',
      revisionRequested: 'revisionRequested',
      pendingApproval: 'pendingApproval',
      rejected: 'rejected',
      inQA: 'inQA',
      accepted: 'accepted',
      withdrawalRequested: 'withdrawalRequested',
      withdrawn: 'withdrawn',
      deleted: 'deleted',
      published: 'published',
      olderVersion: 'olderVersion',
      academicEditorAssignedEditorialType:
        'academicEditorAssignedEditorialType',
      makeDecision: 'makeDecision',
      qualityChecksRequested: 'qualityChecksRequested',
      qualityChecksSubmitted: 'qualityChecksSubmitted',
      refusedToConsider: 'refusedToConsider',
    }
  }

  static get NonRejectableStatuses() {
    const statuses = this.Statuses
    return [
      statuses.draft,
      statuses.deleted,
      statuses.rejected,
      statuses.inQA,
      statuses.accepted,
      statuses.qualityChecksRequested,
      statuses.qualityChecksSubmitted,
      statuses.published,
      statuses.withdrawn,
      statuses.withdrawalRequested,
      statuses.olderVersion,
      statuses.refusedToConsider,
    ]
  }
  static get InProgressStatuses() {
    const statuses = this.Statuses
    return [
      statuses.submitted,
      statuses.underReview,
      statuses.makeDecision,
      statuses.reviewCompleted,
      statuses.pendingApproval,
      statuses.reviewersInvited,
      statuses.revisionRequested,
      statuses.academicEditorInvited,
      statuses.academicEditorAssigned,
    ]
  }

  static compareVersion(m1, m2) {
    let v1 = m1.version
    let v2 = m2.version
    if (typeof v1 !== 'string' || typeof v2 !== 'string') return false
    v1 = v1.split('.')
    v2 = v2.split('.')
    const k = Math.min(v1.length, v2.length)
    for (let i = 0; i < k; i += 1) {
      v1[i] = parseInt(v1[i], 10)
      v2[i] = parseInt(v2[i], 10)
      if (v1[i] > v2[i]) return 1
      if (v1[i] < v2[i]) return -1
    }
    if (v1.length === v2.length) return 0
    return v1.length < v2.length ? -1 : 1
  }

  static filterOlderVersions(manuscripts) {
    const submissions = groupBy(manuscripts, 'submissionId')
    const latestVersions = Object.values(submissions).map(versions => {
      if (versions.length === 1) {
        return versions[0]
      }

      const sortedVersions = versions.sort(this.compareVersion)
      const latestManuscript = last(sortedVersions)

      if (latestManuscript.status === this.Statuses.draft) {
        return sortedVersions[sortedVersions.length - 2]
      }

      return latestManuscript
    })

    return latestVersions
  }

  static async getManuscriptsForAdmin({
    searchValue = '',
    priorityFilter = 'all',
    dateOrder = 'desc',
    page = '0',
    pageSize = '10',
    Team,
  }) {
    const latestVersions = this.query()
      .select(raw('DISTINCT ON (submission_id) *'))
      .orderByRaw(`submission_id, string_to_array(version, '.')::int[] DESC`)
      .whereNot(builder =>
        builder.where('version', '!=', 1).andWhere('status', 'draft'),
      )
      .whereNot('status', 'deleted')
      .as('latest_versions')

    return this.query()
      .select('*')
      .from(latestVersions)
      .where(builder =>
        addPriorityFilter({ builder, priorityFilter, role: 'admin', Team }),
      )
      .where(builder => addSearchFilter(builder, searchValue))
      .orderBy('updated', dateOrder)
      .page(page, pageSize)
      .eager(
        '[articleType, teams.members, journal.peerReviewModel, specialIssue.peerReviewModel]',
      )
  }

  static async getManuscriptsForEditorialAssistant({
    user,
    searchValue = '',
    priorityFilter = 'all',
    dateOrder = 'desc',
    page = '0',
    pageSize = '10',
    Team,
  }) {
    const userJournalTeams = (await user
      .$relatedQuery('teams')
      .select('journal_id')
      .whereNotNull('journal_id')).map(({ journalId }) => journalId)

    const latestVersions = this.query()
      .select(raw('DISTINCT ON (submission_id) *'))
      .orderByRaw(`submission_id, string_to_array(version, '.')::int[] DESC`)
      .whereNotIn('status', ['deleted', 'draft'])
      .whereIn('journalId', userJournalTeams)
      .as('latest_versions')

    return this.query()
      .select('*')
      .from(latestVersions)
      .where(builder =>
        addPriorityFilter({
          builder,
          priorityFilter,
          role: Team.Role.editorialAssistant,
          Team,
        }),
      )
      .where(builder => addSearchFilter(builder, searchValue))
      .orderBy('updated', dateOrder)
      .page(page, pageSize)
      .eager(
        '[articleType, teams.members, journal.peerReviewModel, specialIssue.peerReviewModel]',
      )
  }

  static async getManuscriptsForManuscriptRoles({
    user,
    searchValue = '',
    priorityFilter = 'all',
    dateOrder = 'desc',
    page = '0',
    pageSize = '10',
    Team,
  }) {
    const userManuscriptTeams = (await user
      .$relatedQuery('teams')
      .select('manuscript_id')
      .where(builder =>
        builder
          .whereNotIn('status', ['declined', 'expired', 'removed'])
          .whereNotNull('manuscript_id'),
      )).map(({ manuscriptId }) => manuscriptId)

    const accessibleVersions = this.query()
      .select([
        raw('DISTINCT ON (submission_id) *'),
        Team.query()
          .select('t.role')
          .from('team AS t')
          .join('team_member AS tm', 'tm.team_id', 't.id')
          .where('tm.userId', user.id)
          .andWhere('t.manuscript_id', ref('manuscript.id'))
          .limit(1)
          .as('role'),
      ])
      .orderByRaw(`submission_id, string_to_array(version, '.')::int[] DESC`)
      .whereNot(builder =>
        builder.whereNot('version', 1).andWhere('status', 'draft'),
      )
      .whereIn('id', userManuscriptTeams)
      .whereNot('status', 'deleted')
      .as('accessible_versions')

    return this.query()
      .select('*')
      .from(accessibleVersions)
      .whereNot(builder =>
        builder
          .where('role', '!=', Team.Role.author)
          .andWhere('status', 'withdrawn'),
      )
      .where(builder => addPriorityFilter({ builder, priorityFilter, Team }))
      .where(builder => addSearchFilter(builder, searchValue))
      .orderBy('updated', dateOrder)
      .page(page, pageSize)
      .eager(
        '[articleType, teams.members, journal.peerReviewModel, specialIssue.peerReviewModel]',
      )
  }

  static async _findAllOrderedByVersion({
    order = 'asc',
    queryObject,
    eagerLoadRelations,
  }) {
    return this.query()
      .skipUndefined()
      .where(queryObject)
      .orderByRaw(`string_to_array(version, '.')::int[] ${order}`)
      .eager(this._parseEagerRelations(eagerLoadRelations))
  }

  static async findByCustomId(customId) {
    return this.query()
      .where({ customId })
      .first()
  }

  static async findAll({
    order,
    queryObject,
    orderByField,
    eagerLoadRelations,
  }) {
    if (!orderByField || orderByField === 'version')
      return this._findAllOrderedByVersion({
        order,
        queryObject,
        eagerLoadRelations,
      })
    return this.query()
      .skipUndefined()
      .where(queryObject)
      .orderBy(orderByField, order)
      .eager(this._parseEagerRelations(eagerLoadRelations))
  }

  static async findManuscriptByTeamMember(teamMemberId) {
    try {
      const results = await this.query()
        .select('m.*')
        .from('manuscript AS m')
        .join('team AS t', 'm.id', 't.manuscript_id')
        .join('team_member AS tm', 't.id', 'tm.team_id')
        .where('tm.id', teamMemberId)

      if (results.length > 0) return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async _findManuscriptsBySubmissionIdOrderedByVersion({
    order = 'asc',
    submissionId,
    excludedStatus,
    eagerLoadRelations,
  }) {
    return this.query()
      .skipUndefined()
      .whereNot({ status: excludedStatus })
      .andWhere({ submissionId })
      .orderByRaw(`string_to_array(version, '.')::int[] ${order}`)
      .eager(this._parseEagerRelations(eagerLoadRelations))
  }

  static async findManuscriptsBySubmissionId({
    order,
    orderByField,
    submissionId,
    excludedStatus,
    eagerLoadRelations,
  }) {
    if (!orderByField || orderByField === 'version')
      return this._findManuscriptsBySubmissionIdOrderedByVersion({
        order,
        submissionId,
        excludedStatus,
        eagerLoadRelations,
      })
    return this.query()
      .skipUndefined()
      .whereNot({ status: excludedStatus })
      .andWhere({ submissionId })
      .orderBy(orderByField, order)
      .eager(this._parseEagerRelations(eagerLoadRelations))
  }

  static async findAllBySubmissionAndUser({
    userId,
    submissionId,
    excludedStatuses,
    eagerLoadRelations,
  }) {
    const results = await this.query()
      .select('m.*', 't.role')
      .from('manuscript AS m')
      .join('team AS t', 'm.id', 't.manuscript_id')
      .join('team_member AS tm', 't.id', 'tm.team_id')
      .whereNotIn('m.status', excludedStatuses)
      .andWhere('tm.user_id', userId)
      .andWhere('m.submission_id', submissionId)
      .orderByRaw(`string_to_array(version, '.')::int[] DESC`)
      .eager(this._parseEagerRelations(eagerLoadRelations))

    return results
  }

  static async findAllForAdminBySubmission({
    adminRole,
    submissionId,
    excludedStatuses,
    eagerLoadRelations,
  }) {
    const results = await this.query()
      .select('m.*', 't.role')
      .from('manuscript AS m')
      .join('team AS t', 1, 1)
      .whereNotIn('m.status', excludedStatuses)
      .andWhere('m.submission_id', submissionId)
      .andWhere('t.role', adminRole)
      .orderByRaw(`string_to_array(version, '.')::int[] DESC`)
      .eager(this._parseEagerRelations(eagerLoadRelations))

    return results
  }

  static async findAllForEditorialAssistantBySubmission({
    editorialAssistantRole,
    submissionId,
    excludedStatuses,
    eagerLoadRelations,
  }) {
    const results = await this.query()
      .select('m.*', 't.role')
      .from('manuscript AS m')
      .join('team AS t', 't.journal_id', 'm.journal_id')
      .whereNotIn('m.status', excludedStatuses)
      .andWhere('m.submission_id', submissionId)
      .andWhere('t.role', editorialAssistantRole)
      .orderByRaw(`string_to_array(version, '.')::int[] DESC`)
      .eager(this._parseEagerRelations(eagerLoadRelations))

    return results
  }

  static async findLastManuscriptBySubmissionId({
    submissionId,
    eagerLoadRelations,
  }) {
    return this.query()
      .andWhere({ submissionId })
      .orderByRaw(`string_to_array(version, '.')::int[] desc`)
      .first()
      .eager(this._parseEagerRelations(eagerLoadRelations))
  }

  static async generateUniqueCustomId(maxTries) {
    const id = Math.round(1000000 + Math.random() * 8999999).toString()
    const found = await this.findByCustomId(id)

    if (!found) {
      return id
    }

    if (maxTries <= 0) {
      return undefined
    }

    return this.generateUniqueCustomId(Manuscript, maxTries - 1)
  }

  async getEditorLabel({ SpecialIssue, Journal, role }) {
    if (this.specialIssueId) {
      const specialIssue = await SpecialIssue.find(
        this.specialIssueId,
        'peerReviewModel',
      )
      return specialIssue.peerReviewModel[`${role}Label`]
    }
    const journal = await Journal.find(this.journalId, 'peerReviewModel')
    return journal.peerReviewModel[`${role}Label`]
  }

  getSubmittingAuthor() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const authorTeam = this.teams.find(t => t.role === 'author')

    if (!authorTeam) {
      throw new Error('Could not find author team')
    }

    if (authorTeam.members.length === 0) {
      throw new Error('Members are required.')
    }

    return authorTeam.members.find(tm => tm.isSubmitting)
  }

  getAuthors() {
    if (!this.teams) {
      logger.warn('Cannot get authors when teams are not loaded.')
      return
    }

    const authorTeam = this.teams.find(t => t.role === 'author')

    if (!authorTeam || authorTeam.members.length === 0) {
      return []
    }

    return sortBy(authorTeam.members, 'position')
  }

  getReviewers() {
    if (!this.teams) {
      logger.warn('Cannot get reviewers when teams are not loaded.')
      return
    }

    const reviewerTeam = this.teams.find(t => t.role === 'reviewer')
    if (!reviewerTeam || reviewerTeam.members.length === 0) {
      return []
    }

    return reviewerTeam.members
  }

  getReviewersForEventData() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }
    if (!this.reviewerSuggestions) {
      throw new Error('ReviewerSuggestions are required.')
    }

    const reviewerTeam = this.teams.find(t => t.role === 'reviewer')
    if (!reviewerTeam || reviewerTeam.members.length === 0) {
      return []
    }

    return reviewerTeam.members.map(reviewer => {
      const reviewerData = pick(reviewer, [
        'id',
        'created',
        'updated',
        'userId',
        'status',
        'responded',
      ])

      const orcidIdentity = reviewer.user.identities.find(
        identity => identity.type === 'orcid',
      )

      const externalReviewer = this.reviewerSuggestions.find(
        rs => rs.email === reviewer.alias.email,
      )

      return {
        ...reviewerData,
        ...reviewer.alias,
        orcidId: orcidIdentity ? orcidIdentity.identifier : '',
        fromService: externalReviewer ? externalReviewer.type : '',
      }
    })
  }

  getReviewsForEventData() {
    return this.reviews
      .filter(review => review.submitted !== null)
      .map(review => ({
        ...omit(review, ['manuscriptId']),
        comments: review.comments.map(comment => omit(comment, 'reviewId')),
      }))
  }

  getAuthorsForEventData() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const authorTeam = this.teams.find(t => t.role === 'author')
    if (!authorTeam || authorTeam.members.length === 0) {
      return []
    }

    return authorTeam.members.map(author => {
      const authorData = pick(author, [
        'id',
        'created',
        'updated',
        'userId',
        'status',
        'position',
        'isSubmitting',
        'isCorresponding',
      ])
      const orcidIdentity = author.user.identities.find(
        identity => identity.type === 'orcid',
      )

      return {
        ...authorData,
        ...author.alias,
        orcidId: orcidIdentity ? orcidIdentity.identifier : '',
      }
    })
  }

  getAcademicEditor() {
    if (!this.teams) {
      logger.warn('Cannot get academic editor when teams are not loaded.')
      return
    }

    const academicEditor = chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === 'accepted')
      .value()

    return academicEditor
  }

  getPendingAcademicEditor() {
    if (!this.teams) {
      logger.warn(
        'Cannot get pending academic editor when teams are not loaded.',
      )
      return
    }

    const academicEditor = chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === 'pending')
      .value()

    return academicEditor
  }

  getRIPE() {
    if (!this.teams) {
      logger.warn('Cannot get RIPE when teams are not loaded.')
      return
    }

    const RIPE = chain(this.teams)
      .find(t => t.role === 'researchIntegrityPublishingEditor')
      .get('members')
      .find(member => member.status === 'accepted')
      .value()

    return RIPE
  }

  _getTriageEditor() {
    if (!this.teams) {
      logger.warn('Cannot get triage editor when teams are not loaded.')
      return
    }

    const triageEditorTeam = this.teams.find(
      team => team.role === 'triageEditor',
    )

    if (!triageEditorTeam) {
      return
    }

    if (!triageEditorTeam.members) {
      logger.warn('Cannot get triage editor when team members are not loaded.')
      return
    }

    const triageEditor = triageEditorTeam.members.find(
      member => member.status === 'active',
    )

    return triageEditor
  }

  getAcademicEditorByStatus(status) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const academicEditor = chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === status)
      .value()

    return academicEditor
  }

  assignTeam(team) {
    this.teams = this.teams || []
    this.teams.push(team)
  }

  assignFile(file) {
    this.files = this.files || []
    this.files.push(file)
  }

  assignReview(review) {
    this.reviews = this.reviews || []
    this.reviews.push(review)
  }

  submitManuscript() {
    this.status = Manuscript.Statuses.technicalChecks
    this.technicalCheckToken = uuid.v4()
    this.publicationDates.push({
      type: Manuscript.Statuses.technicalChecks,
      date: Date.now(),
    })
  }

  updateStatus(status) {
    this.status = status
    return this
  }

  setComment() {
    if (!this.reviews) {
      throw new Error('Reviews are required.')
    }

    const responseToRevisionRequest = this.reviews.find(
      review =>
        review.recommendation === Review.Recommendations.responseToRevision,
    )
    if (!responseToRevisionRequest) {
      throw new ValidationError('There has been no request to revision')
    }

    this.comment = responseToRevisionRequest.comments[0].toDTO()
  }

  getLatestEditorReview() {
    if (!this.reviews) {
      throw new Error('Reviews are required.')
    }

    const editorReviews = this.reviews.filter(review =>
      ['admin', 'triageEditor', 'academicEditor'].includes(
        review.member.team.role,
      ),
    )

    return last(sortBy(editorReviews, 'updated'))
  }

  getLatestAcademicEditorRecommendation() {
    const editorReviews = this.reviews.filter(
      review => get(review, 'member.team.role') === 'academicEditor',
    )
    return last(sortBy(editorReviews, 'updated'))
  }

  getReviewersByStatus(status) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }
    const reviewerTeam = this.teams.find(t => t.role === 'reviewer')
    if (!reviewerTeam.members.length === 0) {
      throw new Error('Members are required.')
    }
    return reviewerTeam.members.filter(m => m.status === status)
  }

  getCustomId() {
    return this.role === 'author' &&
      [Manuscript.Statuses.draft, Manuscript.Statuses.technicalChecks].includes(
        this.status,
      )
      ? undefined
      : this.customId
  }

  getSortedFiles() {
    if (!this.files) {
      logger.warn('Cannot sort files when files are not loaded.')
      return []
    }

    return chain(this.files)
      .map(f => ({ ...f, filename: f.fileName }))
      .sortBy('type', 'position')
      .value()
  }

  getVisibleStatus({ userId, TeamRole, TeamMemberStatuses }) {
    let reviewerTeam
    let pendingReviewer
    let acceptedReviewer
    let academicEditorTeam
    let pendingAcademicEditor

    if (this.teams) {
      reviewerTeam = this.teams.find(t => t.role === TeamRole.reviewer)
      academicEditorTeam = this.teams.find(
        t => t.role === TeamRole.academicEditor,
      )
    }
    if (academicEditorTeam) {
      pendingAcademicEditor = academicEditorTeam.members.find(
        tm => tm.status === TeamMemberStatuses.pending && tm.userId === userId,
      )
    }
    if (pendingAcademicEditor) {
      return get(
        config.get('statuses'),
        'academicEditorInvited.academicEditor.label',
      )
    }
    if (reviewerTeam) {
      pendingReviewer = reviewerTeam.members.find(
        tm => tm.status === TeamMemberStatuses.pending && tm.userId === userId,
      )
      acceptedReviewer = reviewerTeam.members.find(
        tm => tm.status === TeamMemberStatuses.accepted && tm.userId === userId,
      )
    }

    if (pendingReviewer) {
      return get(config.get('statuses'), 'reviewersInvited.reviewer.label')
    }

    if (
      acceptedReviewer &&
      this.status === Manuscript.Statuses.reviewCompleted
    ) {
      return get(config.get('statuses'), 'underReview.reviewer.label')
    }

    if (this.status !== Manuscript.Statuses.olderVersion) {
      return get(config.get('statuses'), `${this.status}.${this.role}.label`)
    }

    return 'Viewing An Older Version'
  }

  toDTO() {
    const meta = pick(this, [
      'title',
      'agreeTc',
      'abstract',
      'articleTypeId',
      'publicationDates',
      'dataAvailability',
      'fundingStatement',
      'conflictOfInterest',
    ])

    const triageEditor = !this.triageEditor && this._getTriageEditor()
    const academicEditor = !this.academicEditor && this.getAcademicEditor()
    const pendingAcademicEditor =
      !this.pendingAcademicEditor && this.getPendingAcademicEditor()
    const researchIntegrityPublishingEditor =
      !this.researchIntegrityPublishingEditor && this.getRIPE()

    return Object.assign(this, {
      meta,
      files: this.getSortedFiles(),
      customId: this.getCustomId(),
      journal: this.journal ? this.journal.toDTO() : undefined,
      section: this.section ? this.section.toDTO() : undefined,
      articleType: this.articleType ? this.articleType.toDTO() : undefined,
      specialIssue: this.specialIssue ? this.specialIssue.toDTO() : undefined,
      triageEditor: triageEditor ? triageEditor.toDTO() : undefined,
      reviews:
        this.reviews && this.reviews.length > 0
          ? this.reviews.map(review => review.toDTO())
          : [],
      teams:
        this.teams && this.teams.length > 0
          ? this.teams.map(team => team.toDTO())
          : [],
      academicEditor: academicEditor ? academicEditor.toDTO() : undefined,
      authors:
        this.authors && this.authors.length > 0
          ? this.getAuthors().map(author => author.toDTO())
          : [],
      reviewers:
        this.reviewers && this.reviewers.length > 0
          ? this.getReviewers().map(reviewer => reviewer.toDTO())
          : [],
      pendingAcademicEditor: pendingAcademicEditor
        ? pendingAcademicEditor.toDTO()
        : undefined,
      researchIntegrityPublishingEditor: researchIntegrityPublishingEditor
        ? researchIntegrityPublishingEditor.toDTO()
        : undefined,
    })
  }
}

function addSearchFilter(builder, searchValue) {
  if (searchValue) {
    builder
      .where({
        custom_id: searchValue,
      })
      .orWhereRaw('lower(title) LIKE ?', [`%${searchValue.toLowerCase()}%`])
  }

  return builder
}

function computePriorityFilterStatuses(role, priorityFilter) {
  return chain(statuses)
    .mapValues((value, key) =>
      get(value, `${role}.${priorityFilter}`) ? key : null,
    )
    .values()
    .filter(el => el)
    .value()
}

function addPriorityFilter({ builder, priorityFilter, role, Team }) {
  const FILTER_VALUES = {
    ALL: 'all',
    NEEDS_ATTENTION: 'needsAttention',
    IN_PROGRESS: 'inProgress',
    ARCHIVED: 'archived',
  }

  if (priorityFilter === FILTER_VALUES.ALL) return builder

  if ([Team.Role.admin, Team.Role.editorialAssistant].includes(role)) {
    builder.whereIn(
      'status',
      computePriorityFilterStatuses(role, priorityFilter),
    )
  } else {
    Object.values(Team.Role)
      .filter(
        role => ![Team.Role.admin, Team.Role.editorialAssistant].includes(role),
      )
      .forEach(role => {
        const visibleStatuses = computePriorityFilterStatuses(
          role,
          priorityFilter,
        )
        if (!visibleStatuses.length) return

        builder.orWhere(builder =>
          builder.whereIn('status', visibleStatuses).andWhere('role', role),
        )
      })
  }

  return builder
}

module.exports = Manuscript
