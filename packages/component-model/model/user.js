const HindawiBaseModel = require('../src/hindawiBaseModel')
const Team = require('./team')
const Identity = require('./identity')

class User extends HindawiBaseModel {
  static get tableName() {
    return 'user'
  }

  static get schema() {
    return {
      properties: {
        defaultIdentity: { type: 'string' },
        isActive: { type: 'boolean' },
        isSubscribedToEmails: { type: 'boolean', default: true },
        confirmationToken: { type: ['string', 'null'], format: 'uuid' },
        invitationToken: { type: ['string', 'null'], format: 'uuid' },
        passwordResetToken: { type: ['string', 'null'], format: 'uuid' },
        unsubscribeToken: { type: ['string', 'null'], format: 'uuid' },
        agreeTc: { type: 'boolean' },
        passwordResetTimestamp: {
          type: ['string', 'null', 'object'],
          format: 'date-time',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      identities: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Identity,
        join: {
          from: 'user.id',
          to: 'identity.userId',
        },
      },
      teamMemberships: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./teamMember'),
        join: {
          from: 'user.id',
          to: 'team_member.userId',
        },
      },
      teams: {
        relation: HindawiBaseModel.ManyToManyRelation,
        modelClass: require('./team'),
        join: {
          from: 'user.id',
          through: {
            from: 'team_member.userId',
            to: 'team_member.teamId',
            extra: ['status'],
          },
          to: 'team.id',
        },
      },
    }
  }

  static async findAllWithDefaultIdentity({
    page = 0,
    pageSize = 20,
    orderBy = 'given_names',
    order = 'asc',
    searchValue = '',
  }) {
    const { results, total } = await this.query()
      .alias('u')
      .select(
        'u.id',
        'isActive',
        'surname',
        'given_names',
        'aff',
        'is_confirmed',
        'email',
      )
      .leftJoin('identity as i', 'u.id', 'i.userId')
      .where('i.type', 'local')
      .where(builder => addSearchFilter(builder, searchValue))
      .orderBy(orderBy, order)
      .page(page, pageSize)

    return {
      results: results.map(({ givenNames, surname, ...user }) => ({
        ...user,
        name: {
          givenNames,
          surname,
        },
      })),
      total,
    }
  }

  static async findOneWithDefaultIdentity(userId) {
    const { givenNames, surname, ...user } = await this.query()
      .alias('u')
      .select(
        'u.id',
        'isActive',
        'surname',
        'given_names',
        'aff',
        'is_confirmed',
        'email',
      )
      .leftJoin('identity as i', 'u.id', 'i.userId')
      .where('i.type', 'local')
      .where('u.id', userId)
      .first()

    return {
      ...user,
      name: {
        givenNames,
        surname,
      },
    }
  }

  async hasStaffRole() {
    return this.$relatedQuery('teams')
      .first()
      .then(result => result && Team.StaffRoles.includes(result.role))
  }

  static async findByEmailOrName({ input, eagerLoadRelations }) {
    return this.query()
      .from('user as u')
      .join('identity as i', 'i.user_id', 'u.id')
      .where('u.isActive', true)
      .andWhere('i.isConfirmed', true)
      .andWhere('i.email', 'ILIKE', `%${input}%`)
      .orWhereRaw(`concat(lower(surname), ' ', lower(given_names)) ILIKE ?`, [
        `%${input}%`,
      ])
      .orWhereRaw(`concat(lower(given_names), ' ', lower(surname)) ILIKE ?`, [
        `%${input}%`,
      ])
      .limit(10)
      .eager(HindawiBaseModel._parseEagerRelations(eagerLoadRelations))
  }

  getDefaultIdentity() {
    if (!this.identities) {
      throw new Error('Identities are required.')
    }

    return this.identities.find(
      identity => identity.type === this.defaultIdentity,
    )
  }

  assignIdentity(identity) {
    this.identities = this.identities || []
    this.identities.push(identity)
  }

  toDTO() {
    return {
      ...this,
      identities:
        this.identities && this.identities.map(identity => identity.toDTO()),
    }
  }

  getTeamMemberByRole(role) {
    if (!this.teamMemberships) {
      throw new Error('User team memberships are required.')
    }

    return this.teamMemberships.find(tm => tm.team.role === role)
  }

  async getTeamMemberRoleForManuscript(manuscript) {
    const userTeams = await this.$relatedQuery('teams').where(builder =>
      builder
        .where('manuscriptId', manuscript.id)
        .orWhere('journalId', manuscript.journalId)
        .orWhere('role', Team.Role.admin),
    )

    if (userTeams.find(({ role }) => role === Team.Role.author))
      // a user might be both author and triage editor (until the assignment is fixed)
      return Team.Role.author

    const manuscriptTeam = userTeams.find(
      ({ manuscriptId }) => manuscriptId === manuscript.id,
    )
    if (manuscriptTeam) return manuscriptTeam.role

    const journalTeam = userTeams.find(
      ({ journalId }) => journalId === manuscript.journalId,
    )
    if (journalTeam) return journalTeam.role

    if (userTeams.find(({ role }) => role === Team.Role.admin))
      return Team.Role.admin
  }

  async isUserConfirmed() {
    const confirmedIdentity = await this.$relatedQuery('identities').where(
      builder => builder.where('type', 'local').andWhere('isConfirmed', true),
    )
    return !!confirmedIdentity[0]
  }

  getTeamMemberForManuscript(manuscript) {
    if (!this.teamMemberships) {
      throw new Error('User team memberships are required.')
    }

    const author = this.teamMemberships.find(
      tm =>
        tm.team.role === Team.Role.author &&
        tm.team.manuscriptId === manuscript.id,
    )

    return (
      author ||
      this.teamMemberships.find(
        member =>
          member.team.manuscriptId === manuscript.id ||
          member.team.journalId === manuscript.journalId ||
          member.team.role === Team.Role.admin ||
          member.team.role === Team.Role.editorialAssistant ||
          member.team.role === Team.Role.researchIntegrityPublishingEditor,
      )
    )
  }

  getTeamMemberForJournal(journal) {
    if (!this.teamMemberships) {
      throw new Error('User team memberships are required.')
    }
    return this.teamMemberships.find(
      member =>
        member.team.journalId === journal.id ||
        member.team.role === Team.Role.admin,
    )
  }

  async getStaffRoles() {
    return this.$relatedQuery('teams').whereIn('role', Team.StaffRoles)
  }
}

function addSearchFilter(builder, searchValue) {
  if (searchValue) {
    builder
      .whereRaw('i.aff ILIKE ?', [`%${searchValue}%`])
      .orWhereRaw('i.email ILIKE ?', [`%${searchValue}%`])
      .orWhereRaw('i.surname ILIKE ?', [`%${searchValue}%`])
      .orWhereRaw('i.given_names ILIKE ?', [`%${searchValue}%`])
      .orWhereRaw(`concat(i.given_names, ' ', i.surname) ILIKE ?`, [
        `%${searchValue}%`,
      ])
      .orWhereRaw(`concat(i.surname, ' ', i.given_names) ILIKE ?`, [
        `%${searchValue}%`,
      ])
  }
  return builder
}

module.exports = User
