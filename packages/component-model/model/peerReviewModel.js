const HindawiBaseModel = require('../src/hindawiBaseModel')

class PeerReviewModel extends HindawiBaseModel {
  static get tableName() {
    return 'peer_review_model'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: 'string' },
        approvalEditors: { type: ['string'] },
        hasFigureheadEditor: { type: 'boolean' },
        figureheadEditorLabel: { type: 'string' },
        hasSections: { type: 'boolean' },
        hasTriageEditor: { type: 'boolean' },
        triageEditorLabel: { type: 'string' },
        triageEditorAssignmentTool: { type: ['string'] },
        academicEditorLabel: { type: 'string' },
        academicEditorAssignmentTool: { type: ['string'] },
        reviewAssignmentTool: { type: ['string'] },
      },
    }
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./journal'),
        join: {
          from: 'peerReviewModel.id',
          to: 'journal.peerReviewModelId',
        },
      },
    }
  }

  static async findOneByManuscript({ manuscriptId }) {
    try {
      const result = await this.query()
        .select('prm.*')
        .from('peer_review_model AS prm')
        .join('journal AS j', 'j.peer_review_model_id', 'prm.id')
        .join('manuscript AS m', 'm.journal_id', 'j.id')
        .where('m.id', manuscriptId)

      return result[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByJournal(journalId) {
    try {
      const result = await this.query()
        .select('prm.*')
        .from('peer_review_model AS prm')
        .join('journal AS j', 'j.peer_review_model_id', 'prm.id')
        .where('j.id', journalId)

      return result[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneBySpecialIssue(specialIssueId) {
    try {
      const result = await this.query()
        .select('prm.*')
        .from('peer_review_model AS prm')
        .join('special_issue AS si', 'si.peer_review_model_id', 'prm.id')
        .where('si.id', specialIssueId)

      return result[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  toDTO() {
    return { ...this }
  }
}

module.exports = PeerReviewModel
