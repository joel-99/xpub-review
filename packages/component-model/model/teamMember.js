const { pick, get } = require('lodash')

const HindawiBaseModel = require('../src/hindawiBaseModel')
const Manuscript = require('./manuscript')
const ArticleType = require('./articleType')

class TeamMember extends HindawiBaseModel {
  static get tableName() {
    return 'team_member'
  }

  static get schema() {
    return {
      properties: {
        userId: { type: 'string', format: 'uuid' },
        teamId: { type: 'string', format: 'uuid' },
        position: { type: ['integer', null] },
        isSubmitting: { type: ['boolean', null] },
        isCorresponding: { type: ['boolean', null] },
        status: {
          enum: Object.values(TeamMember.Statuses),
          default: TeamMember.Statuses.pending,
        },
        reviewerNumber: { type: ['integer', null] },
        responded: { type: ['string', 'object', 'null'], format: 'date-time' },
        alias: {
          type: 'object',
          properties: {
            surname: { type: ['string', 'null'] },
            givenNames: { type: ['string', 'null'] },
            email: { type: 'string' },
            aff: { type: ['string', 'null'] },
            country: { type: ['string', 'null'] },
            title: { type: ['string', 'null'] },
          },
        },
      },
    }
  }

  static get relationMappings() {
    return {
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./user'),
        join: {
          from: 'team_member.userId',
          to: 'user.id',
        },
      },
      team: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./team'),
        join: {
          from: 'team_member.teamId',
          to: 'team.id',
          extra: ['role'],
        },
      },
      jobs: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./job'),
        join: {
          from: 'team_member.id',
          to: 'job.teamMemberId',
        },
      },
    }
  }

  $formatDatabaseJson(json) {
    json = super.$formatDatabaseJson(json)
    const alias = JSON.parse(json.alias)
    const email = get(alias, 'email', '')
    return { ...json, alias: { ...alias, email: email.toLowerCase() } }
  }

  static get Statuses() {
    return {
      pending: 'pending',
      accepted: 'accepted',
      declined: 'declined',
      submitted: 'submitted',
      expired: 'expired',
      removed: 'removed',
      active: 'active',
    }
  }

  static async findPublisherMember(userId) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', '=', 't.id')
        .where('tm.user_id', userId)
        .andWhere(function andWhereNull() {
          this.whereNull('t.journal_id')
        })
        .andWhere(function andWhereNull() {
          this.whereNull('t.section_id')
        })
        .andWhere(function andWhereNull() {
          this.whereNull('t.manuscript_id')
        })
      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findTeamMembersWorkload({
    teamRole,
    sectionId,
    journalId,
    teamMemberStatuses,
    manuscriptStatuses,
  }) {
    try {
      const results = await this.query()
        .skipUndefined()
        .select('tm.user_id')
        .count('tm.id as workload')
        .from('team_member as tm')
        .leftJoin('team as t', 'tm.team_id', 't.id')
        .leftJoin('manuscript as m', 't.manuscript_id', 'm.id')
        .whereIn('tm.status', teamMemberStatuses)
        .whereIn('m.status', manuscriptStatuses)
        .andWhere('t.role', teamRole)
        .andWhere('m.journal_id', journalId)
        .andWhere('m.section_id', sectionId)
        .groupBy('tm.user_id')
        .limit(50)

      return results
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByStatuses({
    role,
    statuses,
    manuscriptId,
    submissionId,
  }) {
    try {
      const results = await this.query()
        .skipUndefined()
        .select('tm.*')
        .from('team_member AS tm')
        .leftJoin('team AS t', 'tm.team_id', 't.id')
        .leftJoin('manuscript AS m', 't.manuscript_id', '=', 'm.id')
        .whereIn('tm.status', statuses)
        .andWhere('t.role', role)
        .andWhere('m.id', manuscriptId)
        .andWhere('m.submission_id', submissionId)
        .limit(50)

      return results
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByJournalAndRole({ journalId, role, searchValue = '' }) {
    try {
      return await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('journal AS j', 't.journal_id', 'j.id')
        .where(builder => {
          builder.where('t.role', role).andWhere('j.id', journalId)
        })
        .andWhere(builder => {
          builder
            .whereRaw(`tm.alias->>'email' like ?`, [`${searchValue}%`])
            .orWhereRaw(
              `concat(lower(tm.alias->>'surname'), ' ',lower(tm.alias->>'givenNames')) like ?`,
              [`${searchValue.toLowerCase()}%`],
            )
            .orWhereRaw(
              `concat(lower(tm.alias->>'givenNames'), ' ',lower(tm.alias->>'surname')) like ?`,
              [`${searchValue.toLowerCase()}%`],
            )
        })
        .limit(50)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByManuscriptAndRole({
    manuscriptId,
    role,
    eagerLoadRelations,
  }) {
    try {
      return await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('t.role', role)
        .andWhere('m.id', manuscriptId)
        .limit(50)
        .eager(this._parseEagerRelations(eagerLoadRelations))
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllBySubmissionAndRole({ submissionId, role }) {
    try {
      return await this.query()
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('t.role', role)
        .andWhere('m.submission_id', submissionId)
        .limit(50)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllBySectionAndRole({ sectionId, role }) {
    try {
      return await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('section AS s', 't.section_id', 's.id')
        .where('t.role', role)
        .andWhere('s.id', sectionId)
        .limit(50)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllBySpecialIssueAndRole({
    specialIssueId,
    role,
    searchValue = '',
  }) {
    try {
      return await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('special_issue AS si', 't.special_issue_id', 'si.id')
        .where(builder =>
          builder.where('t.role', role).andWhere('si.id', specialIssueId),
        )
        .andWhere(builder => {
          builder
            .whereRaw(`tm.alias->>'email' like ?`, [`${searchValue}%`])
            .orWhereRaw(
              `concat(lower(tm.alias->>'surname'), ' ',lower(tm.alias->>'givenNames')) like ?`,
              [`${searchValue.toLowerCase()}%`],
            )
            .orWhereRaw(
              `concat(lower(tm.alias->>'givenNames'), ' ',lower(tm.alias->>'surname')) like ?`,
              [`${searchValue.toLowerCase()}%`],
            )
        })
        .limit(50)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllBySubmissionAndRoleAndUser({
    role,
    userId,
    submissionId,
  }) {
    try {
      return await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('tm.userId', userId)
        .andWhere('t.role', role)
        .andWhere('m.submission_id', submissionId)
        .limit(50)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllBySubmissionAndRoleAndStatus({
    role,
    status,
    submissionId,
  }) {
    try {
      return await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('tm.status', status)
        .andWhere('t.role', role)
        .andWhere('m.submission_id', submissionId)
        .limit(50)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByManuscriptAndRoleAndStatus({
    role,
    status,
    manuscriptId,
  }) {
    try {
      return await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('tm.status', status)
        .andWhere('t.role', role)
        .andWhere('m.id', manuscriptId)
        .limit(50)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByUserAndRole({ userId, role }) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .where('tm.userId', userId)
        .andWhere('t.role', role)
        .limit(1)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByRole({ role }) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .where('t.role', role)
        .limit(1)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByJournalAndRole({
    journalId,
    role,
    eagerLoadRelations,
  }) {
    try {
      return await this.query()
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('journal AS j', 't.journal_id', 'j.id')
        .where('t.role', role)
        .andWhere('j.id', journalId)
        .first()
        .eager(this._parseEagerRelations(eagerLoadRelations))
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByManuscriptAndRole({ manuscriptId, role }) {
    try {
      const results = await this.query()
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('t.role', role)
        .andWhere('m.id', manuscriptId)
        .limit(1)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneBySubmissionAndUser({ submissionId, userId, TeamRole }) {
    try {
      const submissionTeamMember = await this.query()
        .select('tm.*', 't.role')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('tm.userId', userId)
        .andWhere('m.submissionId', submissionId)
        .first()
      if (submissionTeamMember) return submissionTeamMember

      const result = await this.query()
        .select('m.journalId')
        .from('manuscript as m')
        .where('m.submissionId', submissionId)
        .first()
      const journalTeamMember = await this.query()
        .select('tm.*', 't.role')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .where('tm.userId', userId)
        .andWhere('t.journalId', result.journalId)
        .andWhere('t.role', TeamRole.editorialAssistant)
        .first()
      if (journalTeamMember) return journalTeamMember

      return await this.query()
        .select('tm.*', 't.role')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .where('tm.userId', userId)
        .andWhere('t.role', TeamRole.admin)
        .first()
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneBySectionAndRole({ sectionId, role }) {
    try {
      const results = await this.query()
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('section AS s', 't.section_id', 's.id')
        .where('t.role', role)
        .andWhere('s.id', sectionId)
        .limit(1)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByManuscriptAndRoleAndStatus({
    role,
    status,
    manuscriptId,
  }) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('t.role', role)
        .andWhere('tm.status', status)
        .andWhere('m.id', manuscriptId)
        .limit(1)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByManuscriptAndRoleAndUser({
    role,
    userId,
    manuscriptId,
  }) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('tm.userId', userId)
        .andWhere('t.role', role)
        .andWhere('m.id', manuscriptId)
        .limit(1)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByManuscriptAndUser({ userId, manuscriptId }) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('tm.userId', userId)
        .andWhere('m.id', manuscriptId)
        .limit(1)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByJournalAndUser({ userId, journalId }) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('journal AS j', 't.journal_id', 'j.id')
        .where('tm.userId', userId)
        .andWhere('j.id', journalId)
        .limit(1)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findOneBySectionAndUser({ userId, sectionId }) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('section AS s', 't.section_id', 's.id')
        .where('tm.userId', userId)
        .andWhere('s.id', sectionId)
        .limit(1)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findOneBySpecialIssueAndUser({ userId, specialIssueId }) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('special_issue AS si', 't.special_issue_id', 'si.id')
        .where('tm.userId', userId)
        .andWhere('si.id', specialIssueId)
        .limit(1)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findSubmittingAuthor(manuscriptId) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('tm.isSubmitting', true)
        .andWhere('m.id', '=', manuscriptId)

      if (results.length > 0) return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findCorrespondingEditorialAssistant(journalId) {
    try {
      const results = await this.query()
        .select('tm.*')
        .from('team_member AS tm')
        .join('team AS t', 'tm.team_id', 't.id')
        .join('journal AS j', 't.journal_id', 'j.id')
        .where('t.role', 'editorialAssistant')
        .andWhere('tm.isCorresponding', true)
        .andWhere('j.id', '=', journalId)

      if (results.length > 0) return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async getApprovalEditorRole({
    TeamRole,
    manuscript,
    TypesWithRIPE = [],
    EditorialTypes = [],
  }) {
    const { articleType } = manuscript

    if (TypesWithRIPE.includes(articleType.name)) {
      return TeamRole.researchIntegrityPublishingEditor
    }
    if (manuscript.specialIssueId) {
      if (EditorialTypes.includes(articleType.name)) {
        return TeamRole.academicEditor
      }
      return TeamRole.triageEditor
    }
    if (articleType.hasPeerReview) {
      if (
        manuscript.journal.peerReviewModel.approvalEditors.includes(
          TeamRole.academicEditor,
        )
      ) {
        return TeamRole.academicEditor
      }
      return TeamRole.triageEditor
    } else if (
      await this.findOneByManuscriptAndRoleAndStatus({
        status: 'accepted',
        manuscriptId: manuscript.id,
        role: TeamRole.academicEditor,
      })
    ) {
      return TeamRole.academicEditor
    }
    return TeamRole.triageEditor
  }

  static async isApprovalEditor({ userId, manuscriptId, models }) {
    const { Manuscript, Team } = models
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[articleType, journal.peerReviewModel]',
    )

    const approvalEditorRole = await this.getApprovalEditorRole({
      manuscript,
      TeamRole: Team.Role,
      TypesWithRIPE: ArticleType.TypesWithRIPE,
      EditorialTypes: ArticleType.EditorialTypes,
    })

    let approvalEditorTeamMembers
    approvalEditorTeamMembers = await this.findAllBySpecialIssueAndRole({
      specialIssueId: manuscript.specialIssueId,
      role: approvalEditorRole,
    })

    if (approvalEditorTeamMembers.length === 0) {
      approvalEditorTeamMembers = await this.findAllByManuscriptAndRole({
        role: approvalEditorRole,
        manuscriptId,
      })
    }
    if (!approvalEditorTeamMembers.length === 0) {
      return false
    }

    return approvalEditorTeamMembers.some(member => member.userId === userId)
  }

  static async findApprovalEditor({ manuscriptId, TeamRole }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[articleType, journal.peerReviewModel]',
    )
    const approvalEditorRole = await this.getApprovalEditorRole({
      manuscript,
      TeamRole,
      TypesWithRIPE: ArticleType.TypesWithRIPE,
    })

    const status =
      approvalEditorRole === TeamRole.triageEditor
        ? this.Statuses.active
        : this.Statuses.accepted

    return this.findOneByManuscriptAndRoleAndStatus({
      status,
      role: approvalEditorRole,
      manuscriptId: manuscript.id,
    })
  }

  static async findTriageEditor({
    TeamRole,
    sectionId,
    journalId,
    manuscriptId,
  }) {
    let triageEditor = await this.findOneByManuscriptAndRoleAndStatus({
      manuscriptId,
      role: TeamRole.triageEditor,
      status: this.Statuses.active,
    })

    if (triageEditor) return triageEditor

    if (!journalId && !sectionId) {
      throw new Error('Journal ID or Section ID is required.')
    }

    triageEditor = await this.findOneBySectionAndRole({
      sectionId,
      role: TeamRole.triageEditor,
    })

    if (triageEditor) return triageEditor

    triageEditor = await this.findOneByJournalAndRole({
      journalId,
      role: TeamRole.triageEditor,
    })

    return triageEditor
  }

  linkUser(user) {
    this.user = user
    if (!this.alias) {
      const defaultIdentity = user.getDefaultIdentity()
      this.alias = pick(defaultIdentity, [
        'surname',
        'title',
        'givenNames',
        'email',
        'aff',
        'country',
      ])
    }
  }

  getName() {
    return `${get(this, 'alias.givenNames', '')} ${get(
      this,
      'alias.surname',
      '',
    )}`
  }

  getEmail() {
    return `${get(this, 'alias.email', '')}`
  }

  getLastName() {
    return `${get(this, 'alias.surname', '')}`
  }

  getEventData({ peerReviewModel, Team, role }) {
    const getLabel = ({ prm, role }) => {
      if (role === Team.Role.editorialAssistant) return 'Editorial Assistant'
      if (role === Team.Role.triageEditor) return prm.triageEditorLabel
      if (role === Team.Role.academicEditor) return prm.academicEditorLabel
      if (role === Team.Role.researchIntegrityPublishingEditor)
        return 'Research Integrity Publishing Editor'
    }
    const orcidIdentity = this.user.identities.find(
      identity => identity.type === 'orcid',
    )
    return {
      id: this.id,
      userId: this.userId,
      isCorresponding: this.isCorresponding,
      ...this.alias,
      role: {
        type: role,
        label: getLabel({ prm: peerReviewModel, role, Team }),
      },
      orcidId: orcidIdentity ? orcidIdentity.identifier : '',
    }
  }

  toDTO() {
    return {
      ...this,
      invited: this.created,
      user: this.user ? this.user.toDTO() : undefined,
      alias: {
        ...this.alias,
        name: {
          surname: this.alias.surname,
          givenNames: this.alias.givenNames,
          title: this.alias.title,
        },
      },
      role: this.team ? this.team.role : undefined,
    }
  }
}

module.exports = TeamMember
