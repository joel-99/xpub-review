process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const { models, fixtures } = require('fixture-service')

const { Manuscript, Team, TeamMember } = models
const User = require('../model/user')

jest.mock('component-model', () => ({
  HindawiBaseModel: jest.fn(),
}))

describe('User model', () => {
  describe('getTeamMemberForManuscript', () => {
    it("returns the user's author member if he has multiple team-members", () => {
      const manuscript = fixtures.generateManuscript({ Manuscript })
      const user = new User()

      const authorTeam = fixtures.generateTeam({
        properties: { role: Team.Role.author },
        Team,
      })
      const authorMember = fixtures.generateTeamMember({
        properties: {
          userId: user.id,
          teamId: authorTeam.id,
        },
        TeamMember,
      })
      authorMember.team = authorTeam

      const aeTeam = fixtures.generateTeam({
        properties: { role: Team.Role.academicEditor },
        Team,
      })
      const editorMember = fixtures.generateTeamMember({
        properties: {
          userId: user.id,
          teamId: aeTeam.id,
        },
        TeamMember,
      })
      editorMember.team = aeTeam

      user.teamMemberships = [authorMember, editorMember]
      const teamMember = user.getTeamMemberForManuscript(manuscript)

      expect(teamMember).toBeDefined()
      expect(teamMember.team.role).toEqual(Team.Role.author)
    })
  })
})
