process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { Journal, Team, Manuscript } = models

const { getManuscriptUseCase } = require('../src/useCases')

describe('Get Manuscript use case', () => {
  it('returns manuscripts with a role a property', async () => {
    const journal = fixtures.generateJournal({ Journal })

    const triageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.submitted,
        journalId: journal.id,
      },
      Manuscript,
    })
    manuscript.journal = journal

    const responseManuscript = await getManuscriptUseCase
      .initialize({ models })
      .execute({ manuscriptId: manuscript.id, userId: triageEditor.userId })

    expect(responseManuscript.role).toEqual(Team.Role.triageEditor)
  })
})
