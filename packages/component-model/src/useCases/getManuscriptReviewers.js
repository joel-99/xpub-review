const initialize = models => ({
  execute: async ({ manuscript }) => {
    const { Team, TeamMember } = models
    if (manuscript.reviewers && manuscript.reviewers.length > 0)
      return manuscript.reviewers

    const reviewers = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.reviewer,
    })

    return reviewers.map(a => a.toDTO())
  },
})

const authsomePolicies = ['isAuthenticated', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
