const initialize = models => ({
  execute: async ({ manuscript }) => {
    if (manuscript.researchIntegrityPublishingEditor)
      return manuscript.researchIntegrityPublishingEditor

    const { Team, TeamMember } = models
    const researchIntegrityPublishingEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        status: TeamMember.Statuses.accepted,
        role: Team.Role.researchIntegrityPublishingEditor,
      },
    )

    return researchIntegrityPublishingEditor
      ? researchIntegrityPublishingEditor.toDTO()
      : undefined
  },
})

const authsomePolicies = ['isAuthenticated', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
