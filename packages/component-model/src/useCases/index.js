const getUserUseCase = require('./getUser')
const getJournalUseCase = require('./getJournal')
const currentUserUseCase = require('./currentUser')
const getSubmissionUseCase = require('./getSubmission')
const getManuscriptUseCase = require('./getManuscript')
const getDraftRevisionUseCase = require('./getDraftRevision')
const getTeamMemberUserUseCase = require('./getTeamMemberUser')
const getManuscriptRIPEUseCase = require('./getManuscriptRIPE')
const getManuscriptAuthorsUseCase = require('./getManuscriptAuthors')
const getManuscriptReviewsUseCase = require('./getManuscriptReviews')
const getManuscriptReviewersUseCase = require('./getManuscriptReviewers')
const getManuscriptTriageEditorUseCase = require('./getManuscriptTriageEditor')
const getManuscriptVisibleStatusUseCase = require('./getManuscriptVisibleStatus')
const getManuscriptAcademicEditorUseCase = require('./getManuscriptAcademicEditor')
const getManuscriptIsApprovalEditorUseCase = require('./getManuscriptIsApprovalEditor')
const getManuscriptPendingAcademicEditorUseCase = require('./getManuscriptPendingAcademicEditor')
const getUserForAutoAssignmentBasedOnWorkload = require('./getUserForAutoAssignmentBasedOnWorkload')

module.exports = {
  getUserUseCase,
  getJournalUseCase,
  currentUserUseCase,
  getManuscriptUseCase,
  getSubmissionUseCase,
  getDraftRevisionUseCase,
  getManuscriptRIPEUseCase,
  getTeamMemberUserUseCase,
  getManuscriptReviewsUseCase,
  getManuscriptAuthorsUseCase,
  getManuscriptReviewersUseCase,
  getManuscriptTriageEditorUseCase,
  getManuscriptVisibleStatusUseCase,
  getManuscriptAcademicEditorUseCase,
  getManuscriptIsApprovalEditorUseCase,
  getUserForAutoAssignmentBasedOnWorkload,
  getManuscriptPendingAcademicEditorUseCase,
}
