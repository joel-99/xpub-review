const initialize = models => ({
  execute: async ({ manuscript }) => {
    const { Team, TeamMember } = models
    if (manuscript.authors && manuscript.authors.length > 0)
      return manuscript.authors

    const authors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.author,
    })

    return authors.map(a => a.toDTO())
  },
})

const authsomePolicies = ['isAuthenticated', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
