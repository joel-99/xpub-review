const initialize = models => ({
  execute: async ({ manuscript }) => {
    if (manuscript.academicEditor) return manuscript.academicEditor
    const { Team, TeamMember } = models

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
      },
    )

    return academicEditor ? academicEditor.toDTO() : undefined
  },
})

const authsomePolicies = ['isAuthenticated', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
