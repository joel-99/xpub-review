const initialize = models => ({
  execute: async ({ manuscript }) => {
    if (manuscript.triageEditor) return manuscript.triageEditor
    const { Team, TeamMember } = models

    const triageEditor = await TeamMember.findOneByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.triageEditor,
    })

    return triageEditor ? triageEditor.toDTO() : undefined
  },
})

const authsomePolicies = ['isAuthenticated', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
