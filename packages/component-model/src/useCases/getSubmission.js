const initialize = models => ({
  execute: async ({ submissionId, userId }) => {
    const { TeamMember, Manuscript, Team } = models
    const requestTeamMember = await TeamMember.findOneBySubmissionAndUser({
      userId,
      submissionId,
      TeamRole: Team.Role,
    })

    if (!requestTeamMember) {
      throw new AuthorizationError(
        `Operation not permitted for user ${userId}!`,
      )
    }

    const forbiddenStatuses = [
      TeamMember.Statuses.declined,
      TeamMember.Statuses.expired,
      TeamMember.Statuses.removed,
    ]

    if (forbiddenStatuses.includes(requestTeamMember.status)) {
      throw new AuthorizationError(
        `Operation not permitted for user ${userId}!`,
      )
    }

    const eagerLoadRelations = [
      'files',
      'articleType',
      'journal.[peerReviewModel]',
      'specialIssue.peerReviewModel',
    ]

    if (
      requestTeamMember.status === TeamMember.Statuses.pending &&
      [Team.Role.academicEditor, Team.Role.reviewer].includes(
        requestTeamMember.role,
      )
    ) {
      eagerLoadRelations.splice(eagerLoadRelations.indexOf('files'), 1)
    }

    const excludedStatuses = [Manuscript.Statuses.draft]
    if (
      ![
        Team.Role.admin,
        Team.Role.author,
        Team.Role.editorialAssistant,
      ].includes(requestTeamMember.role)
    ) {
      excludedStatuses.push(Manuscript.Statuses.withdrawn)
    }

    let manuscripts = []

    if (requestTeamMember.role === Team.Role.admin) {
      manuscripts = await Manuscript.findAllForAdminBySubmission({
        submissionId,
        excludedStatuses,
        eagerLoadRelations,
        adminRole: Team.Role.admin,
      })
    } else if (requestTeamMember.role === Team.Role.editorialAssistant) {
      manuscripts = await Manuscript.findAllForEditorialAssistantBySubmission({
        submissionId,
        excludedStatuses,
        eagerLoadRelations,
        editorialAssistantRole: Team.Role.editorialAssistant,
      })
    } else {
      manuscripts = await Manuscript.findAllBySubmissionAndUser({
        submissionId,
        excludedStatuses,
        eagerLoadRelations,
        userId: requestTeamMember.userId,
      })
    }

    return manuscripts.map(m => m.toDTO())
  },
})

const authsomePolicies = ['isAuthenticated', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
