const initialize = models => ({
  execute: async ({ manuscriptId, userId }) => {
    const { TeamMember } = models

    return TeamMember.isApprovalEditor({
      userId,
      models,
      manuscriptId,
    })
  },
})

const authsomePolicies = ['isAuthenticated', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
