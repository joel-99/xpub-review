const initialize = models => ({
  execute: async ({ manuscript, userId }) => {
    if (manuscript.visibleStatus) return manuscript.visibleStatus

    const { Team, TeamMember } = models

    return manuscript.getVisibleStatus({
      userId,
      TeamRole: Team.Role,
      TeamMemberStatuses: TeamMember.Statuses,
    })
  },
})

const authsomePolicies = ['isAuthenticated', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
