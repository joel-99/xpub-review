const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('../useCases')

const resolvers = {
  Query: {
    async getManuscript(_, { manuscriptId }, ctx) {
      return useCases.getManuscriptUseCase
        .initialize({ models })
        .execute({ manuscriptId, userId: ctx.user })
    },
    async getSubmission(_, { submissionId }, ctx) {
      return useCases.getSubmissionUseCase
        .initialize(models)
        .execute({ submissionId, userId: ctx.user })
    },
    async getDraftRevision(_, { submissionId }, ctx) {
      return useCases.getDraftRevisionUseCase
        .initialize(models)
        .execute({ submissionId, userId: ctx.user })
    },
  },
  Manuscript: {
    async reviews(manuscript, query, ctx) {
      return useCases.getManuscriptReviewsUseCase
        .initialize(models)
        .execute({ manuscript, userId: ctx.user })
    },
    async visibleStatus(manuscript, query, ctx) {
      return useCases.getManuscriptVisibleStatusUseCase
        .initialize(models)
        .execute({ manuscript, userId: ctx.user })
    },
    async isApprovalEditor(manuscript, query, ctx) {
      return useCases.getManuscriptIsApprovalEditorUseCase
        .initialize(models)
        .execute({ manuscriptId: manuscript.id, userId: ctx.user })
    },
    async authors(manuscript, query, ctx) {
      return useCases.getManuscriptAuthorsUseCase
        .initialize(models)
        .execute({ manuscript })
    },
    async reviewers(manuscript, query, ctx) {
      return useCases.getManuscriptReviewersUseCase
        .initialize(models)
        .execute({ manuscript })
    },
    async triageEditor(manuscript, query, ctx) {
      return useCases.getManuscriptTriageEditorUseCase
        .initialize(models)
        .execute({ manuscript })
    },
    async academicEditor(manuscript, query, ctx) {
      return useCases.getManuscriptAcademicEditorUseCase
        .initialize(models)
        .execute({ manuscript })
    },
    async pendingAcademicEditor(manuscript, query, ctx) {
      return useCases.getManuscriptPendingAcademicEditorUseCase
        .initialize(models)
        .execute({ manuscript })
    },
    async researchIntegrityPublishingEditor(manuscript, query, ctx) {
      return useCases.getManuscriptRIPEUseCase
        .initialize(models)
        .execute({ manuscript })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
