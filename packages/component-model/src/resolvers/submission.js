const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('../useCases')

const resolvers = {
  Query: {
    async getSubmission(_, { submissionId }, ctx) {
      return useCases.getSubmissionUseCase
        .initialize(models)
        .execute({ submissionId, userId: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
