const Chance = require('chance')

const chance = new Chance()
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  Team,
  Review,
  Journal,
  TeamMember,
  Manuscript,
  ArticleType,
  PeerReviewModel,
} = models

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  revision_submitted: 'submitted manuscript revision',
}
logEvent.objectType = { manuscript: 'manuscript' }

const invitationsService = {
  sendInvitationToReviewerAfterMajorRevision: jest.fn(),
}

const emailJobsService = {
  scheduleEmailsWhenReviewerIsInvitedAfterMajorRevision: jest.fn(),
}

const removalJobsService = {
  scheduleRemovalJob: jest.fn(),
}

const eventsService = {
  publishSubmissionEvent: jest.fn(),
}

const useCases = require('../../src/useCases/submitRevision')

const { submitRevisionUseCase } = useCases

describe('Submit Revision Use Case', () => {
  let notificationService = {}
  beforeEach(() => {
    notificationService = {
      notifyTriageEditor: jest.fn(),
      notifyAcademicEditor: jest.fn(),
      notifyHeWhenRevisionSubmitted: jest.fn(),
      notifyEiCWhenRevisionSubmitted: jest.fn(),
    }
  })
  it('updates the status when the triage editor has requested revision', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: { approvalEditors: [Team.Role.triageEditor] },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    const triageEditor = await dataService.createUserOnJournal({
      journal,
      fixtures,
      models,
      role: Team.Role.triageEditor,
    })

    await fixtures.generateArticleTypes(ArticleType)
    const articleType = fixtures.getArticleTypeByPeerReview(true)

    const manuscript = fixtures.generateManuscript({
      properties: {
        version: '1',
        journalId: journal.id,
        submissionId: chance.guid(),
        status: Manuscript.Statuses.submitted,
      },
      Manuscript,
    })
    manuscript.journal = journal

    const author = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await dataService.createReviewOnManuscript({
      models,
      fixtures,
      manuscript,
      teamMember: triageEditor,
      recommendation: Review.Recommendations.revision,
    })

    const revisionManuscript = fixtures.generateManuscript({
      properties: {
        version: '2',
        journalId: journal.id,
        articleTypeId: articleType.id,
        status: Manuscript.Statuses.draft,
        submissionId: manuscript.submissionId,
      },
      Manuscript,
    })

    await dataService.addUserOnManuscript({
      models,
      fixtures,
      role: Team.Role.author,
      input: { isSubmitting: true },
      manuscript: revisionManuscript,
      user: fixtures.users.find(u => u.id === author.userId),
    })

    await dataService.createReviewOnManuscript({
      models,
      fixtures,
      manuscript,
      teamMember: author,
      recommendation: Review.Recommendations.responseToRevision,
    })

    await submitRevisionUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        eventsService,
        emailJobsService,
        removalJobsService,
        invitationsService,
        notificationService,
      })
      .execute({ submissionId: manuscript.submissionId })

    expect(revisionManuscript.status).toEqual(Manuscript.Statuses.submitted)
  })
  it('updates the status when AE has requested revision without reviewers', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: { approvalEditors: [Team.Role.triageEditor] },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      models,
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        version: '1',
        journalId: journal.id,
        status: Manuscript.Statuses.submitted,
        submissionId: chance.guid(),
      },
      Manuscript,
    })

    const author = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })
    const academicEditor = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })

    await dataService.createReviewOnManuscript({
      models,
      fixtures,
      manuscript,
      recommendation: Review.Recommendations.minor,
      teamMember: academicEditor,
    })
    const revisionManuscript = fixtures.generateManuscript({
      properties: {
        version: '2',
        journalId: journal.id,
        status: Manuscript.Statuses.draft,
        submissionId: manuscript.submissionId,
      },
      Manuscript,
    })
    await dataService.addUserOnManuscript({
      fixtures,
      models,
      manuscript: revisionManuscript,
      role: Team.Role.author,
      user: fixtures.users.find(u => u.id === author.userId),
      input: { isSubmitting: true },
    })
    await dataService.addUserOnManuscript({
      fixtures,
      models,
      manuscript: revisionManuscript,
      role: Team.Role.academicEditor,
      user: fixtures.users.find(u => u.id === academicEditor.userId),
      input: { isSubmitting: true },
    })
    await dataService.createReviewOnManuscript({
      models,
      fixtures,
      manuscript,
      recommendation: Review.Recommendations.responseToRevision,
      teamMember: author,
    })
    await submitRevisionUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        eventsService,
        emailJobsService,
        invitationsService,
        notificationService,
        removalJobsService,
      })
      .execute({ submissionId: manuscript.submissionId })

    expect(revisionManuscript.status).toEqual(
      Manuscript.Statuses.academicEditorAssigned,
    )
  })
  it('updates the status when AE has requested a major revision with reviewers', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: { approvalEditors: [Team.Role.triageEditor] },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      models,
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        version: '1',
        journalId: journal.id,
        status: Manuscript.Statuses.submitted,
        submissionId: chance.guid(),
      },
      Manuscript,
    })

    const author = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })
    const academicEditor = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })
    const reviewer = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      input: {
        isSubmitting: true,
        status: TeamMember.Statuses.submitted,
        reviewerNumber: 1,
      },
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      input: { isSubmitting: true, status: TeamMember.Statuses.accepted },
    })
    await dataService.createReviewOnManuscript({
      models,
      fixtures,
      manuscript,
      recommendation: Review.Recommendations.major,
      teamMember: academicEditor,
    })
    const revisionManuscript = fixtures.generateManuscript({
      properties: {
        version: '2',
        journalId: journal.id,
        status: Manuscript.Statuses.draft,
        submissionId: manuscript.submissionId,
      },
      Manuscript,
    })
    await dataService.addUserOnManuscript({
      fixtures,
      models,
      manuscript: revisionManuscript,
      role: Team.Role.author,
      user: fixtures.users.find(u => u.id === author.userId),
      input: { isSubmitting: true },
    })
    await dataService.addUserOnManuscript({
      fixtures,
      models,
      manuscript: revisionManuscript,
      role: Team.Role.academicEditor,
      user: fixtures.users.find(u => u.id === academicEditor.userId),
      input: { isSubmitting: true },
    })
    await dataService.addUserOnManuscript({
      fixtures,
      models,
      manuscript: revisionManuscript,
      role: Team.Role.reviewer,
      user: fixtures.users.find(u => u.id === reviewer.userId),
      input: { status: TeamMember.Statuses.pending },
    })
    await dataService.createReviewOnManuscript({
      models,
      fixtures,
      manuscript,
      recommendation: Review.Recommendations.responseToRevision,
      teamMember: author,
    })
    await submitRevisionUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        eventsService,
        emailJobsService,
        invitationsService,
        notificationService,
        removalJobsService,
      })
      .execute({ submissionId: manuscript.submissionId })

    expect(revisionManuscript.status).toEqual(Manuscript.Statuses.underReview)
    expect(manuscript.status).toEqual(Manuscript.Statuses.olderVersion)
  })
  it('updates the status when AE has requested a minor revision with reviewers', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: { approvalEditors: [Team.Role.triageEditor] },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      models,
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        version: '1',
        journalId: journal.id,
        status: Manuscript.Statuses.submitted,
        submissionId: chance.guid(),
      },
      Manuscript,
    })

    const author = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })
    const academicEditor = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })
    const reviewer = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      input: {
        isSubmitting: true,
        status: TeamMember.Statuses.submitted,
        reviewerNumber: 1,
      },
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      input: { isSubmitting: true, status: TeamMember.Statuses.accepted },
    })
    await dataService.createReviewOnManuscript({
      models,
      fixtures,
      manuscript,
      recommendation: Review.Recommendations.minor,
      teamMember: academicEditor,
    })
    const revisionManuscript = fixtures.generateManuscript({
      properties: {
        version: '2',
        journalId: journal.id,
        status: Manuscript.Statuses.draft,
        submissionId: manuscript.submissionId,
      },
      Manuscript,
    })
    await dataService.addUserOnManuscript({
      fixtures,
      models,
      manuscript: revisionManuscript,
      role: Team.Role.author,
      user: fixtures.users.find(u => u.id === author.userId),
      input: { isSubmitting: true },
    })
    await dataService.addUserOnManuscript({
      fixtures,
      models,
      manuscript: revisionManuscript,
      role: Team.Role.academicEditor,
      user: fixtures.users.find(u => u.id === academicEditor.userId),
      input: { isSubmitting: true },
    })
    await dataService.addUserOnManuscript({
      fixtures,
      models,
      manuscript: revisionManuscript,
      role: Team.Role.reviewer,
      user: fixtures.users.find(u => u.id === reviewer.userId),
      input: { status: TeamMember.Statuses.pending },
    })
    await dataService.createReviewOnManuscript({
      models,
      fixtures,
      manuscript,
      recommendation: Review.Recommendations.responseToRevision,
      teamMember: author,
    })
    await submitRevisionUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        eventsService,
        emailJobsService,
        invitationsService,
        notificationService,
        removalJobsService,
      })
      .execute({ submissionId: manuscript.submissionId })

    expect(revisionManuscript.status).toEqual(
      Manuscript.Statuses.reviewCompleted,
    )
    expect(manuscript.status).toEqual(Manuscript.Statuses.olderVersion)
  })
  it('invites all submitted reviewers after AE requested a major revision', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: { approvalEditors: [Team.Role.triageEditor] },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      models,
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        version: '1',
        journalId: journal.id,
        status: Manuscript.Statuses.submitted,
        submissionId: chance.guid(),
      },
      Manuscript,
    })

    const author = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })
    const academicEditor = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      input: {
        isSubmitting: true,
        status: TeamMember.Statuses.submitted,
        reviewerNumber: 1,
      },
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      input: {
        isSubmitting: true,
        status: TeamMember.Statuses.accepted,
      },
    })

    await dataService.createReviewOnManuscript({
      models,
      fixtures,
      manuscript,
      recommendation: Review.Recommendations.major,
      teamMember: academicEditor,
    })

    const revisionManuscript = fixtures.generateManuscript({
      properties: {
        version: '2',
        journalId: journal.id,
        status: Manuscript.Statuses.draft,
        submissionId: manuscript.submissionId,
      },
      Manuscript,
    })

    await dataService.addUserOnManuscript({
      fixtures,
      models,
      manuscript: revisionManuscript,
      role: Team.Role.author,
      user: fixtures.users.find(u => u.id === author.userId),
      input: { isSubmitting: true },
    })
    await dataService.createReviewOnManuscript({
      models,
      fixtures,
      manuscript,
      recommendation: Review.Recommendations.responseToRevision,
      teamMember: author,
    })

    await submitRevisionUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        eventsService,
        emailJobsService,
        notificationService,
        invitationsService,
        removalJobsService,
      })
      .execute({ submissionId: manuscript.submissionId })

    expect(
      emailJobsService.scheduleEmailsWhenReviewerIsInvitedAfterMajorRevision,
    ).toHaveBeenCalledTimes(2)
    expect(removalJobsService.scheduleRemovalJob).toHaveBeenCalledTimes(2)
  })
  it('notifies the academic editor when a regular revision is submitted under the Single Tier AE peer-review model', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: { approvalEditors: [Team.Role.academicEditor] },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        version: '1',
        journalId: journal.id,
        status: Manuscript.Statuses.submitted,
        submissionId: chance.guid(),
      },
      Manuscript,
    })
    manuscript.journal = journal

    const author = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    const academicEditor = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })
    await dataService.createReviewOnManuscript({
      models,
      fixtures,
      manuscript,
      teamMember: academicEditor,
      recommendation: Review.Recommendations.revision,
    })

    await fixtures.generateArticleTypes(ArticleType)
    const articleType = fixtures.getArticleTypeByPeerReview(true)
    const revisionManuscript = fixtures.generateManuscript({
      properties: {
        version: '2',
        journalId: journal.id,
        articleTypeId: articleType.id,
        status: Manuscript.Statuses.draft,
        submissionId: manuscript.submissionId,
      },
      Manuscript,
    })
    revisionManuscript.journal = journal

    await dataService.addUserOnManuscript({
      models,
      fixtures,
      role: Team.Role.author,
      input: { isSubmitting: true },
      manuscript: revisionManuscript,
      user: fixtures.users.find(u => u.id === author.userId),
    })

    await dataService.createReviewOnManuscript({
      models,
      fixtures,
      manuscript,
      teamMember: author,
      recommendation: Review.Recommendations.responseToRevision,
    })

    await submitRevisionUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        eventsService,
        emailJobsService,
        removalJobsService,
        invitationsService,
        notificationService,
      })
      .execute({ submissionId: manuscript.submissionId })

    expect(revisionManuscript.status).toEqual(
      Manuscript.Statuses.academicEditorAssigned,
    )
    expect(notificationService.notifyAcademicEditor).toHaveBeenCalledTimes(1)
  })
})
