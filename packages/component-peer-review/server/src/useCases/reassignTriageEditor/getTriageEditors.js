const { pullAllBy } = require('lodash')

const initialize = ({ TeamMember, Team, Manuscript }) => ({
  async execute(manuscriptId) {
    const manuscript = await Manuscript.find(manuscriptId)

    let members = []
    if (!manuscript.specialIssueId && !manuscript.sectionId) {
      members = await TeamMember.findAllByJournalAndRole({
        role: Team.Role.triageEditor,
        journalId: manuscript.journalId,
      })
    }

    if (manuscript.specialIssueId) {
      members = await TeamMember.findAllBySpecialIssueAndRole({
        role: Team.Role.triageEditor,
        specialIssueId: manuscript.specialIssueId,
      })
    }

    if (manuscript.sectionId) {
      members = await TeamMember.findAllBySectionAndRole({
        role: Team.Role.triageEditor,
        sectionId: manuscript.sectionId,
      })
    }

    const removedMembers = await TeamMember.findAllBySubmissionAndRoleAndStatus(
      {
        role: Team.Role.triageEditor,
        status: TeamMember.Statuses.removed,
        submissionId: manuscript.submissionId,
      },
    )

    return pullAllBy(members, removedMembers, 'userId').map(tm => tm.toDTO())
  },
})

const authsomePolicies = ['isAuthenticated', 'isEditorialAssistant']

module.exports = {
  initialize,
  authsomePolicies,
}
