const config = require('config')
const models = require('@pubsweet/models')
const Email = require('@pubsweet/component-email-templating')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')

const useCases = require('./index')
const urlService = require('../../urlService/urlService')

const getProps = require('../../emailPropsService/emailPropsService')
const getEmailCopy = require('../../notifications/reassignTriageEditor/getEmailCopy')
const reassignTriageEditorNotifications = require('../../notifications/reassignTriageEditor/reassignTriageEditor')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const resolver = {
  Query: {
    async getTriageEditors(_, { manuscriptId }, ctx) {
      return useCases.getTriageEditorsUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
  Mutation: {
    async reassignTriageEditor(_, { teamMemberId, manuscriptId }, ctx) {
      const getEmailCopyService = getEmailCopy.initialize()
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const notificationService = reassignTriageEditorNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })

      return useCases.reassignTriageEditorUseCase
        .initialize({
          models,
          logEvent,
          notificationService,
        })
        .execute({ teamMemberId, userId: ctx.user, manuscriptId })
    },
  },
}

module.exports = resolver
