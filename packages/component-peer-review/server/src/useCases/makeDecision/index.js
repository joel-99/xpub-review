const makeDecisionToPublishUseCase = require('./publish')
const makeDecisionToRejectUseCase = require('./reject')
const makeDecisionToReturnUseCase = require('./return')

module.exports = {
  makeDecisionToPublishUseCase,
  makeDecisionToRejectUseCase,
  makeDecisionToReturnUseCase,
}
