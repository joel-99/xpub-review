const { isEmpty } = require('lodash')
const { uploadToMTS } = require('./uploadToMTS')
const { notifyAcademicEditor } = require('./notifyAcademicEditor')
const Promise = require('bluebird')

const initialize = ({
  notificationService,
  models: {
    Job,
    Team,
    User,
    Review,
    Comment,
    Journal,
    Section,
    TeamMember,
    Manuscript,
    SpecialIssue,
  },
  logEvent,
  sendPackage,
  jobsService,
  eventsService,
}) => ({
  async execute({ manuscriptId, content, userId }) {
    const manuscript = await Manuscript.find(manuscriptId, 'articleType')
    const nonRejectableStatuses = Manuscript.NonRejectableStatuses
    if (nonRejectableStatuses.includes(manuscript.status)) {
      throw new ValidationError(
        `Cannot reject a manuscript in the current status.`,
      )
    }

    let rejectEditor = await TeamMember.findOneByManuscriptAndUser({
      manuscriptId,
      userId,
    })
    if (!rejectEditor) {
      rejectEditor = await TeamMember.findOneByJournalAndUser({
        journalId: manuscript.journalId,
        userId,
      })
    }
    if (!rejectEditor) {
      rejectEditor = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      })
    }

    const review = new Review({
      manuscriptId,
      teamMemberId: rejectEditor.id,
      submitted: new Date().toISOString(),
      recommendation: Review.Recommendations.reject,
    })
    await review.save()
    const comment = new Comment({
      content,
      reviewId: review.id,
      type: Comment.Types.public,
    })
    await comment.save()

    manuscript.assignReview(review)
    manuscript.updateStatus(Manuscript.Statuses.rejected)
    await manuscript.save()

    await uploadToMTS({
      manuscriptId,
      sendPackage,
      isRejected: true,
      models: { Section, Journal, Manuscript },
    })

    const journal = await Journal.find(manuscript.journalId, 'peerReviewModel')

    const academicEditors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.academicEditor,
    })
    const editorialAssistant = await TeamMember.findOneByJournalAndRole({
      role: Team.Role.editorialAssistant,
      journalId: journal.id,
    })

    const academicEditorsJobs = await Job.findAllByTeamMembers(
      academicEditors.map(academicEditor => academicEditor.id),
    )
    const editorialAssistantJobs = await Job.findAllByTeamMemberAndManuscript({
      teamMemberId: editorialAssistant.id,
      manuscriptId,
    })

    jobsService.cancelJobs(editorialAssistantJobs)
    jobsService.cancelJobs(academicEditorsJobs)

    const authors = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.author,
      manuscriptId: manuscript.id,
    })
    await Promise.each(
      authors,
      async author => (author.user = await User.find(author.userId)),
    )

    if (!manuscript.articleType.hasPeerReview) {
      notificationService.notifyAuthorsNoPeerReview({
        journal,
        authors,
        comment,
        manuscript,
        rejectEditor,
        editorialAssistant,
      })
    } else {
      const reviewers = await TeamMember.findAllByManuscriptAndRole({
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
      })

      const reviewersJobs = await Job.findAllByTeamMembers(
        reviewers.map(reviewer => reviewer.id),
      )
      jobsService.cancelJobs(reviewersJobs)

      const submittingReviewers = reviewers.filter(
        r => r.status === TeamMember.Statuses.submitted,
      )
      await Promise.each(
        submittingReviewers,
        async reviewer => (reviewer.user = await User.find(reviewer.userId)),
      )

      const triageEditor = await TeamMember.findTriageEditor({
        TeamRole: Team.Role,
        journalId: journal.id,
        manuscriptId: manuscript.id,
        sectionId: manuscript.sectionId,
      })

      const submittingAuthor = authors.find(a => a.isSubmitting)

      if (triageEditor && triageEditor.id !== rejectEditor.id) {
        triageEditor.user = await User.find(triageEditor.userId)

        notificationService.notifyTriageEditor({
          journal,
          manuscript,
          triageEditor,
          rejectEditor,
          submittingAuthor,
          editorialAssistant,
        })
      }

      if (!isEmpty(submittingReviewers)) {
        const academicEditorLabel = await manuscript.getEditorLabel({
          SpecialIssue,
          Journal,
          role: Team.Role.academicEditor,
        })
        notificationService.notifyReviewers({
          journal,
          manuscript,
          rejectEditor,
          submittingAuthor,
          editorialAssistant,
          academicEditorLabel,
          reviewers: submittingReviewers,
        })
        notificationService.notifyAuthorsAfterPeerReview({
          journal,
          authors,
          comment,
          manuscript,
          rejectEditor,
          editorialAssistant,
        })
      } else {
        notificationService.notifyAuthorsNoPeerReview({
          authors,
          journal,
          comment,
          manuscript,
          rejectEditor,
          editorialAssistant,
        })
      }

      if (
        !journal.peerReviewModel.approvalEditors.includes(
          Team.Role.academicEditor,
        )
      ) {
        notifyAcademicEditor({
          Team,
          Review,
          User,
          journal,
          manuscript,
          TeamMember,
          rejectEditor,
          editorialAssistant,
          notificationService,
          submittingAuthor: authors.find(a => a.isSubmitting),
        })
      }
    }

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionRejected',
    })

    logEvent({
      userId,
      manuscriptId,
      objectId: manuscriptId,
      objectType: logEvent.objectType.manuscript,
      action: logEvent.actions.manuscript_rejected,
    })
  },
})

const authsomePolicies = ['isAuthenticated', 'isApprovalEditor']

module.exports = {
  initialize,
  authsomePolicies,
}
