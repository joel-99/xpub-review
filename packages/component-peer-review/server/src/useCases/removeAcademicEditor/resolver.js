const config = require('config')
const models = require('@pubsweet/models')
const Email = require('@pubsweet/component-email-templating')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')

const useCases = require('./index')
const urlService = require('../../urlService/urlService')

const jobs = require('../../jobsService/jobsService')

const getProps = require('../../emailPropsService/emailPropsService')
const getEmailCopy = require('../../notifications/removeAcademicEditor/getEmailCopy')
const removeAcademicEditorNotifications = require('../../notifications/removeAcademicEditor/removeAcademicEditor')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const resolver = {
  Mutation: {
    async removeAcademicEditor(_, { teamMemberId }, ctx) {
      const jobsService = jobs.initialize({ models })

      const getEmailCopyService = getEmailCopy.initialize()
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const notificationService = removeAcademicEditorNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })

      return useCases.removeAcademicEditorUseCase
        .initialize({
          models,
          logEvent,
          jobsService,
          notificationService,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
  },
}

module.exports = resolver
