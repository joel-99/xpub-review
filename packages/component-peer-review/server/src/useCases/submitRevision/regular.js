const Promise = require('bluebird')

const initialize = ({ notificationService, models, eventsService }) => ({
  async execute({
    draft,
    beforeDraft,
    submittingAuthor,
    ManuscriptStatuses,
    editorialAssistant,
  }) {
    const { Journal, TeamMember, Team, User, ArticleType } = models
    const journal = await Journal.find(draft.journalId, 'teams.members')
    draft.articleType = await ArticleType.find(draft.articleTypeId)
    draft.journal = await Journal.find(draft.journalId, 'peerReviewModel')

    const approvalEditorRole = await TeamMember.getApprovalEditorRole({
      manuscript: draft,
      TeamRole: Team.Role,
    })

    if (approvalEditorRole !== Team.Role.academicEditor) {
      draft.updateStatus(ManuscriptStatuses.submitted)
      await draft.save()

      const triageEditor = await TeamMember.findTriageEditor({
        TeamRole: Team.Role,
        journalId: journal.id,
        manuscriptId: draft.id,
        sectionId: draft.sectionId,
      })
      triageEditor.user = await User.find(triageEditor.userId)

      notificationService.notifyTriageEditor({
        draft,
        journal,
        triageEditor,
        submittingAuthor,
        editorialAssistant,
      })
    } else {
      const pendingReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
        {
          role: Team.Role.reviewer,
          manuscriptId: beforeDraft.id,
          status: TeamMember.Statuses.pending,
        },
      )
      await Promise.each(pendingReviewers, pR => pR.delete())

      const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
        {
          manuscriptId: beforeDraft.id,
          role: Team.Role.academicEditor,
          status: TeamMember.Statuses.accepted,
        },
      )

      if (academicEditor) {
        draft.updateStatus(ManuscriptStatuses.academicEditorAssigned)
        await draft.save()
      } else {
        draft.updateStatus(ManuscriptStatuses.submitted)
        await draft.save()
      }

      notificationService.notifyAcademicEditor({
        draft,
        journal,
        academicEditor,
        submittingAuthor,
        editorialAssistant,
      })

      eventsService.publishSubmissionEvent({
        submissionId: draft.submissionId,
        eventName: 'SubmissionRevisionSubmitted',
      })
    }
  },
})

module.exports = { initialize }
