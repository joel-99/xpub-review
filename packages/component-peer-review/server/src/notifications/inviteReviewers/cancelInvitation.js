const { get } = require('lodash')

const initialize = ({
  Email,
  getPropsService,
  getEmailCopyService,
  getExpectedDate,
}) => ({
  async notifyReviewer({
    user,
    journal,
    manuscript,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { title } = manuscript
    const { name: journalName } = journal
    const titleText = `the manuscript titled "${title}" by ${submittingAuthor.getName()}`

    const emailType = 'reviewer-cancel-invitation'
    const editorialAssistantName = editorialAssistant.getName()
    const editorialAssistantEmail = get(editorialAssistant, 'alias.email')

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      emailType,
      titleText,
      journalName,
      editorialAssistantEmail,
      expectedDate: getExpectedDate({
        timestamp: new Date(),
        daysExpected: 14,
      }),
    })

    const emailProps = getPropsService.getProps({
      manuscript,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: user,
      subject: `${manuscript.customId}: Review no longer required`,
      paragraph,
      signatureName: editorialAssistantName,
      journalName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
