import { get, chain } from 'lodash'
import moment from 'moment'
import { useQuery } from 'react-apollo'
import { getSubmission } from '../graphql/queries'

const compareVersion = (m1, m2) => {
  let [v1, v2] = [m1.version, m2.version]
  if (typeof v1 !== 'string' || typeof v2 !== 'string') return false
  v1 = v1.split('.')
  v2 = v2.split('.')
  const k = Math.min(v1.length, v2.length)
  for (let i = 0; i < k; i += 1) {
    v1[i] = parseInt(v1[i], 10)
    v2[i] = parseInt(v2[i], 10)
    if (v1[i] > v2[i]) return 1
    if (v1[i] < v2[i]) return -1
  }
  if (v1.length === v2.length) return 0
  return v1.length < v2.length ? -1 : 1
}

const useManuscriptVersionData = ({ match }) => {
  const { data, loading, error } = useQuery(getSubmission, {
    variables: {
      submissionId: get(match, 'params.submissionId', ''),
    },
    fetchPolicy: 'cache-and-network',
  })

  const versions = chain(data)
    .get('getSubmission', [])
    .map(({ version, id }) => ({ version, id }))
    .value()

  const manuscript = chain(data)
    .get('getSubmission', [])
    .find(m => m.id === get(match, 'params.manuscriptId'))
    .value()

  const pendingAcademicEditor = get(manuscript, 'pendingAcademicEditor')

  const academicEditor =
    get(manuscript, 'academicEditor') || pendingAcademicEditor

  const role = get(manuscript, 'role')

  const reviews = get(manuscript, 'reviews') || []
  const reviewers = chain(manuscript)
    .get('reviewers')
    .toArray()
    .filter(reviewer => get(reviewer, 'status') !== 'expired')
    .map(reviewer => ({
      ...reviewer,
      review: reviews.find(
        review => review.member.user.id === reviewer.user.id,
      ),
    }))
    .value()
  const isLatestVersion =
    chain(data)
      .get('getSubmission', [])
      .sort(compareVersion)
      .last()
      .get('id')
      .value() === get(match, 'params.manuscriptId')
  const submittingAuthor = get(manuscript, 'authors', []).find(a =>
    get(a, 'isSubmitting'),
  )

  const authorResponse = get(manuscript, 'reviews', []).find(
    r => get(r, 'recommendation') === 'responseToRevision',
  )
  const editorialReviews = reviews
    .filter(r =>
      [
        'admin',
        'triageEditor',
        'academicEditor',
        'editorialAssistant',
        'researchIntegrityPublishingEditor',
      ].includes(get(r, 'member.role')),
    )
    .map(r => ({
      ...r,
      submittedTimestamp: moment(get(r, 'submitted', '')).valueOf(),
    }))
    .sort((a, b) => b.submittedTimestamp - a.submittedTimestamp)

  const reviewerReports = reviews.filter(
    review => review.member.role === 'reviewer',
  )
  const version = get(manuscript, 'version', '')
  const manuscriptStatus = get(manuscript, 'status', '')
  const allManuscripts = get(data, 'getSubmission', []).filter(Boolean)

  const reviewTypes = allManuscripts
    .flatMap(manuscript => manuscript.reviews)
    .map(review => review.member.role)

  const previousManuscriptReviews = get(
    allManuscripts[allManuscripts.length - 2],
    'reviews',
    [],
  )

  const hasPublishOption =
    version === '1'
      ? manuscriptStatus === 'reviewCompleted'
      : previousManuscriptReviews
          .map(review => review.recommendation)
          .some(elem => elem === 'major' || elem === 'minor') &&
        reviewTypes.includes('reviewer')

  const revisionDraft = get(data, 'getDraftRevision')
  const peerReviewModel = get(manuscript, 'specialIssue')
    ? get(manuscript, 'specialIssue.peerReviewModel')
    : get(manuscript, 'journal.peerReviewModel')
  return {
    role,
    error,
    reviews,
    loading,
    versions,
    reviewers,
    manuscript,
    revisionDraft,
    academicEditor,
    authorResponse,
    peerReviewModel,
    reviewerReports,
    isLatestVersion,
    submittingAuthor,
    editorialReviews,
    pendingAcademicEditor,
    hasPublishOption,
  }
}

export default useManuscriptVersionData
