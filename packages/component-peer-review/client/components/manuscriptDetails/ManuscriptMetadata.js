import React, { Fragment } from 'react'
import { H4 } from '@pubsweet/ui'
import { isEmpty, get } from 'lodash'
import { withProps, compose } from 'recompose'
import { Row, Text, ContextualBox } from '@hindawi/ui'

import { ManuscriptFileList } from './'

const ManuscriptMetadata = ({
  files,
  abstract,
  filesLabel,
  conflictOfInterest,
  dataAvailability,
  fundingStatement,
  hasAuthorDeclarations,
}) => (
  <Fragment>
    {!!abstract && (
      <ContextualBox
        data-test-id="abstract-tab"
        label="Abstract"
        mt={2}
        startExpanded
        transparent
      >
        <Text lineHeight="18px" whiteSpace="pre-wrap">
          {abstract}
        </Text>
      </ContextualBox>
    )}
    {hasAuthorDeclarations && (
      <ContextualBox
        data-test-id="conflict-of-interest-tab"
        label="Author Declaration"
        mt={2}
        transparent
      >
        <Row alignItems="center" justify="flex-start">
          <H4 mb={2} mt={2}>
            Conflicts of interest:
          </H4>
          <Text fontStyle={conflictOfInterest ? 'normal' : 'italic'} ml={2}>
            {conflictOfInterest || 'Not declared'}
          </Text>
        </Row>

        <Row alignItems="center" justify="flex-start">
          <H4 mb={2} mt={2}>
            Data availability statement:
          </H4>
          <Text fontStyle={dataAvailability ? 'normal' : 'italic'} ml={2}>
            {dataAvailability || 'Not declared'}
          </Text>
        </Row>

        <Row alignItems="center" justify="flex-start">
          <H4 mb={2} mt={2}>
            Funding statement:
          </H4>
          <Text fontStyle={fundingStatement ? 'normal' : 'italic'} ml={2}>
            {fundingStatement || 'Not declared'}
          </Text>
        </Row>
      </ContextualBox>
    )}

    {!isEmpty(files) && (
      <ContextualBox
        data-test-id="files-tab"
        label={filesLabel}
        mt={2}
        transparent
      >
        <ManuscriptFileList files={files} />
      </ContextualBox>
    )}
  </Fragment>
)

export default compose(
  withProps(({ manuscript }) => ({
    files: get(manuscript, 'files', ''),
    conflictOfInterest: get(manuscript, 'meta.conflictOfInterest'),
    dataAvailability: get(manuscript, 'meta.dataAvailability'),
    fundingStatement: get(manuscript, 'meta.fundingStatement'),
    abstract: get(manuscript, 'meta.abstract', 'abc'),
    filesLabel: `Files (${get(manuscript, 'files', []).length})`,
    articleType: get(manuscript, 'articleType'),
  })),
  withProps(({ articleType }) => ({
    hasAuthorDeclarations:
      get(articleType, 'name') !== 'Erratum' &&
      get(articleType, 'name') !== 'Corrigendum' &&
      get(articleType, 'name') !== 'Retraction' &&
      get(articleType, 'name') !== 'Expression of Concern',
  })),
)(ManuscriptMetadata)
