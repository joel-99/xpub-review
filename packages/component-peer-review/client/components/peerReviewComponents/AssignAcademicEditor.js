import React, { Fragment, useState, useEffect } from 'react'
import { get } from 'lodash'
import { space } from 'styled-system'
import styled from 'styled-components'
import { Modal } from 'component-modal'
import { Button, TextField } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import {
  Row,
  Item,
  Text,
  Icon,
  Label,
  Loader,
  MultiAction,
  ContextualBox,
} from '@hindawi/ui'
import { useLazyQuery } from 'react-apollo'
import { getAcademicEditors } from '../../graphql/queries'
import { useAssignAcademicEditor } from '..'

const AssignAcademicEditor = ({
  role,
  match,
  toggle,
  expanded,
  manuscript,
  academicEditorLabel,
}) => {
  const [getAcademicEditorsMutation, { data, loading }] = useLazyQuery(
    getAcademicEditors,
    { fetchPolicy: 'network-only' },
  )
  const academicEditors = get(data, 'getAcademicEditors', [])

  const { inviteAcademicEditor } = useAssignAcademicEditor({
    role,
    match,
    manuscript,
  })

  const [searchValue, setSearchValue] = useState('')
  const [inputValue, setInputValue] = useState(searchValue)
  const clearSearch = () => {
    setSearchValue('')
    setInputValue('')
  }
  useEffect(() => {
    getAcademicEditorsMutation({
      variables: {
        searchValue,
        manuscriptId: manuscript.id,
      },
    })
  }, [searchValue, manuscript, getAcademicEditorsMutation])

  const onInvite = ({
    name,
    email,
    searchIndex,
    ...academicEditor
  }) => modalProps => inviteAcademicEditor(academicEditor, modalProps)

  const filteredAcademicEditors = academicEditors.map(academicEditor => {
    const givenNames = get(academicEditor, 'alias.name.givenNames', '')
    const surname = get(academicEditor, 'alias.name.surname', '')
    const email = get(academicEditor, 'alias.email', '')
    return {
      ...academicEditor,
      email,
      name: `${givenNames} ${surname}`,
      searchIndex: `${givenNames} ${surname} ${email}`,
    }
  })
  const academicEditorStatus = get(manuscript, 'academicEditor.status', '')
  const pendingAcademicEditor = get(manuscript, 'pendingAcademicEditor', '')
  const assignedEditorId = get(manuscript, 'academicEditor.user.id', '')

  const canReassign =
    academicEditorStatus && academicEditorStatus === 'accepted'

  const pendingApprovalLabel = () =>
    pendingAcademicEditor && (
      <Row alignItems="baseline" fitContent justify="flex-end">
        <PendingLabel invited mr={6}>
          Pending Response
        </PendingLabel>
      </Row>
    )

  return (
    <ContextualBox
      expanded={expanded}
      height={8}
      highlight={!canReassign}
      label={
        canReassign
          ? `Reassign ${academicEditorLabel}`
          : `Assign ${academicEditorLabel}`
      }
      mt={4}
      rightChildren={pendingApprovalLabel}
      toggle={toggle}
    >
      <Root>
        <TextContainer>
          <TextField
            data-test-id="manuscript-assign-academic-editor-filter"
            inline
            onChange={e => setInputValue(e.target.value)}
            onKeyPress={e =>
              e.key === 'Enter' && setSearchValue(e.target.value.toLowerCase())
            }
            placeholder="Search by name or email"
            value={inputValue}
          />
          {searchValue !== '' && (
            <StyledIcon
              data-test-id="clear-filter"
              fontSize="16px"
              icon="remove"
              onClick={clearSearch}
            />
          )}
        </TextContainer>

        {loading && (
          <Row>
            <Loader mb={2} />
          </Row>
        )}
        {!loading && filteredAcademicEditors.length === 0 && (
          <Row alignItems="baseline" justify="flex-start" pl={2}>
            <Icon color="colorError" icon="warning" pr={1} />
            <Text medium secondary>
              No {academicEditorLabel}s have been found.
            </Text>
          </Row>
        )}
        {!loading && filteredAcademicEditors.length > 0 && (
          <Fragment>
            <Row height={6} pl={4}>
              <Item alignItems="center" flex={1}>
                <Label>Full name</Label>
              </Item>
              <Item alignItems="center" flex={2}>
                <Label>Email</Label>
              </Item>
              <Item alignItems="center" flex={1} />
            </Row>

            {filteredAcademicEditors.map((academicEditor, index) => (
              <CustomRow
                data-test-id={`manuscript-assign-academic-editor-invite-${academicEditor.id}`}
                height={8}
                isFirst={index === 0}
                isLast={index === filteredAcademicEditors.length - 1}
                key={academicEditor.id}
                pl={4}
              >
                {assignedEditorId === academicEditor.user.id ? (
                  <Fragment>
                    <WithEllipsis>
                      <Text ellipsis secondary title={academicEditor.name}>
                        {academicEditor.name}
                      </Text>
                    </WithEllipsis>
                    <Item flex={2}>
                      <Text secondary>{academicEditor.email}</Text>
                    </Item>
                    <Item flex={1} justify="flex-end">
                      <CustomAssignedButton disabled mr={6} xs>
                        Assigned
                      </CustomAssignedButton>
                    </Item>
                  </Fragment>
                ) : (
                  <Fragment>
                    <WithEllipsis>
                      <Text ellipsis title={academicEditor.name}>
                        {academicEditor.name}
                      </Text>
                    </WithEllipsis>
                    <Item flex={2}>
                      <Text>{academicEditor.email}</Text>
                    </Item>

                    {pendingAcademicEditor &&
                    pendingAcademicEditor.user.id === academicEditor.user.id ? (
                      <Item flex={1} justify="flex-end">
                        <PendingLabel invited mr={6}>
                          Pending Response
                        </PendingLabel>
                      </Item>
                    ) : (
                      <Item flex={1} justify="flex-end">
                        <Modal
                          cancelText="CANCEL"
                          component={MultiAction}
                          confirmText={canReassign ? 'Reassign' : 'Invite'}
                          modalKey={`${academicEditor.id}-inviteAcademicEditor`}
                          onConfirm={onInvite(academicEditor)}
                          subtitle={
                            canReassign
                              ? `Are you sure you want to assign as ${academicEditorLabel}?`
                              : academicEditor.name
                          }
                          title={
                            canReassign
                              ? academicEditor.name
                              : 'Confirm Invitation'
                          }
                        >
                          {showModal => (
                            <CustomButton mr={6} onClick={showModal} xs>
                              Invite
                            </CustomButton>
                          )}
                        </Modal>
                      </Item>
                    )}
                  </Fragment>
                )}
              </CustomRow>
            ))}
          </Fragment>
        )}
      </Root>
    </ContextualBox>
  )
}

export default AssignAcademicEditor

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;

  ${space};
`
const PendingLabel = styled(Text)`
  font-size: ${th('text.fontSizeBaseSmall')};
  font-family: ${th('defaultFont')};
  font-weight: 700;
  text-transform: upperCase;
`
const WithEllipsis = styled(Item)`
  display: inline-grid;
`
const TextContainer = styled.div`
  margin: calc(${th('gridUnit')} * 2);
  margin-bottom: calc(${th('gridUnit')} * 4);
  position: relative;
  width: calc(${th('gridUnit')} * 80);
`

const CustomAssignedButton = styled(Button)`
  pointer-events: none;
  :disabled {
    background-color: ${th('actionSecondaryColor')};
    border-color: ${th('actionSecondaryColor')};
    color: ${th('backgroundColor')};
  }
`

const CustomButton = styled(Button)`
  height: calc(${th('gridUnit')} * 6);
  opacity: 0;
  line-height: calc(${th('gridUnit')} * 4);
  background-color: #000;
  border-color: #000;
  color: ${th('white')};

  &:hover {
    background-color: ${th('actionSecondaryColor')};
    border-color: ${th('actionSecondaryColor')};
    color: ${th('backgroundColor')};
  }
`

const CustomRow = styled(Row)`
  border-top: ${props => props.isFirst && th('box.border')};
  border-bottom: ${props => !props.isLast && th('box.border')};

  &:hover {
    background-color: #eee;
    border-bottom-left-radius: ${props => props.isLast && '3px'};
    border-bottom-right-radius: ${props => props.isLast && '3px'};

    ${CustomButton} {
      opacity: 1;
    }
  }
`

const StyledIcon = styled(Icon)`
  position: absolute;
  top: 7px;
  right: 8px;
  color: ${({ disabled }) =>
    disabled ? th('colorBorder') : th('textPrimaryColor')};
  cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};
`
// #endregion
