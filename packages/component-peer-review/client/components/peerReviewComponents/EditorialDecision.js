import React, { Fragment } from 'react'
import { get } from 'lodash'
import { Formik } from 'formik'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withModal } from 'component-modal'
import { compose, withHandlers, withProps, fromRenderProps } from 'recompose'

import {
  Row,
  Item,
  Menu,
  Label,
  Textarea,
  validators,
  MultiAction,
  RemoteOpener,
  ContextualBox,
  ValidatedFormField,
} from '@hindawi/ui'

const MessageForm = ({ message }) => (
  <Row>
    <Item vertical>
      <Label mb={2} required>
        {message}
      </Label>
      <ValidatedFormField
        component={Textarea}
        data-test-id="triage-editor-decision-message"
        name="message"
        validate={[validators.required]}
      />
    </Item>
  </Row>
)

const SubmitButton = ({ handleSubmit, values }) => (
  <Row justify="flex-end" mb={4}>
    <StyledButton
      data-test-id="submit-triage-editor-decision"
      height={8}
      onClick={handleSubmit}
      primary
      width={34}
    >
      Submit decision
    </StyledButton>
  </Row>
)

const EditorialDecision = ({
  initialValues,
  triageEditorExpanded,
  academicEditorLabel,
  decisions,
  toggleTriageEditor,
  onSubmit,
  validate,
  highlight,
}) => (
  <Formik initialValues={initialValues} onSubmit={onSubmit} validate={validate}>
    {({ handleSubmit, values }) => (
      <ContextualBox
        expanded={triageEditorExpanded}
        highlight={highlight}
        label="Your Editorial Decision"
        mt={4}
        toggle={toggleTriageEditor}
      >
        <Root>
          <Row width={62}>
            <Item mb={2} vertical>
              <Label mb={2} required>
                Decision
              </Label>
              <ValidatedFormField
                component={Menu}
                data-test-id="triage-editor-decision-menu"
                inline
                name="decision"
                options={decisions}
                placeholder="Please select"
                validate={[validators.required]}
              />
            </Item>
          </Row>
          <Fragment>
            {(values.decision === 'revision' ||
              values.decision === 'reject') && (
              <MessageForm message="Comments for Author" />
            )}

            {values.decision === 'return-to-academic-editor' && (
              <MessageForm message={`Comments for ${academicEditorLabel}`} />
            )}

            {values.decision && (
              <SubmitButton handleSubmit={handleSubmit} values={values} />
            )}
          </Fragment>
        </Root>
      </ContextualBox>
    )}
  </Formik>
)

const filteredDecisions = ({ manuscriptStatus, options, hasPeerReview }) => {
  switch (manuscriptStatus) {
    case 'submitted': {
      return options.filter(
        hasPeerReview
          ? ({ value }) => ['revision', 'reject'].includes(value)
          : ({ value }) => ['publish', 'reject'].includes(value),
      )
    }
    case 'academicEditorAssigned': {
      return options.filter(({ value }) =>
        ['revision', 'reject'].includes(value),
      )
    }
    case 'academicEditorInvited': {
      return options.filter(({ value }) => value === 'reject')
    }
    case 'makeDecision': {
      return options.filter(({ value }) =>
        hasPeerReview
          ? ['publish', 'revision', 'reject'].includes(value)
          : ['publish', 'reject'].includes(value),
      )
    }
    case 'pendingApproval': {
      return options.filter(({ value }) =>
        ['publish', 'return-to-academic-editor', 'reject'].includes(value),
      )
    }
    default:
      return options.filter(
        hasPeerReview
          ? ({ value }) => value === 'reject'
          : ({ value }) => ['publish', 'reject'].includes(value),
      )
  }
}

export default compose(
  withProps(({ options, manuscriptStatus, hasPeerReview }) => ({
    initialValues: { decision: '', message: '' },
    decisions: filteredDecisions({
      manuscriptStatus,
      options,
      hasPeerReview,
    }),
  })),
  withModal({
    component: MultiAction,
    modalKey: 'editorialDecision',
  }),
  fromRenderProps(RemoteOpener, ({ toggle, expanded }) => ({
    toggleTriageEditor: toggle,
    triageEditorExpanded: expanded,
  })),
  withHandlers({
    onSubmit: ({ onSubmit, showModal, options, toggleTriageEditor }) => (
      values,
      formikBag,
    ) => {
      const { title, subtitle, confirmButton } = options.find(
        ({ value }) => value === get(values, 'decision'),
      )
      showModal({
        cancelText: 'Cancel',
        confirmText: confirmButton,
        onConfirm: modalProps => {
          onSubmit(values, modalProps, formikBag).then(() => {
            formikBag.resetForm({})
            toggleTriageEditor()
          })
        },
        subtitle,
        title,
      })
    },
  }),
)(EditorialDecision)

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  padding: calc(${th('gridUnit')} * 4);
  padding-bottom: 0;
`
const StyledButton = styled(Button)`
  height: calc(${th('gridUnit')} * 8);
  line-height: normal;
`
