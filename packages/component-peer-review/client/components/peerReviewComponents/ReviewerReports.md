Reviewer Reports

```jsx
import ContextualBox from '../ContextualBox'
const Chance = require('chance')
const basicReportReview = () => ({
  id: chance.guid(),
  comments: [
    {
      id: chance.guid(),
      content: chance.sentence(),
      type: 'public',
      files: [
        {
          id: chance.guid(),
          originalName: `${chance.word()}.docx`,
          mimeType: 'application/pdf',
          size: chance.d100(),
        },
      ],
    },
    {
      id: chance.guid(),
      content: chance.sentence(),
      type: 'private',
      files: [],
    },
  ],
  created: chance.natural(),
  member: {
    alias: { name: { surname: chance.last(), givenNames: chance.first() } },
    role: 'reviewer',
    user: { id: chance.guid() },
    reviewerNumber: chance.d20(),
  },
  open: true,
  recommendation: chance.pickone(['publish', 'minor', 'major', 'reject']),
  submitted: chance.timestamp(),
})
const reviewerReports = []
reviewerReports.push(basicReportReview())
reviewerReports.push(basicReportReview())
reviewerReports.push(basicReportReview())

const options = [
  {
    label: 'Publish Unaltered',
    message: 'Recommend to Publish',
    value: 'publish',
  },
  {
    label: 'Consider after minor revision',
    message: 'Minor Revision Requested',
    value: 'minor',
  },
  {
    label: 'Major revision',
    message: 'Major Revision Requested',
    value: 'major',
  },
  {
    label: 'Reject',
    message: 'Recommend to Reject',
    value: 'reject',
  },
]
;<ContextualBox startExpanded label="Reviewer Reports">
  <ReviewerReports
    reviewerReports={reviewerReports}
    options={options}
    onPreview={() => console.log('clicked preview')}
    onDelete={() => console.log('deleted')}
  />
</ContextualBox>
```

No reviewer reports to display.

```jsx
import ContextualBox from '../ContextualBox'
const reviewerReports = []

const options = [
  {
    label: 'Publish Unaltered',
    message: 'Recommend to Publish',
    value: 'publish',
  },
  {
    label: 'Consider after minor revision',
    message: 'Minor Revision Requested',
    value: 'minor',
  },
  {
    label: 'Major revision',
    message: 'Major Revision Requested',
    value: 'major',
  },
  {
    label: 'Reject',
    message: 'Recommend to Reject',
    value: 'reject',
  },
]
;<ContextualBox startExpanded label="Reviewer Reports">
  <ReviewerReports
    reviewerReports={reviewerReports}
    options={options}
    onPreview={() => console.log('clicked preview')}
    onDelete={() => console.log('deleted')}
  />
</ContextualBox>
```
