export const publisher = {
  links: {
    websiteLink: 'https://new.hindawi.com/',
    privacyLink: 'https://new.hindawi.com/privacy/',
    termsLink: 'https://new.hindawi.com/terms/',
    ethicsLink: 'https://new.hindawi.com/ethics/',
    coiLink: 'https://new.hindawi.com/ethics/#conflicts-of-interest',
    apcLink: 'https://new.hindawi.com/journals/{journalCode}/apc',
    guidelinesLink:
      'https://new.hindawi.com/journals/{journalCode}/guidelines/ ',
  },
}

export const journal = {
  statuses: [],
}
