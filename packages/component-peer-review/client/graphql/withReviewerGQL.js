import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import * as mutations from './mutations'
import { refetchGetSubmission } from '../graphql/refetchQueries'

export default compose(
  graphql(mutations.acceptReviewerInvitation, {
    name: 'acceptReviewerInvitation',
    options: ({ match }) => ({
      refetchQueries: [refetchGetSubmission(match)],
    }),
  }),
  graphql(mutations.declineReviewerInvitation, {
    name: 'declineReviewerInvitation',
    options: ({ match, currentUser, data }) => {
      if (!currentUser) return {}
      return data.getSubmission && data.getSubmission.length > 1
        ? [refetchGetSubmission(match)]
        : []
    },
  }),
  graphql(mutations.updateDraftReview, {
    name: 'updateDraftReview',
  }),
  graphql(mutations.updateAutosaveReview, {
    name: 'updateAutosaveReview',
  }),
  graphql(mutations.submitReview, {
    name: 'submitReview',
  }),
)
