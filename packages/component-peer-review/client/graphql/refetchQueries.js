import { queries as activityLogQueries } from 'component-activity-log/client'
import * as queries from './queries'

export const refetchGetSubmission = ({ params: { submissionId = '' } }) => ({
  query: queries.getSubmission,
  variables: { submissionId },
})

export const refetchGetAuditLogEvents = ({
  params: { manuscriptId = '' },
}) => ({
  query: activityLogQueries.getAuditLogEvents,
  variables: { manuscriptId },
})

export const refetchLoadReviewerSuggestions = ({
  params: { manuscriptId = '' },
}) => ({
  query: queries.loadReviewerSuggestions,
  variables: { manuscriptId },
})
export const refetchGetTriageEditors = ({ params: { manuscriptId = '' } }) => ({
  query: queries.getTriageEditors,
  variables: { manuscriptId },
})
