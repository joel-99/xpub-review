import gql from 'graphql-tag'

export const teamMember = gql`
  fragment teamMember on TeamMember {
    id
    isSubmitting
    isCorresponding
    invited
    responded
    reviewerNumber
    user {
      id
    }
    status
    alias {
      aff
      email
      country
      name {
        surname
        givenNames
      }
    }
  }
`
export const specialIssueFragment = gql`
  fragment specialIssue on SpecialIssue {
    id
    name
    isCancelled
  }
`

export const fileFragment = gql`
  fragment manuscriptDetailsFile on File {
    id
    type
    size
    filename
    originalName
    mimeType
  }
`

export const articleTypeFragment = gql`
  fragment articleTypeDetails on ArticleType {
    id
    name
    hasPeerReview
  }
`

export const reviewDetails = gql`
  fragment reviewDetails on Review {
    id
    member {
      role
      user {
        id
      }
      alias {
        name {
          surname
          givenNames
        }
      }
      reviewerNumber
    }
    created
    comments {
      id
      type
      content
      files {
        ...manuscriptDetailsFile
      }
    }
    recommendation
    open
    submitted
  }
  ${fileFragment}
`

export const manuscriptFragment = gql`
  fragment manuscriptDetails on Manuscript {
    id
    status
    submissionId
    journalId
    submissionId
    created
    visibleStatus
    inProgress
    journal {
      name
      code
      peerReviewModel {
        name
        hasTriageEditor
        hasFigureheadEditor
        triageEditorLabel
        academicEditorLabel
        approvalEditors
        figureheadEditorLabel
      }
      triageEditor {
        ...teamMember
      }
    }
    triageEditor {
      ...teamMember
    }
    isApprovalEditor
    specialIssue {
      id
      peerReviewModel {
        academicEditorLabel
        triageEditorLabel
        hasTriageEditor
      }
    }
    authors {
      id
      isSubmitting
      isCorresponding
      alias {
        aff
        name {
          surname
          givenNames
        }
        email
        country
      }
    }
    meta {
      title
      abstract
      dataAvailability
      fundingStatement
      conflictOfInterest
    }
    version
    technicalCheckToken
    role
    customId
    articleType {
      ...articleTypeDetails
    }
    files {
      ...manuscriptDetailsFile
    }
    specialIssue {
      ...specialIssue
    }
    reviews {
      ...reviewDetails
    }
    academicEditor {
      ...teamMember
    }
    pendingAcademicEditor {
      ...teamMember
    }
    reviewers {
      ...teamMember
    }
    researchIntegrityPublishingEditor {
      ...teamMember
    }
  }
  ${specialIssueFragment}
  ${teamMember}
  ${fileFragment}
  ${reviewDetails}
  ${articleTypeFragment}
`

export const userFragment = gql`
  fragment manuscriptDetailsUser on User {
    id
    isActive
    role
    identities {
      ... on Local {
        name {
          surname
          givenNames
          title
        }
        email
        aff
        isConfirmed
        country
      }
    }
  }
`
