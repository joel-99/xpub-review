process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { requestQualityChecksUseCase } = require('../src/useCases')

const logEvent = jest.fn(async () => {})
logEvent.actions = {
  invitation_agreed: 'invitation_agreed',
}
logEvent.objectType = { manuscript: 'manuscript' }

describe('Request quality checks use case', () => {
  let peerReviewModel
  let journal
  beforeAll(async () => {
    const { PeerReviewModel, Journal, Team } = models
    peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
  })
  it('Should update last manuscript status to qualityChecksRequested', async () => {
    const { Manuscript, Team } = models
    const manuscriptVersion1 = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.inQA,
        version: '1',
      },
      Manuscript,
    })
    dataService.createUserOnManuscript({
      models,
      manuscript: manuscriptVersion1,
      fixtures,
      role: Team.Role.author,
    })

    const manuscriptVersion2 = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.inQA,
        version: '2',
        submissionId: manuscriptVersion1.submissionId,
      },
      Manuscript,
    })
    dataService.createUserOnManuscript({
      models,
      manuscript: manuscriptVersion2,
      fixtures,
      role: Team.Role.author,
    })

    await requestQualityChecksUseCase
      .initialize({ models, logEvent })
      .execute({ submissionId: manuscriptVersion1.submissionId })

    expect(manuscriptVersion2.status).toEqual(
      Manuscript.Statuses.qualityChecksRequested,
    )
    expect(logEvent).toHaveBeenCalledTimes(1)
  })
  it('Should create a new draft version', async () => {
    const { Manuscript, Team } = models
    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.inQA,
        version: '1',
      },
      Manuscript,
    })
    dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })

    await requestQualityChecksUseCase
      .initialize({ models, logEvent })
      .execute({ submissionId: manuscript.submissionId })

    const manuscripts = fixtures.manuscripts.filter(
      m => m.submissionId === manuscript.submissionId,
    )
    const lastManuscript = manuscripts[manuscripts.length - 1]

    expect(lastManuscript.version).toEqual(
      (+manuscript.version + 0.1).toString(),
    )
    expect(lastManuscript.status).toEqual(Manuscript.Statuses.draft)
  })
})
