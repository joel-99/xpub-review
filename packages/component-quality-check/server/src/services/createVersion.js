const uuid = require('uuid')
const { s3service } = require('component-files/server')
const { Promise } = require('bluebird')

module.exports = {
  async createVersion({
    models: { Manuscript, Review, Comment, File, Team, TeamMember },
    submissionId,
  }) {
    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    })

    const newManuscript = new Manuscript({
      ...lastManuscript,
      created: new Date().toISOString(),
      updated: new Date().toISOString(),
      version: newVersion(lastManuscript.version),
      status: Manuscript.Statuses.draft,
      isPostAcceptance: true,
    })
    delete newManuscript.id
    await newManuscript.save()

    const files = await File.findBy({ manuscriptId: lastManuscript.id })
    await uploadNewFiles({
      File,
      files,
      newManuscriptId: newManuscript.id,
    })

    const newTriageEditorTeam = new Team({
      role: Team.Role.triageEditor,
      manuscriptId: newManuscript.id,
    })
    await newTriageEditorTeam.save()

    const newAuthorTeam = new Team({
      role: Team.Role.author,
      manuscriptId: newManuscript.id,
    })
    await newAuthorTeam.save()

    const newAcademicEditorTeam = new Team({
      role: Team.Role.academicEditor,
      manuscriptId: newManuscript.id,
    })
    await newAcademicEditorTeam.save()

    const manuscriptId = lastManuscript.id
    const existingAuthors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
    })
    const existingAcademicEditors = await TeamMember.findAllByManuscriptAndRole(
      {
        manuscriptId,
        role: Team.Role.academicEditor,
      },
    )
    const existingTriageEditors = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.triageEditor,
      manuscriptId,
    })

    await Promise.each(existingAuthors, existingAuthor => {
      const newAuthor = new TeamMember({
        ...existingAuthor,
        created: new Date().toISOString(),
        updated: new Date().toISOString(),
        teamId: newAuthorTeam.id,
      })
      delete newAuthor.id
      return newAuthor.save()
    })

    await Promise.each(existingAcademicEditors, existingAcademicEditor => {
      const newAcademicEditor = new TeamMember({
        ...existingAcademicEditor,
        created: new Date().toISOString(),
        updated: new Date().toISOString(),
        teamId: newAcademicEditorTeam.id,
      })
      delete newAcademicEditor.id
      return newAcademicEditor.save()
    })

    await Promise.each(existingTriageEditors, existingTriageEditor => {
      const newTriageEditor = new TeamMember({
        ...existingTriageEditor,
        created: new Date().toISOString(),
        updated: new Date().toISOString(),
        teamId: newTriageEditorTeam.id,
      })
      delete newTriageEditor.id
      return newTriageEditor.save()
    })
  },
}

const uploadNewFiles = async ({ files, File, newManuscriptId }) => {
  files.map(async file => {
    const newKey = `${newManuscriptId}/${uuid.v4()}`
    await s3service.copyObject({ prevKey: file.providerKey, newKey })

    const newFile = new File({
      ...file,
      providerKey: newKey,
      manuscriptId: newManuscriptId,
      created: new Date().toISOString(),
      updated: new Date().toISOString(),
    })
    delete newFile.id
    return newFile.save()
  })
}

const newVersion = v => {
  const [major, minor = 0] = v.split('.')
  return `${major}.${+minor + 1}`
}
