const initialize = ({ models: { Manuscript }, logEvent, eventsService }) => ({
  async execute({ input: { submissionId }, userId }) {
    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      order: 'desc',
      orderByField: 'version',
      submissionId,
    })
    await manuscripts[0]
      .updateStatus(Manuscript.Statuses.qualityChecksSubmitted)
      .save()

    const lastManuscript = manuscripts[1]
    await lastManuscript.updateStatus(Manuscript.Statuses.olderVersion).save()

    logEvent({
      userId,
      manuscriptId: lastManuscript.id,
      action: logEvent.actions.quality_checks_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: lastManuscript.id,
    })

    eventsService.publishSubmissionEvent({
      submissionId: lastManuscript.submissionId,
      eventName: 'SubmissionQualityChecksSubmitted',
    })
  },
})

module.exports = {
  initialize,
  authsomePolicies: [],
}
