const initialize = ({
  models: { Manuscript },
  sendPackage,
  eventsService,
}) => ({
  async execute({ data }) {
    const { submissionId, qc, qcLeaders = [], es, esLeaders = [] } = data

    const screeners = [
      ...qcLeaders.map(e => ({ ...e, role: 'qcLeader' })),
      ...esLeaders.map(e => ({ ...e, role: 'esLeader' })),
      ...(qc ? [{ ...qc, role: 'qc' }] : []),
      ...(es ? [{ ...es, role: 'es' }] : []),
    ]

    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
      eagerLoadRelations: `[
           articleType,
           journal.[teams.members.team,peerReviewModel],
           files,
           section,
           teams.members.user.identities,
           reviews.[
             comments.files,
             member.team
            ],
           specialIssue.section
          ]`,
    })
    lastManuscript.updateProperties({
      status: Manuscript.Statuses.published,
    })
    await lastManuscript.save()

    await sendPackage({
      manuscript: lastManuscript,
      isForProduction: true,
      screeners,
    })

    eventsService.publishSubmissionEvent({
      submissionId: lastManuscript.submissionId,
      eventName: 'SubmissionPackageCreated',
    })
  },
})

module.exports = {
  initialize,
  authsomePolicies: [],
}
