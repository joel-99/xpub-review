const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

const { reviewerSuggestions } = fixtures
const { findOneByMock, findByMock } = require('./repositoryMocks')

class ReviewerSuggestion {
  constructor(props) {
    this.id = chance.guid()
    this.created = props.create || Date.now()
    this.updated = props.updated || Date.now()
    this.aff = props.aff
    this.email = props.email
    this.surname = props.surname
    this.givenNames = props.givenNames
    this.manuscriptId = props.manuscriptId
    this.numberOfReviews = props.numberOfReviews
  }

  static findOneBy = values =>
    findOneByMock(values, 'reviewerSuggestions', fixtures)
  static findBy = values => findByMock(values, 'reviewerSuggestions', fixtures)

  async save() {
    reviewerSuggestions.push(this)
    return Promise.resolve(this)
  }

  toDTO() {
    return {
      ...this,
    }
  }
}

module.exports = ReviewerSuggestion
