const { assign, filter } = require('lodash')
const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

const { specialIssues } = fixtures
const { findMock, findAllMock, findOneByMock } = require('./repositoryMocks')

class SpecialIssue {
  constructor(props) {
    this.id = chance.guid()
    this.name = props.name || chance.sentence({ words: 5 })
    this.created = props.created || Date.now()
    this.startDate = props.activationDate || Date.now()
    this.endDate = props.endDate || Date.now()
    this.isActive = props.isActive === undefined ? false : props.isActive
    this.journalId = props.journalId || ''
    this.sectionId = props.sectionId || ''
    this.callForPapers = props.callForPapers || ''
    this.isCancelled = props.isCancelled || false
    this.cancelReason = props.cancelReason || null
    this.customId = chance.integer({ min: 100000, max: 999999 }).toString()
    this.peerReviewModelId = props.peerReviewModelId || null
  }

  static find = id => findMock(id, 'specialIssues', fixtures)
  static findOneBy = values => findOneByMock(values, 'specialIssues', fixtures)
  static findAll = ({ orderByField, order, queryObject }) =>
    findAllMock('specialIssues', fixtures, orderByField, order, queryObject)

  static findUnique = name => {
    const foundSpecialIssue = specialIssues.find(
      si => si.name.toLowerCase() === name.toLowerCase(),
    )

    return Promise.resolve(foundSpecialIssue)
  }

  static async findAllByJournal({ journalId }) {
    const specialIssues = filter(fixtures.specialIssues, { journalId })

    return specialIssues
  }
  static async findAllBySection({ sectionId }) {
    const specialIssues = filter(fixtures.specialIssues, { sectionId })

    return specialIssues
  }
  async save() {
    const existingSpecialIssue = specialIssues.find(si => si.id === this.id)

    if (existingSpecialIssue) {
      assign(existingSpecialIssue, this)
    } else {
      specialIssues.push(this)
    }
    return Promise.resolve(this)
  }

  updateProperties(properties) {
    assign(this, properties)
    return this
  }

  toDTO() {
    return {
      ...this,
    }
  }
}

module.exports = SpecialIssue
