const {
  map,
  get,
  pick,
  last,
  chain,
  assign,
  remove,
  sortBy,
  filter,
  orderBy,
  groupBy,
} = require('lodash')
const Chance = require('chance')

const config = require('config')

const statuses = config.get('statuses')
const chance = new Chance()

const fixtures = require('../fixtures')

const { manuscripts } = fixtures
const {
  allMock,
  findMock,
  findByMock,
  findOneByMock,
  findByFieldMock,
} = require('./repositoryMocks')

const Review = require('./review')

class Manuscript {
  constructor(props) {
    this.id = chance.guid()
    this.journalId = props.journalId || null
    this.submissionId = props.submissionId || chance.guid()
    this.created = props.created || new Date()
    this.updated = props.updated || new Date()
    this.status = props.status || 'draft'
    this.customId = props.customId || null
    this.version = props.version || '1'
    this.title = props.title || chance.sentence()
    this.abstract = props.abstract || chance.paragraph()
    this.publicationDates = props.publicationDates || []
    this.technicalCheckToken = props.technicalCheckToken || null
    this.hasPassedEqa = props.hasPassedEqa
    this.hasPassedEqs = props.hasPassedEqs
    this.agreeTc = props.agreeTc || null
    this.files = props.files || []
    this.teams = props.teams || []
    this.reviews = props.reviews || []
    this.logs = props.logs || []
    this.journal = props.journal || {}
    this.articleTypeId = props.articleTypeId || null
    this.articleType = props.articleType || {}
    this.section = props.section || null
    this.sectionId = props.sectionId || null
    this.specialIssueId = props.specialIssueId || null
    this.specialIssue = props.specialIssue || null
    this.isPostAcceptance = props.isPostAcceptance || false
  }

  static get Statuses() {
    return {
      draft: 'draft',
      technicalChecks: 'technicalChecks',
      submitted: 'submitted',
      academicEditorInvited: 'academicEditorInvited',
      academicEditorAssigned: 'academicEditorAssigned',
      reviewersInvited: 'reviewersInvited',
      underReview: 'underReview',
      reviewCompleted: 'reviewCompleted',
      revisionRequested: 'revisionRequested',
      pendingApproval: 'pendingApproval',
      rejected: 'rejected',
      inQA: 'inQA',
      accepted: 'accepted',
      withdrawalRequested: 'withdrawalRequested',
      withdrawn: 'withdrawn',
      deleted: 'deleted',
      published: 'published',
      olderVersion: 'olderVersion',
      academicEditorAssignedEditorialType:
        'academicEditorAssignedEditorialType',
      makeDecision: 'makeDecision',
      qualityChecksRequested: 'qualityChecksRequested',
      qualityChecksSubmitted: 'qualityChecksSubmitted',
      refusedToConsider: 'refusedToConsider',
    }
  }

  static get NonRejectableStatuses() {
    const statuses = this.Statuses
    return [
      statuses.draft,
      statuses.deleted,
      statuses.rejected,
      statuses.inQA,
      statuses.accepted,
      statuses.qualityChecksRequested,
      statuses.qualityChecksSubmitted,
      statuses.published,
      statuses.withdrawn,
      statuses.withdrawalRequested,
      statuses.olderVersion,
      statuses.refusedToConsider,
    ]
  }
  static get InProgressStatuses() {
    const statuses = this.Statuses
    return [
      statuses.academicEditorInvited,
      statuses.academicEditorAssigned,
      statuses.reviewersInvited,
      statuses.underReview,
      statuses.reviewCompleted,
      statuses.revisionRequested,
      statuses.pendingApproval,
      statuses.makeDecision,
    ]
  }

  static compareVersion(m1, m2) {
    let v1 = m1.version
    let v2 = m2.version
    if (typeof v1 !== 'string' || typeof v2 !== 'string') return false
    v1 = v1.split('.')
    v2 = v2.split('.')
    const k = Math.min(v1.length, v2.length)
    for (let i = 0; i < k; i += 1) {
      v1[i] = parseInt(v1[i], 10)
      v2[i] = parseInt(v2[i], 10)
      if (v1[i] > v2[i]) return 1
      if (v1[i] < v2[i]) return -1
    }
    if (v1.length === v2.length) return 0
    return v1.length < v2.length ? -1 : 1
  }

  static findByCustomId = customId =>
    findOneByMock({ queryObject: { customId } }, 'manuscripts', fixtures)
  static find = id => findMock(id, 'manuscripts', fixtures)
  static findBy = values => findByMock(values, 'manuscripts', fixtures)
  static findByField = (field, value) =>
    findByFieldMock(field, value, 'manuscripts', fixtures)
  static findOneBy = values => findOneByMock(values, 'manuscripts', fixtures)
  static findAll = ({ orderByField, order = '', queryObject }) => {
    const filteredData = filter(fixtures.manuscripts, queryObject)
    if (!orderByField || orderByField === 'version') {
      return order === 'asc'
        ? filteredData.sort(this.compareVersion)
        : filteredData.sort(this.compareVersion).reverse()
    }
    return orderBy(filteredData, orderByField, order)
  }
  static all = () => allMock('manuscripts', fixtures)

  getVisibleStatus({ userId, TeamRole, TeamMemberStatuses }) {
    let reviewerTeam
    let pendingReviewer
    let acceptedReviewer
    let academicEditorTeam
    let pendingAcademicEditor

    if (this.teams) {
      reviewerTeam = this.teams.find(t => t.role === TeamRole.reviewer)
      academicEditorTeam = this.teams.find(
        t => t.role === TeamRole.academicEditor,
      )
    }

    if (academicEditorTeam) {
      pendingAcademicEditor = academicEditorTeam.members.find(
        tm => tm.status === TeamMemberStatuses.pending && tm.userId === userId,
      )
    }

    if (pendingAcademicEditor) {
      return get(
        config.get('statuses'),
        'academicEditorInvited.academicEditor.label',
      )
    }

    if (reviewerTeam) {
      pendingReviewer = reviewerTeam.members.find(
        tm => tm.status === TeamMemberStatuses.pending && tm.userId === userId,
      )
      acceptedReviewer = reviewerTeam.members.find(
        tm => tm.status === TeamMemberStatuses.accepted && tm.userId === userId,
      )
    }

    if (pendingReviewer) {
      return get(config.get('statuses'), 'reviewersInvited.reviewer.label')
    }

    if (
      acceptedReviewer &&
      this.status === Manuscript.Statuses.reviewCompleted
    ) {
      return get(config.get('statuses'), 'underReview.reviewer.label')
    }

    if (this.status !== Manuscript.Statuses.olderVersion) {
      return get(config.get('statuses'), `${this.status}.${this.role}.label`)
    }

    return 'Viewing An Older Version'
  }

  static async findManuscriptsBySubmissionId({
    order = '',
    submissionId,
    orderByField,
    excludedStatus,
  }) {
    const manuscripts = fixtures.manuscripts.filter(
      m => m.status !== excludedStatus && m.submissionId === submissionId,
    )

    if (!orderByField || orderByField === 'version') {
      return order.toLowerCase() === 'asc'
        ? manuscripts.sort(this.compareVersion)
        : manuscripts.sort(this.compareVersion).reverse()
    }
    return orderBy(manuscripts, orderByField, order)
  }

  static async findLastManuscriptBySubmissionId({
    submissionId,
    eagerLoadRelations,
  }) {
    return chain(manuscripts)
      .filter(m => m.submissionId === submissionId)
      .sort(this.compareVersion)
      .last()
      .value()
  }

  static async findManuscriptByTeamMember(teamMemberId) {
    const teamMember = fixtures.teamMembers.find(tM => tM.id === teamMemberId)
    const { manuscript } = teamMember.team
    return manuscript
  }

  static filterOlderVersions(manuscripts) {
    const submissions = groupBy(manuscripts, 'submissionId')
    const latestVersions = Object.values(submissions).map(versions => {
      if (versions.length === 1) {
        return versions[0]
      }

      const sortedVersions = versions.sort(this.compareVersion)
      const latestManuscript = last(sortedVersions)

      if (latestManuscript.status === Manuscript.Statuses.draft) {
        return sortedVersions[sortedVersions.length - 2]
      }

      return latestManuscript
    })

    return latestVersions
  }

  async save() {
    const existingManuscript = manuscripts.find(m => m.id === this.id)
    if (existingManuscript) {
      assign(existingManuscript, this)
    } else {
      if (!this.id) {
        this.id = chance.guid()
      }
      manuscripts.push(this)
    }
    return Promise.resolve(this)
  }

  async saveRecursively() {
    await this.save()
    if (this.files.length === 0) return

    await Promise.all(this.files.map(async file => file.save()))
  }

  async getEditorLabel({ SpecialIssue, Journal, role }) {
    if (this.specialIssueId) {
      const specialIssue = await SpecialIssue.find(
        this.specialIssueId,
        'peerReviewModel',
      )
      return specialIssue.peerReviewModel[`${role}Label`]
    }
    const journal = await Journal.find(this.journalId, 'peerReviewModel')
    return journal.peerReviewModel[`${role}Label`]
  }

  submitManuscript() {
    this.status = Manuscript.Statuses.technicalChecks
    this.technicalCheckToken = chance.string()
    this.publicationDates.push({
      type: Manuscript.Statuses.technicalChecks,
      date: Date.now(),
    })
  }

  updateProperties(properties) {
    assign(this, properties)
    return this
  }

  getAcademicEditor() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const academicEditor = chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === 'accepted')
      .value()

    return academicEditor
  }

  getTriageEditor() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const triageEditor = chain(this.teams)
      .find(t => t.role === 'triageEditor')
      .get('members')
      .find(member => member.status === 'pending')
      .value()

    return triageEditor
  }

  getSubmittingAuthor() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const authorTeam = this.teams.find(t => t.role === 'author')

    if (!authorTeam) {
      throw new Error('Could not find author team')
    }

    if (authorTeam.members.length === 0) {
      throw new Error('Members are required.')
    }

    return authorTeam.members.find(tm => tm.isSubmitting)
  }

  getAuthors() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const authorTeam = this.teams.find(t => t.role === 'author')

    if (!authorTeam || authorTeam.members.length === 0) {
      return []
    }

    return authorTeam.members
  }
  getReviewersByStatus(status) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }
    const reviewerTeam = this.teams.find(t => t.role === 'reviewer')
    if (!reviewerTeam.members.length === 0) {
      throw new Error('Members are required.')
    }
    return reviewerTeam.members.filter(m => m.status === status)
  }

  assignReview(review) {
    this.reviews = this.reviews || []
    this.reviews.push(review)
  }
  assignTeam(team) {
    this.teams = this.teams || []
    this.teams.push(team)
  }

  delete() {
    remove(manuscripts, f => f.id === this.id)
  }

  updateStatus(status) {
    this.status = status
  }

  getReviewers() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const reviewerTeam = this.teams.find(t => t.role === 'reviewer')
    if (!reviewerTeam || reviewerTeam.members.length === 0) {
      return []
    }

    return reviewerTeam.members
  }

  setComment() {
    if (!this.reviews) {
      throw new Error('Reviews are required.')
    }

    const responseToRevisionRequest = this.reviews.find(
      review =>
        review.recommendation === Review.Recommendations.responseToRevision,
    )
    if (!responseToRevisionRequest) {
      throw new ValidationError('There has been no request to revision')
    }

    this.comment = responseToRevisionRequest.comments[0].toDTO()
  }

  toDTO() {
    const membersPerTeam = get(this, 'teams', []).reduce((acc, team) => {
      acc[team.role] = map(sortBy(team.members, 'position'), member =>
        member.toDTO(),
      )
      return acc
    }, {})

    const manuscript = pick(this, [
      'id',
      'submissionId',
      'created',
      'updated',
      'status',
      'version',
      'hasPassedEQS',
      'hasPassedEQA',
      'technicalCheckToken',
      'teams',
      'files',
      'reviews',
      'role',
      'comment',
    ])

    manuscript.customId =
      manuscript.role === 'author' &&
      ['draft', 'technicalChecks'].includes(manuscript.status)
        ? undefined
        : this.customId

    const meta = pick(this, [
      'title',
      'abstract',
      'publicationDates',
      'agreeTc',
    ])

    const status = get(manuscript, 'status', 'draft')
    manuscript.visibleStatus = get(
      statuses,
      `${status}.${manuscript.role || 'admin'}.label`,
    )

    return {
      ...manuscript,
      authors: membersPerTeam.author,
      reviewers: membersPerTeam.reviewer,
      meta: {
        ...meta,
        title: meta.title || '',
      },
      files: sortBy(this.files, ['type', 'position']) || [],
    }
  }
}

module.exports = Manuscript
