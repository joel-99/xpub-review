const journalArticleTypes = []

const generateJournalArticleType = ({ properties, JournalArticleType }) => {
  const journalArticleType = new JournalArticleType(properties || {})

  journalArticleTypes.push(journalArticleType)

  return journalArticleType
}

const getJournalArticleTypesByJournalId = journalId =>
  journalArticleTypes.filter(
    journalArticleType => journalArticleType.journalId === journalId,
  )

module.exports = {
  journalArticleTypes,
  generateJournalArticleType,
  getJournalArticleTypesByJournalId,
}
