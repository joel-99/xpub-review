const logger = require('@pubsweet/logger')
const models = require('@pubsweet/models')
const { db, migrate } = require('@pubsweet/db-manager')
const { uniq } = require('lodash')
const { getManuscriptsUseCase } = require('../src/use-cases')
const config = require('config')

describe('get manuscripts use case', () => {
  beforeAll(async () => {
    logger.info(config['pubsweet-server'].db)
    await dbCleaner()
    await db.seed.run({
      directory: '../app/seeds',
    })
    await db.seed.run({
      directory: '../app/testSeeds',
    })
  })

  afterAll(done => {
    db.destroy()
    done()
  })

  it('returns all manuscripts if the user is admin', async () => {
    const { Team, TeamMember, Manuscript } = models
    const { userId } = await TeamMember.findOneByRole({
      role: Team.Role.admin,
    })
    const { manuscripts } = await getManuscriptsUseCase
      .initialize({ models })
      .execute({ input: {}, userId })

    const submissionIds = manuscripts.map(m => m.submissionId)
    expect(submissionIds.length).toEqual(uniq(submissionIds).length)

    const draftManuscript = manuscripts.find(
      m => m.status === Manuscript.Statuses.draft,
    )
    expect(draftManuscript.version).toEqual('1')
  })

  describe('as an admin', () => {
    let userId
    beforeAll(async () => {
      const { Team, TeamMember } = models
      ;({ userId } = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      }))
    })
    it('should return the version 1 draft of a manuscript', async () => {
      const { Manuscript } = models
      const result = await getManuscriptsUseCase
        .initialize({ models })
        .execute({ input: {}, userId })

      const draftManuscript = result.manuscripts.find(
        m => m.version === '1' && m.status === Manuscript.Statuses.draft,
      )
      expect(draftManuscript).toBeDefined()
    })
    it('should not return the version 2 draft of a manuscript', async () => {
      const { Manuscript } = models
      const result = await getManuscriptsUseCase
        .initialize({ models })
        .execute({ input: {}, userId })

      const draftManuscript = result.manuscripts.find(
        m => m.version !== '1' && m.status === Manuscript.Statuses.draft,
      )
      expect(draftManuscript).toBeUndefined()
    })
  })

  describe('as an editorial assistant', () => {
    let userId
    let journalId
    beforeAll(async () => {
      const { Journal, Team, TeamMember } = models
      ;[{ id: journalId }] = await Journal.all()
      ;({ userId } = await TeamMember.findOneByJournalAndRole({
        journalId,
        role: Team.Role.editorialAssistant,
      }))
    })
    it('should not return draft manuscripts', async () => {
      const { Manuscript } = models
      const result = await getManuscriptsUseCase
        .initialize({ models })
        .execute({ input: {}, userId })

      expect(
        result.manuscripts.every(m => m.status !== Manuscript.Statuses.draft),
      ).toBeTruthy()
    })
    it('should not see manuscripts from unassigned journals', async () => {
      const result = await getManuscriptsUseCase
        .initialize({ models })
        .execute({ input: {}, userId })

      expect(
        result.manuscripts.every(m => m.journalId === journalId),
      ).toBeTruthy()
    })
  })

  describe('as a manuscript user', () => {
    it('should  return draft manuscripts if the user is author', async () => {
      const { Team, TeamMember, Manuscript } = models
      const { userId } = await TeamMember.findOneByRole({
        role: Team.Role.author,
      })
      const result = await getManuscriptsUseCase
        .initialize({ models })
        .execute({ input: {}, userId })

      const draftManuscript = result.manuscripts.find(
        m => m.version === '1' && m.status === Manuscript.Statuses.draft,
      )

      expect(draftManuscript).toBeDefined()
    })
    it('should  not return manuscript if reviewer teamMember status is declined ', async () => {
      const { Manuscript, TeamMember, Team } = models
      const manuscripts = await Manuscript.all()
      const underReviewManuscript = manuscripts.find(
        manuscript => manuscript.status === Manuscript.Statuses.underReview,
      )
      const reviewerTeamMembers = await TeamMember.findAllByManuscriptAndRole({
        manuscriptId: underReviewManuscript.id,
        role: Team.Role.reviewer,
      })
      const declinedReviewer = reviewerTeamMembers.find(
        teamMember => teamMember.status === TeamMember.Statuses.declined,
      )
      const result = await getManuscriptsUseCase
        .initialize({ models })
        .execute({ input: {}, userId: declinedReviewer.userId })
      const manuscriptIds = result.manuscripts.map(manuscript => manuscript.id)
      expect(!manuscriptIds.includes(underReviewManuscript.id)).toBeTruthy()
    })
  })
})

const dbCleaner = async options => {
  await db.raw('DROP SCHEMA public CASCADE;')
  await db.raw('CREATE SCHEMA public;')
  await db.raw('GRANT ALL ON SCHEMA public TO public;')
  await migrate(options)
  logger.info('Dropped all tables and ran all migrations')

  return true
}
