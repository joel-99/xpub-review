const Promise = require('bluebird')

const initialize = ({ models, useCases }) => ({
  async execute({ userId, input = {} }) {
    const { Team, TeamMember, User, Manuscript } = models
    let results, total
    const currentUser = await User.find(userId)
    const currentUserStaffTeams = (await currentUser.getStaffRoles()).map(
      ({ role }) => role,
    )

    if (currentUserStaffTeams.includes(Team.Role.admin)) {
      ;({ results, total } = await Manuscript.getManuscriptsForAdmin({
        ...input,
        Team,
      }))
    } else if (currentUserStaffTeams.includes(Team.Role.editorialAssistant)) {
      ;({
        results,
        total,
      } = await Manuscript.getManuscriptsForEditorialAssistant({
        ...input,
        user: currentUser,
        Team,
      }))
    } else {
      ;({ results, total } = await Manuscript.getManuscriptsForManuscriptRoles({
        ...input,
        user: currentUser,
        Team,
      }))
    }

    const manuscripts = await Promise.mapSeries(results, async m => {
      if (!m.role) {
        m.role = await currentUser.getTeamMemberRoleForManuscript(m)
      }
      m.visibleStatus = m.getVisibleStatus({
        userId,
        TeamRole: Team.Role,
        TeamMemberStatuses: TeamMember.Statuses,
      })

      return m.toDTO()
    })

    return { manuscripts, totalCount: total }
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
