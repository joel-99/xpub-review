const { Promise } = require('bluebird')
const { isEmpty } = require('lodash')

const initialize = ({
  models: { Journal, Manuscript, Job, TeamMember, Team },
  logEvent,
  jobsService,
  eventsService,
  notificationService,
}) => ({
  async execute({ submissionId, userId }) {
    const manuscripts = await Manuscript.findBy({ submissionId })
    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
      eagerLoadRelations: 'teams.members.user',
    })

    await Promise.each(manuscripts, async manuscript => {
      if (
        [
          Manuscript.Statuses.rejected,
          Manuscript.Statuses.accepted,
          Manuscript.Statuses.deleted,
          Manuscript.Statuses.published,
        ].includes(manuscript.status)
      ) {
        throw new ValidationError('Manuscript cannot be withdrawn')
      }
      if (
        manuscript.status === Manuscript.Statuses.draft &&
        manuscript.version === '1'
      ) {
        throw new ValidationError('Manuscript cannot be withdrawn')
      }
      if (manuscript.status === Manuscript.Statuses.withdrawn) {
        throw new ValidationError('Manuscript already withdrawn.')
      }

      manuscript.updateStatus(Manuscript.Statuses.withdrawn)
      await manuscript.save()
    })

    const reviewers = await lastManuscript.getReviewers()
    const academicEditors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: lastManuscript.id,
      role: Team.Role.academicEditor,
    })
    const academicEditor = academicEditors.find(
      tm => tm.status === TeamMember.Statuses.accepted,
    )

    if (!isEmpty(reviewers) || !isEmpty(academicEditors)) {
      const reviewerAndAcademicEditorJobs = await Job.findAllByTeamMembers([
        ...academicEditors.map(tm => tm.id),
        ...reviewers.map(tm => tm.id),
      ])
      await jobsService.cancelJobs(reviewerAndAcademicEditorJobs)
    }
    const journal = await Journal.find(
      lastManuscript.journalId,
      'teams.members.user',
    )
    const editorialAssistant = await journal.getCorrespondingEditorialAssistant()
    let editorialAssistantJobs
    if (editorialAssistant) {
      editorialAssistantJobs = await Job.findAllByTeamMember(
        editorialAssistant.id,
      )
    }
    if (editorialAssistantJobs) {
      jobsService.cancelStaffMemberJobs({
        staffMemberJobs: editorialAssistantJobs,
        manuscriptId: lastManuscript.id,
      })
    }

    const authors = await lastManuscript.getAuthors()
    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      manuscriptId: lastManuscript.id,
      journalId: lastManuscript.journalId,
      sectionId: lastManuscript.sectionId,
    })

    notificationService.notifyManuscriptMembers({
      manuscript: lastManuscript,
      journalName: journal.name,
      reviewers,
      authors,
      academicEditor,
      triageEditor,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId: lastManuscript.id,
      action: logEvent.actions.withdraw_requested,
      objectType: logEvent.objectType.manuscript,
      objectId: lastManuscript.id,
    })

    eventsService.publishSubmissionEvent({
      submissionId,
      eventName: 'SubmissionWithdrawn',
    })
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
