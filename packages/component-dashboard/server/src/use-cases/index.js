const getManuscriptsUseCase = require('./getManuscripts')
const deleteManuscriptUseCase = require('./deleteManuscript')
const archiveManuscriptUseCase = require('./archiveManuscript')
const withdrawManuscriptUseCase = require('./withdrawManuscript')

module.exports = {
  getManuscriptsUseCase,
  deleteManuscriptUseCase,
  archiveManuscriptUseCase,
  withdrawManuscriptUseCase,
}
