import React, { useState, Fragment } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button } from '@pubsweet/ui'
import { Row, Item, Label, Menu, Icon } from '@hindawi/ui'

const DashboardFilters = ({
  values,
  options,
  changeSort,
  isConfirmed,
  searchValue,
  goToFirstPage,
  setSearchValue,
  changePriority,
}) => {
  const [inputValue, setInputValue] = useState(searchValue)
  const handleOnChange = event => setInputValue(event.target.value)
  const handleOnSearch = value => {
    setSearchValue(value)
    goToFirstPage()
  }
  const clearSearch = () => {
    setInputValue('')
    handleOnSearch('')
  }

  return (
    <Root isConfirmed={isConfirmed}>
      <Row
        alignItems="flex-end"
        justify="flex-start"
        pt={isConfirmed ? 22 : 28}
      >
        <Fragment>
          <InputWrapper>
            <Input
              minLength={3}
              name="searchFilter"
              onChange={handleOnChange}
              onKeyPress={e => e.key === 'Enter' && handleOnSearch(inputValue)}
              placeholder="Search by Manuscript ID or Title..."
              value={inputValue}
            />
            {searchValue && (
              <StyledIcon fontSize="16px" icon="remove" onClick={clearSearch} />
            )}
          </InputWrapper>
          <SearchButton
            invert
            ml={1}
            mr={4}
            onClick={() => handleOnSearch(inputValue)}
            secondary
            xs
          >
            Search
          </SearchButton>
        </Fragment>

        <Item
          alignItems="flex-start"
          data-test-id="dashboard-filter-priority"
          flex={0}
          mr={2}
          vertical
        >
          <Label>Priority</Label>
          <Menu
            inline
            onChange={priority => {
              changePriority(priority)
              goToFirstPage()
            }}
            options={options.priority}
            placeholder="Please select"
            value={values.priority}
            width={40}
          />
        </Item>
        <Item
          alignItems="flex-start"
          data-test-id="dashboard-filter-order"
          flex={0}
          vertical
        >
          <Label>Order</Label>
          <Menu
            inline
            onChange={changeSort}
            options={options.sort}
            placeholder="Please select"
            value={values.sort}
            width={40}
          />
        </Item>
      </Row>
    </Root>
  )
}

export default DashboardFilters

const Root = styled.div`
  /* The filter slot's height goes to the top of
  the page because of Safari's implementation
  (weird element jumping when scrolling) */
  height: calc(
    ${({ isConfirmed }) => (isConfirmed ? 36 : 42)} * ${th('gridUnit')}
  );
  position: fixed;
  top: 0;
  left: calc(${th('gridUnit')} * 20);
  right: calc(${th('gridUnit')} * 20);
  background-color: ${th('colorBackground')};
`
const SearchButton = styled(Button)`
  height: calc(${th('gridUnit')} * 8);
  min-width: calc(${th('gridUnit')} * 17);
`

const InputWrapper = styled.div`
  position: relative;
  /* width: 100%; */
`

const Input = styled.input`
  box-sizing: border-box;
  height: calc(${th('gridUnit')} * 8);
  width: calc(${th('gridUnit')} * 121);
  padding: 0;
  padding-left: calc(${th('gridUnit')} * 2);
  padding-right: calc(${th('gridUnit')} * 2);

  border-width: ${th('borderWidth')};
  border-style: ${th('borderStyle')};
  border-color: ${th('colorBorder')};
  border-radius: ${th('borderRadius')};

  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBase')};
  color: th('textPrimaryColor')};
  ::placeholder {
    color: th('textPrimaryColor')};
    opacity: 1;
    font-family: ${th('defaultFont')};
  }
  :focus {
    outline: none;
    border-color: ${th('action.colorActive')};
  }
`

const StyledIcon = styled(Icon)`
  position: absolute;
  top: 7px;
  right: 8px;

  color: ${({ disabled }) =>
    disabled ? th('colorBorder') : th('textPrimaryColor')};
  cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};
`
