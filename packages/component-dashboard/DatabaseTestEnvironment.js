const NodeEnvironment = require('jest-environment-node')
const pg = require('pg')
const logger = require('@pubsweet/logger')
const config = require('config')

let dbConfig = config['pubsweet-server'] && config['pubsweet-server'].db
if (process.env.DATABASE) {
  logger.info(`Env database has been found: ${process.env.DATABASE}`)

  dbConfig = {
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DATABASE,
    host: process.env.DB_HOST,
    port: 5432,
    ssl: true,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 20000,
  }
}
let client

class DatabaseTestEnvironment extends NodeEnvironment {
  async setup() {
    await super.setup()
    client = await new pg.Client(dbConfig)
    try {
      await client.connect()
    } catch (e) {
      logger.warn(e)

      return
    }
    // pass the test database name into the test environment as a global
    this.global.__testDbName = `test_${Math.floor(Math.random() * 9999999)}`

    await client.query(`CREATE DATABASE ${this.global.__testDbName}`)
    logger.info(`${this.global.__testDbName} has been created.`)
  }

  async teardown() {
    if (!this.global.__testDbName) {
      logger.warn('No test database has been found. Exiting...')

      return
    }

    // terminate other connections from test before dropping db
    await client.query(
      `REVOKE CONNECT ON DATABASE ${this.global.__testDbName} FROM public`,
    )
    await client.query(`
      SELECT pg_terminate_backend(pg_stat_activity.pid)
      FROM pg_stat_activity
      WHERE pg_stat_activity.datname = '${this.global.__testDbName}'`)
    await client.query(`DROP DATABASE ${this.global.__testDbName}`)
    logger.info(`${this.global.__testDbName} has been dropped.`)

    await client.end()
    await super.teardown()
  }
}

module.exports = DatabaseTestEnvironment
