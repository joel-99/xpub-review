process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

const chance = new Chance()
const { unsubscribeToEmailsUseCase } = require('../src/use-cases')

describe('Unsubscribe user to emails', () => {
  it('should unsubscribe the user to emails', async () => {
    const { User, Identity } = models
    const user = fixtures.generateUser({
      properties: { isSubscribedToEmails: true },
      User,
      Identity,
    })
    const input = {
      token: user.unsubscribeToken,
    }
    await unsubscribeToEmailsUseCase
      .initialize(models)
      .execute({ userId: user.id, input })

    expect(user.isSubscribedToEmails).toEqual(false)
  })
  it('should not return an error when the user is already unsubscribed to emails', async () => {
    const { User, Identity } = models
    const user = fixtures.generateUser({
      properties: { isSubscribedToEmails: false },
      User,
      Identity,
    })
    const input = {
      token: user.unsubscribeToken,
    }
    const result = unsubscribeToEmailsUseCase
      .initialize(models)
      .execute({ userId: user.id, input })

    return expect(result).resolves.not.toThrow()
  })
  it('should return an error when the token is invalid', async () => {
    const { User, Identity } = models
    const user = fixtures.generateUser({
      properties: { isSubscribedToEmails: true },
      User,
      Identity,
    })
    const input = {
      token: chance.guid(),
    }
    const result = unsubscribeToEmailsUseCase
      .initialize(models)
      .execute({ userId: user.id, input })

    return expect(result).rejects.toThrow(`Invalid token ${input.token}.`)
  })
})
