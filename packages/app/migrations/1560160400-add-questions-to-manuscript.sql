ALTER TABLE manuscript
  ADD COLUMN conflict_of_interest text;

ALTER TABLE manuscript
  ADD COLUMN data_availability text;

ALTER TABLE manuscript
  ADD COLUMN funding_statement text;





