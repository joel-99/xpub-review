CREATE TABLE job (
    id uuid PRIMARY KEY,
    created timestamptz NOT NULL DEFAULT now(),
    updated timestamptz NOT NULL DEFAULT now(),
    team_member_id uuid NOT NULL REFERENCES team_member (id) ON DELETE CASCADE,
    manuscript_id uuid REFERENCES manuscript (id) ON DELETE CASCADE
);