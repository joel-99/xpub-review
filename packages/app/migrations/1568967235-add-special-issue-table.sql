CREATE TABLE special_issue (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  created timestamptz NOT NULL DEFAULT now(),
  updated timestamptz NOT NULL DEFAULT now(),
  name text UNIQUE NOT NULL,
  journal_id uuid REFERENCES journal,
  section_id uuid REFERENCES section,
  start_date timestamptz NOT NULL,
  end_date timestamptz NOT NULL,
  is_active bool DEFAULT false
)