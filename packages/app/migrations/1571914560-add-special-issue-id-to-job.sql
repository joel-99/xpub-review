ALTER TABLE job
  ADD COLUMN special_issue_id uuid REFERENCES special_issue (id) ON DELETE CASCADE;
