/* eslint-disable no-await-in-loop */
const uuid = require('uuid')
const Chance = require('chance')
const Promise = require('bluebird')

const chance = new Chance()

exports.seed = async knex => {
  const createUsers = async ({ knex, numberOfUsers }) => {
    const users = []
    for (let i = 0; i < numberOfUsers; i += 1) {
      const userId = uuid.v4()
      const userInsertion = await knex('user')
        .insert([
          {
            id: userId,
            defaultIdentity: 'local',
            unsubscribeToken: uuid.v4(),
            agreeTc: true,
            isActive: true,
            isSubscribedToEmails: true,
          },
        ])
        .returning('*')
      const user = userInsertion[0]

      const identityInsertion = await knex('identity')
        .insert([
          {
            userId,
            type: 'local',
            isConfirmed: true,
            email: chance.email(),
            givenNames: chance.first(),
            surname: chance.last(),
            title: 'dr',
            aff: chance.company(),
            country: chance.country(),
          },
        ])
        .returning('*')
      const identity = identityInsertion[0]
      user.identity = identity

      users.push(user)
    }

    return users
  }

  const createTeams = async ({ knex, journal }) => {
    const roles = ['academicEditor', 'triageEditor', 'editorialAssistant']
    await Promise.each(roles, role => {
      const teamId = uuid.v4()
      return knex('team').insert([
        {
          id: teamId,
          role,
          journalId: journal.id,
        },
      ])
    })
  }

  const createTeamMembers = async ({ knex }) => {
    const teams = await knex
      .select()
      .table('team')
      .whereNotIn('team.role', ['admin', 'author'])

    const users = await createUsers({ knex, numberOfUsers: 6 })

    let i = 0
    await Promise.each(teams, async team => {
      const alias = {
        given_names: users[i].identity.givenNames,
        surname: users[i].identity.surname,
        title: users[i].identity.title,
        aff: users[i].identity.aff,
        country: users[i].identity.country,
        email: users[i].identity.email,
      }
      await knex('team_member').insert([
        {
          alias,
          teamId: team.id,
          userId: users[i].id,
          status: 'pending',
        },
      ])
      i += 1
    })
  }

  const journals = await knex.select().table('journal')
  const journal = journals[0]
  const peerReviewModels = await knex.select().table('peer_review_model')

  const peerReviewModel = peerReviewModels.find(
    prm => prm.id === journal.peerReviewModelId,
  )

  if (peerReviewModel.name === 'Chief Minus') {
    await createTeams({ knex, journal })
    await createTeamMembers({ knex, journal })
  }
}
