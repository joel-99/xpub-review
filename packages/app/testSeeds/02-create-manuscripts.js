const Chance = require('chance')

const chance = new Chance()
const uuid = require('uuid')

exports.seed = async knex => {
  const journals = await knex.select().table('journal')
  if (journals.length === 0) {
    throw new Error('No journals have been found')
  }
  const journalId = journals[0].id
  const secondJournalId = journals[1].id

  const articleTypes = await knex.select().table('article_type')
  if (articleTypes.length === 0) {
    throw new Error('No article types have been found')
  }
  const articleTypeId = articleTypes[0].id

  //  Temporary insert
  await knex('manuscript').insert([
    {
      submissionId: chance.guid(),
      status: 'reviewersInvited',
      version: '1',
      title: chance.sentence(),
      abstract: chance.paragraph(),
      journalId: secondJournalId,
      agreeTc: true,
      articleTypeId,
    },
  ])

  const draftManuscript = await knex('manuscript')
    .insert([
      {
        submissionId: chance.guid(),
        status: 'draft',
        version: '1',
        title: chance.sentence(),
        abstract: chance.paragraph(),
        journalId,
        agreeTc: true,
        articleTypeId,
      },
    ])
    .returning('*')
    .then(manuscript => manuscript[0])

  const userAndIdentity = await createUser({ knex })

  const authorTeam = await createTeam({
    knex,
    manuscriptId: draftManuscript.id,
    role: 'author',
  })

  await createTeamMember({
    knex,
    teamId: authorTeam.id,
    userId: userAndIdentity.user.id,
    status: 'pending',
  })

  const underReviewSubmission = await createSubmission({
    knex,
    journalId,
    articleTypeId,
    latestVersionStatus: 'underReview',
  })
  const lastVersion = underReviewSubmission.find(
    manuscript => manuscript.status === 'underReview',
  )
  const authorUser = await createUser({ knex })
  const academicEditorUser = await createUser({ knex })
  const triageEditorUser = await createUser({ knex })
  const reviewerUser = await createUser({ knex })
  const reviewer2User = await createUser({ knex })
  const authTeam = await createTeam({
    knex,
    manuscriptId: lastVersion.id,
    role: 'author',
  })
  const academicEditorTeam = await createTeam({
    knex,
    manuscriptId: lastVersion.id,
    role: 'academicEditor',
  })
  const triageEditorTeam = await createTeam({
    knex,
    manuscriptId: lastVersion.id,
    role: 'triageEditor',
  })
  const reviewerTeam = await createTeam({
    knex,
    manuscriptId: lastVersion.id,
    role: 'reviewer',
  })

  await createTeamMember({
    knex,
    teamId: authTeam.id,
    userId: authorUser.user.id,
    status: 'pending',
  })
  await createTeamMember({
    knex,
    teamId: triageEditorTeam.id,
    userId: triageEditorUser.user.id,
    status: 'active',
  })
  await createTeamMember({
    knex,
    teamId: academicEditorTeam.id,
    userId: academicEditorUser.user.id,
    status: 'accepted',
  })
  await createTeamMember({
    knex,
    teamId: reviewerTeam.id,
    userId: reviewerUser.user.id,
    status: 'declined',
  })
  await createTeamMember({
    knex,
    teamId: reviewerTeam.id,
    userId: reviewer2User.user.id,
    status: 'accepted',
  })

  await createSubmission({
    knex,
    journalId,
    articleTypeId,
    latestVersionStatus: 'draft',
  })
}

const createSubmission = async ({
  knex,
  journalId,
  articleTypeId,
  latestVersionStatus,
}) => {
  const submissionId = chance.guid()

  const submission = await knex('manuscript')
    .insert([
      {
        submissionId,
        status: latestVersionStatus,
        version: '2',
        title: chance.sentence(),
        abstract: chance.paragraph(),
        journalId,
        agreeTc: true,
        articleTypeId,
      },
      {
        submissionId,
        status: 'olderVersion',
        version: '1',
        title: chance.sentence(),
        abstract: chance.paragraph(),
        journalId,
        agreeTc: true,
        articleTypeId,
      },
    ])
    .returning('*')
    .then(submission => submission)
  return submission
}

const createUser = async ({ knex }) => {
  const userId = uuid.v4()
  const user = await knex('user')
    .insert([
      {
        id: userId,
        defaultIdentity: 'local',
        unsubscribeToken: uuid.v4(),
        agreeTc: true,
        isActive: true,
        isSubscribedToEmails: true,
      },
    ])
    .returning('*')
    .then(user => user[0])

  const identity = await knex('identity')
    .insert([
      {
        userId,
        type: 'local',
        isConfirmed: true,
        email: chance.email(),
        givenNames: chance.first(),
        surname: chance.last(),
        title: 'dr',
        aff: chance.company(),
        country: chance.country(),
      },
    ])
    .returning('*')
    .then(identity => identity[0])
  return { user, identity }
}

const createTeam = async ({ knex, manuscriptId, role }) => {
  const teamId = uuid.v4()
  return knex('team')
    .insert([
      {
        id: teamId,
        role,
        manuscriptId,
      },
    ])
    .returning('*')
    .then(team => team[0])
}

const createTeamMember = async ({ knex, teamId, userId, status }) => {
  const identities = await knex.select().table('identity')
  const userIdentity = identities.find(identity => identity.userId === userId)

  const alias = {
    given_names: userIdentity.givenNames,
    surname: userIdentity.surname,
    title: userIdentity.title,
    aff: userIdentity.aff,
    country: userIdentity.country,
    email: userIdentity.email,
  }
  const teamMember = knex('team_member')
    .insert([
      {
        alias,
        teamId,
        userId,
        status,
      },
    ])
    .returning('*')
    .then(tm => tm[0])
  return teamMember
}
