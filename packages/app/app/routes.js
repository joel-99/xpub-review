import React, { Fragment } from 'react'
import { Route, Switch, Redirect } from 'react-router'
import { Dashboard } from 'component-dashboard/client'
import {
  Login,
  SignUp,
  InfoPage,
  ResetPassword,
  ConfirmAccount,
  SetNewPassword,
  SSOReviewerRedirect,
  AuthenticatedRoute,
  SignUpFromInvitation,
} from 'component-authentication/client'
import { EQSDecision, EQADecision } from 'component-screening/client'
import { EmailResponse, ManuscriptDetails } from 'component-peer-review/client'
import {
  SubmissionConfirmation,
  Wizard,
  SubmissionFromURL,
} from 'component-submission/client'
import {
  AdminDashboard,
  AdminUsers,
  AdminRoute,
  AdminJournals,
  AdminJournalDetails,
} from 'component-admin/client'
import { UserProfilePage, ChangePassword } from 'component-user-profile/client'
import { useKeycloak } from 'component-sso/client'
import FourOFour from './FourOFour'
import HindawiApp from './HindawiApp'
import ReviewerRedirect from './ReviewerRedirect'
import ManuscriptRedirect from './ManuscriptRedirect'

const Routes = () => {
  const keycloak = useKeycloak()
  return (
    <HindawiApp>
      <Switch>
        <Route component={InfoPage} exact path="/info-page" />
        <Route component={EQSDecision} exact path="/eqs-decision" />
        <Route component={EQADecision} exact path="/eqa-decision" />
        <Route
          component={SubmissionConfirmation}
          exact
          path="/confirmation-page"
        />
        {keycloak && (
          <Route
            component={SSOReviewerRedirect}
            exact
            path="/emails/accept-review-new-user"
          />
        )}
        <Route component={EmailResponse} exact path="/emails/:action" />
        <Route
          component={ManuscriptRedirect}
          exact
          path="/projects/:projectId/versions/:versionId/details"
        />
        <Route component={ReviewerRedirect} exact path="/invite-reviewer" />

        <AdminRoute component={AdminUsers} exact path="/admin/users" />
        <AdminRoute component={AdminJournals} exact path="/admin/journals" />
        <AdminRoute
          component={AdminJournalDetails}
          exact
          path="/admin/journals/:journalId"
        />
        <AdminRoute component={AdminDashboard} exact path="/admin" />

        <AuthenticatedRoute component={Dashboard} exact path="/" />
        <AuthenticatedRoute
          component={ManuscriptDetails}
          exact
          path="/details/:submissionId/:manuscriptId"
        />
        <AuthenticatedRoute
          component={ChangePassword}
          exact
          path="/profile/change-password"
        />
        <AuthenticatedRoute
          component={Wizard}
          exact
          path="/submit/:submissionId/:manuscriptId"
        />
        <AuthenticatedRoute
          component={SubmissionFromURL}
          exact
          path="/submit/:code"
        />
        <AuthenticatedRoute component={UserProfilePage} exact path="/profile" />

        <Route component={ConfirmAccount} exact path="/confirm-signup" />

        <Route exact path="/login">
          {keycloak ? (
            <Redirect to="/" />
          ) : (
            <Route component={Login} exact path="/login" />
          )}
        </Route>

        {!keycloak && (
          <Fragment>
            <Route component={SignUpFromInvitation} exact path="/invite" />
            <Route component={SignUp} exact path="/signup" />
            <Route component={ResetPassword} exact path="/password-reset" />
            <Route component={SetNewPassword} exact path="/forgot-password" />
          </Fragment>
        )}
        <Route component={ConfirmAccount} exact path="/invite" />
        <Route component={FourOFour} exact path="/404" />
        <Redirect to="/404" />
      </Switch>
    </HindawiApp>
  )
}

export default Routes
