import React from 'react'
import gql from 'graphql-tag'
import ReactDOM from 'react-dom'
import {
  InMemoryCache,
  IntrospectionFragmentMatcher,
} from 'apollo-cache-inmemory'
import { Root } from 'pubsweet-client'
import { theme } from '@hindawi/ui'
import { createBrowserHistory } from 'history'
import { ModalProvider } from 'component-modal'
import { KeycloakProvider, keycloak } from 'component-sso/client'
import { JournalProvider as HindawiContextProvider } from 'component-journal-info'

import Routes from './routes'
import * as journal from './config/journal'
import * as publishers from './config/publisher'

// wait for PS to stop supporting redux
const store = {
  subscribe: () => {},
  dispatch: () => {},
  getState: () => {},
}

const history = createBrowserHistory()

const typeDefs = gql`
  extend type Mutation {
    updateAutosave: Boolean
  }
`

const makeApolloConfig = ({ cache, link, ...config }) => {
  // Reference: https://github.com/apollographql/apollo-client/issues/3397
  const hindawiCache = new InMemoryCache({
    fragmentMatcher: new IntrospectionFragmentMatcher({
      introspectionQueryResultData: {
        __schema: {
          types: [],
        },
      },
    }),
  })

  const clientState = {
    typeDefs,
    resolvers: {
      Mutation: {
        updateAutosave: (_, { params }, { cache }) => {
          const data = {
            autosave: {
              __typename: 'AutosaveState',
              ...params,
            },
          }
          cache.writeData({ data })
          return null
        },
      },
    },
    defaults: {
      autosave: {
        __typename: 'AutosaveState',
        error: null,
        updatedAt: null,
        inProgress: false,
      },
    },
  }
  return {
    cache: hindawiCache,
    link,
    ...config,
    ...clientState,
  }
}

const render = keycloak => {
  document.title = publishers[process.env.PUBLISHER_NAME].name
  ReactDOM.render(
    <React.Fragment>
      <ModalProvider>
        <HindawiContextProvider
          journal={journal}
          publisher={publishers[process.env.PUBLISHER_NAME]}
        >
          <KeycloakProvider keycloak={keycloak}>
            <Root
              connectToWebSocket={false}
              history={history}
              makeApolloConfig={makeApolloConfig}
              routes={<Routes />}
              store={store}
              theme={theme}
            />
          </KeycloakProvider>
        </HindawiContextProvider>
      </ModalProvider>
      <div id="ps-modal-root" style={{ height: 0 }} />
      <div id="portal-root" style={{ height: 0 }} />
    </React.Fragment>,
    document.getElementById('root'),
  )
}

const renderError = (keycloakError = {}, keycloak) => {
  const { error = 'An error occurred.', error_description } = keycloakError
  if (error === 'access_denied') {
    keycloak.login()
  }
  ReactDOM.render(
    <React.Fragment>
      <h1 style={{ textAlign: 'center' }}>
        {error_description ? error.error_description : error}
      </h1>
      <p style={{ textAlign: 'center' }}>
        <a href="/">Go back</a>
      </p>
    </React.Fragment>,
    document.getElementById('root'),
  )
}
const keycloakEnv = process.env.KEYCLOAK
if (keycloakEnv && keycloakEnv.authServerURL) {
  keycloak.init(keycloakEnv, render, renderError)
} else {
  render()
}
