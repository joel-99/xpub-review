describe('Admin edits before EQS', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Author creates draft', () => {
    cy.createDraftViaAPI()
  })

  it('Admin edits manuscript before EQS', function editManuscript() {
    const { admin, fragment } = this
    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click({
      force: true,
    })
    cy.editManuscript(fragment)
    cy.visit('/')
      .get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .contains('Fragment 2 - modified')
  })
})
