describe(`AcademicEditor doesn't see files on latest manuscript if pending`, () => {
  beforeEach(() => {
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('manuscripts/statuses').as('statuses')
    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Should hide files from AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { statuses, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)

    cy.get('.icn_icn_preview').should('not.be.visible')
    cy.get('.icn_icn_downloadZip').should('not.be.visible')
    cy.get('[data-test-id="files-tab"]').should('not.be.visible')
  })
})
