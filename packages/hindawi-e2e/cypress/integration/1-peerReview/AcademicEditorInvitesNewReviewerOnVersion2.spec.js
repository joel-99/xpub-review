describe('Academic Editor invites New Reviewer on version 2', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
    // cy.pause()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { statuses, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit(`/`)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
    cy.respondToInvitationAsAcademicEditor('yes')

    cy.checkStatus(statuses.academicEditorAssigned.academicEditor)
  })

  it('Invite reviewers as AcademicEditor', function inviteReviewer() {
    const { statuses, reviewer, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.inviteReviewer({ reviewer, academicEditor })
    cy.checkStatus(statuses.reviewersInvited.academicEditor)
  })

  it('Give a review for major revision as Reviewer', function majorRevisionReview() {
    const { reviewer, academicEditor, statuses } = this
    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('yes')
    cy.checkStatus(statuses.underReview.reviewer)

    cy.submitReview('Major Revision')

    cy.get('[data-test-id="manuscript-status"]').contains('Under Review') // Bug

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
    cy.checkStatus(statuses.reviewCompleted.academicEditor)
  })

  it('AcademicEditor makes recommendation for major revision', function majorRevisionRecommendation() {
    const { academicEditor, triageEditor, statuses } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.academicEditorMakesRecommendation('Major Revision')
    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.revisionRequested.triageEditor)
  })

  it('Author submit major revision', function majorRevisionSubmission() {
    const { author, statuses, updatedFragment } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.revisionRequested.author)
    cy.authorSubmitRevision({ updatedFragment })
    // cy.visit('/')
    // cy.get(`[data-test-id="dashboard-list-items"] > div`)
    //   .first()
    //   .click()
    // return cy.location().then(loc => {
    //   Cypress.env(
    //     'manuscriptId',
    //     loc.pathname
    //       .replace('/details', '')
    //       .split('/')
    //       .pop(),
    //   )
    // })
  })

  it('AcademicEditor invites new reviewer on version 2 of the manuscript', function academicEditorInvitesNewRevVersion2() {
    const { statuses, reviewer, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.underReview.academicEditor)
    cy.contains('Reviewer Details & Reports').click()
    const newRev = reviewer.filter(
      reviewer => reviewer.email !== 'rev@hindawi.com',
    )
    cy.inviteReviewer({ reviewer: newRev, academicEditor })
    cy.checkStatus(statuses.underReview.academicEditor)

    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.reviewersInvited.reviewer)
    cy.respondToInvitationAsReviewer('yes')
    cy.checkStatus(statuses.underReview.reviewer)
    cy.contains('Response to Revision Request')
      .should('be.visible')
      .click()
    cy.contains('Author Reply')

    cy.contains('Your Report').should('be.visible')

    cy.get(`[role="listbox"]`).contains('Version 2')
  })
})
