describe('Reviewer declines invitation', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { statuses, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
    cy.respondToInvitationAsAcademicEditor('yes')

    cy.checkStatus(statuses.academicEditorAssigned.academicEditor)
  })

  it('Invite reviewers as AcademicEditor', function inviteReviewer() {
    const { statuses, reviewer, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.inviteReviewer({ reviewer })
    cy.checkStatus(statuses.reviewersInvited.academicEditor)
  })

  it('Reviewer should decline invitation', function reviewerDeclinesInvitation() {
    const { reviewer, academicEditor, statuses } = this
    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('no')
    cy.reload()

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).should(
      'not.exist',
    )

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.get('span').contains('DECLINED')
  })
})
