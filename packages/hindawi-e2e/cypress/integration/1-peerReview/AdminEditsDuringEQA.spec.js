describe('Admin edits during EQA', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })
  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { academicEditor } = this

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsAcademicEditor('yes')
  })

  it('Invite reviewers as AcademicEditor', function inviteReviewer() {
    const { reviewer, academicEditor } = this

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.inviteReviewer({ reviewer, academicEditor })
  })

  it('Give a review for publish as Reviewer', function minorRevisionReview() {
    const { reviewer } = this

    cy.visit('/')
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsReviewer('yes')
    cy.reload()
    cy.submitReview('Publish')
  })

  it('AcademicEditor makes recommendation to publish', function academicEditorMakesRecommendationToPublish() {
    const { academicEditor } = this

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.academicEditorMakesRecommendation('Publish')
  })

  it('Triage Editor makes decision to publish', function triageEditorMakesDecisioToPublish() {
    const { triageEditor } = this

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.triageEditorMakesDecision('Publish')
  })

  it('Admin edit manuscript during EQA', function approveManuscriptByEQA() {
    const { admin, fragment, statuses } = this

    const id = Cypress.env('manuscriptId')
    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${id}"]`).click()
    cy.get(`[data-test-id="button-qa-manuscript-edit"]`)
      .should('be.visible')
      .click()

    cy.editManuscript(fragment)
    cy.get('[type="button"]')
      .contains('SAVE CHANGES')
      .click()
    cy.get('[data-test-id="manuscript-title"]')
      .scrollIntoView()
      .contains('Fragment 2 - modified')

    cy.checkStatus(statuses.inQA.admin)
  })
})
