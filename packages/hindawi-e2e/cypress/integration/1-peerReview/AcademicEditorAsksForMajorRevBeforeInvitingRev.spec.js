describe('AcademicEditor asks for major revision before inviting reviewers', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { statuses, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
    cy.respondToInvitationAsAcademicEditor('yes')

    cy.checkStatus(statuses.academicEditorAssigned.academicEditor)
  })

  it('AcademicEditor asks for major revision before inviting reviewers', async function academicEditorAsksForMajRevBeforeInviteingRev() {
    const { academicEditor, statuses, author } = this

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.academicEditorAssigned.academicEditor)
    cy.contains('Reviewer Details & Reports').should('be.visible')
    cy.contains('No reviewers invited yet.')
    cy.academicEditorMakesRecommendation('Major Revision')
    cy.checkStatus(statuses.revisionRequested.academicEditor)
    cy.contains('Editorial Comments')
    cy.contains('Major Revision Requested')
    cy.should('not.contain', 'Publish')
    cy.should('not.contain', 'Minor Revision Requested')

    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.checkStatus(statuses.revisionRequested.author)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.contains('Editorial Comments')
    cy.contains('Major Revision Requested')
    cy.contains(Cypress.env('academicEditorRecommendationText'))

    cy.contains('Submit Revision')
  })
})
