describe('ORCID ID', () => {
  beforeEach(() => {
    cy.fixture('users/author').as('author')
  })

  it('ORCID ID', function orcidId() {
    const { author } = this

    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get('[data-test-id="admin-menu-button"]').click()
    cy.get('[data-test-id="admin-dropdown-profile"]').click()
    cy.get('[data-test-id="orcid-action"]')
      .should('be.visible')
      .contains('Link')
  })
})
