describe('Edit user profile as admin', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
  })

  it('Edit user profile', function editUserProfileAsAdmin() {
    const { admin } = this

    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')

    cy.get('.icn_icn_caretDown')
      .first()
      .click()
    cy.get('[data-test-id="admin-dropdown-dashboard"]').click()
    cy.contains('Users').click()
    cy.wait(2000)
    cy.get('.icn_icn_moreDefault')
      .eq(1)
      .click({ force: true })
    cy.findByText('Edit User').click({ force: true })

    cy.get('[name="givenNames"]')
      .clear()
      .type('FIRSTNAMEv1')

    cy.get('[name="surname"]')
      .clear()
      .type('LastNamev1')

    cy.get('[role="listbox"]')
      .eq(0)
      .click()
      .get('[role="option"]')
      .contains('Professor')
      .click()

    cy.get('[data-test-id="country"]')
      .get('div[role="listbox"] input')
      .clear()
      .type('al', { force: true })
      .type('{enter}', { force: true })

    cy.get('[name="aff"]')
      .clear()
      .type('Cypress Org')

    cy.get('button')
      .contains('SAVE USER')
      .click()
  })
})
