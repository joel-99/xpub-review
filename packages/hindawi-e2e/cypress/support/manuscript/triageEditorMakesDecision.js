const triageEditorMakesDecision = decision => {
  switch (decision) {
    case 'Publish': {
      cy.get('div[label="Your Editorial Decision"]')
        .should('be.visible')
        .click()

      cy.get('div[role="listbox"]')
        .eq(1)
        .click()

      cy.get('[role="option"]')
        .should('contain', 'Return to Academic Editor')
        .should('contain', 'Reject')
        .contains('Publish')
        .click()

      cy.get('[data-test-id="submit-triage-editor-decision"]')
        .should('be.visible')
        .click({ force: true })

      cy.get('[data-test-id="modal-confirm"]').click()
      break
    }
    default: {
      cy.get('div[label="Your Editorial Decision"]')
        .should('be.visible')
        .click()

      cy.get('div[role="listbox"]')
        .eq(1)
        .click()

      cy.get('[role="option"]')
        .contains(decision)
        .click()

      Cypress.env(
        'triageEditorDecisionText',
        'loreimpsum dolor sit ameloreimpsum dolores.',
      )

      cy.get('[data-test-id="triage-editor-decision-message"]')
        .should('be.visible')
        .type(Cypress.env('triageEditorDecisionText'))

      cy.get('[data-test-id="submit-triage-editor-decision"]')
        .should('be.visible')
        .click({ force: true })

      cy.get('[data-test-id="modal-confirm"]').click()
    }
  }
}

Cypress.Commands.add('triageEditorMakesDecision', triageEditorMakesDecision)
