const rejectManuscriptByEQA = () => {
  cy.get('[data-test-id="button-qa-manuscript-technical-checks"]')
    .should('be.visible')
    .click()
  cy.contains('Take a decision for manuscript.')
  cy.get('[data-test-id="reject-button"]').click()
  cy.get('[data-test-id="return-reason"]').type('It sucks')
  cy.get('[data-test-id="modal-confirm"]').click()
  cy.contains('Manuscript decision submitted. Thank you!')
}

Cypress.Commands.add('rejectManuscriptByEQA', rejectManuscriptByEQA)
