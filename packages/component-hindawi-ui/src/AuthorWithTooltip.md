```js
const author = {
  alias: {
    aff: 'TS',
    email: 'silv.celmare@gmail.com',
    country: 'Romania',
    name: {
      surname: 'Cel Mare',
      givenNames: 'Silv',
    }
  },
  isSubmitting: true,
  isCorresponding: true,
}
;
<div>
  <AuthorWithTooltip
    author={author}
  />

</div>
```