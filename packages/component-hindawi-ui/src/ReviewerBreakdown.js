import React from 'react'
import { get } from 'lodash'
import { withProps } from 'recompose'

import { Row, Text } from '../'

const ReviewerBreakdown = ({ accepted, declined, reviewers, submitted }) =>
  reviewers && reviewers.length ? (
    <Row alignItems="baseline" fitContent justify="flex-end" mr={1}>
      <Text customId mr="4px">
        {reviewers.length}
      </Text>
      <Text mr={1} secondary>
        invited,
      </Text>

      <Text customId mr="4px">
        {accepted}
      </Text>
      <Text mr={1} secondary>
        agreed,
      </Text>

      <Text customId mr="4px">
        {declined}
      </Text>
      <Text mr={1} secondary>
        declined,
      </Text>

      <Text customId mr="4px">
        {submitted}
      </Text>
      <Text mr={1} secondary>
        submitted
      </Text>
    </Row>
  ) : (
    <Row alignItems="baseline" fitContent justify="flex-end" mr={1}>
      <Text secondary>0 invited</Text>
    </Row>
  )

export default withProps(({ reviewers = [] }) => ({
  accepted: reviewers
    ? reviewers.filter(reviewer => get(reviewer, 'status') === 'accepted')
        .length +
      reviewers.filter(reviewer => get(reviewer, 'status') === 'submitted')
        .length
    : [],
  declined: reviewers
    ? reviewers.filter(reviewer => get(reviewer, 'status') === 'declined')
        .length
    : [],
  submitted: reviewers
    ? reviewers.filter(reviewer => get(reviewer, 'status') === 'submitted')
        .length
    : [],
}))(ReviewerBreakdown)
