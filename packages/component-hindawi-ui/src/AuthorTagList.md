A list of author tags. Used on manuscript card and details.

```js
const authors = [
  {
    id: 1,
    alias: {
      aff: 'University of California',
      email: 'john.doe@gmail.com',
      name: {
        surname: 'JohnS',
        givenNames: 'Doe',
      },
    },
    affiliation: 'University of California',
  },
  {
    id: 2,
    alias: {
      aff: 'US Presidency',
      email: 'michael.felps@gmail.com',
      name: {
        surname: 'Michael',
        givenNames: 'Felps',
      },
    },
    isSubmitting: true,
    isCorresponding: true,
  },
  {
    id: 3,
    alias: {
      aff: 'US Presidency',
      email: 'barrack.obama@gmail.com',
      name: {
        surname: 'Barrack',
        givenNames: 'Obama',
      },
    },
  },
]
;<AuthorTagList authors={authors} />
```

A list of author tags with affiliation and tooltip

```js
const authors = [
  {
    id: 1,
    alias: {
      aff: 'University of California',
      email: 'john.doe@gmail.com',
      name: {
        surname: 'JohnS',
        givenNames: 'Doe',
      },
    },
    isSubmitting: true,
  },
  {
    id: 2,
    alias: {
      aff: 'US Presidency',
      email: 'michael.felps@gmail.com',
      name: {
        surname: 'Michael',
        givenNames: 'Felps',
      },
    },
    isSubmitting: true,
    isCorresponding: true,
  },
  {
    id: 3,
    alias: {
      aff: 'US Presidency',
      email: 'barrack.obama@gmail.com',
      name: {
        surname: 'Barrack',
        givenNames: 'Obama',
      },
    },
  },
  {
    id: 4,
    alias: {
      aff: 'US Presidency',
      email: 'barrack.obama@gmail.com',
      name: {
        surname: 'Sebi',
        givenNames: 'Ciocanete',
      },
    },
  },
]
;<AuthorTagList authors={authors} withTooltip withAffiliations />
```

A list of author tags with tooltip

```js
const authors = [
  {
    id: 1,
    alias: {
      email: 'john.doe@gmail.com',
      name: {
        surname: 'John',
        givenNames: 'Doe',
      },
    },
    isSubmitting: true,
  },
  {
    id: 2,
    alias: {
      email: 'michael.felps@gmail.com',
      name: {
        surname: 'Michael',
        givenNames: 'Felps',
      },
    },
    isSubmitting: true,
    isCorresponding: true,
  },
  {
    id: 3,
    alias: {
      email: 'barrack.obama@gmail.com',
      name: {
        surname: 'Barrack',
        givenNames: 'Obama',
      },
    },
  },
]
;<AuthorTagList authors={authors} withTooltip />
```

Use a different separator and key for mapping the authors.

```js
const authors = [
  {
    id: 1,
    alias: {
      aff: 'UTI',
      email: 'john.doe@gmail.com',
      name: {
        surname: 'John',
        givenNames: 'Doe',
      },
    },
    isSubmitting: true,
  },
  {
    id: 2,
    alias: {
      aff: 'Boko Haram',
      email: 'michael.felps@gmail.com',
      name: {
        surname: 'Michael',
        givenNames: 'Felps',
      },
    },
    isSubmitting: true,
    isCorresponding: true,
  },
  {
    id: 3,
    alias: {
      aff: 'UTI',
      email: 'barrack.obama@gmail.com',
      name: {
        surname: 'Barrack',
        givenNames: 'Obama',
      },
    },
  },
]
;<AuthorTagList separator="* * *" authors={authors} authorKey="alias.email" />
```
