import PropTypes from 'prop-types'
import { compose, withStateHandlers } from 'recompose'

const Tabs = ({ items, selectedTab = 2, changeTab, children }) =>
  children({ selectedTab, changeTab })

Tabs.propTypes = {
  /** The selected tab. */
  selectedTab: PropTypes.number,
  /** Handler to change the tab. */
  changeTab: PropTypes.func,
}

Tabs.defaultProps = {
  selectedTab: 0,
  changeTab: () => {},
}

export default compose(
  withStateHandlers(({ selectedTab = 0 }) => ({ selectedTab }), {
    changeTab: () => selectedTab => ({
      selectedTab,
    }),
  }),
)(Tabs)
