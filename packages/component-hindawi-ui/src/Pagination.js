import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Button } from '@pubsweet/ui'

import { Icon, Text } from '../'

const PaginationComponent = ({
  totalCount,
  toLast,
  toFirst,
  nextPage,
  prevPage,
  setPage,
  page,
  hasMore = true,
  itemsPerPage,
  changeItemsPerPage = () => {},
}) => {
  const pageLimit = 5
  const offset = Math.floor(pageLimit / 2)
  const totalPages = Math.ceil(totalCount / itemsPerPage)

  const pageRange = [
    ...Array(totalPages <= pageLimit ? totalPages : pageLimit),
  ].map((item, index) => {
    if (totalPages <= pageLimit || page <= offset) return index // start of range or all pages can be displayed
    if (page >= totalPages - offset) return offset * -2 + index + totalPages - 1 // end of range
    return page - offset + index
  })

  const disabledRight = page === totalPages - 1
  const disabledLeft = page === 0
  return (
    <Root data-test-id="pagination-component">
      <Button
        data-test-id="arrow-end-left"
        disabled={disabledLeft}
        mr={1}
        onClick={toFirst}
        square
      >
        <Icon disabled={disabledLeft} icon="arrowEndLeft" />
      </Button>
      <Button
        data-test-id="caret-left"
        disabled={disabledLeft}
        mr={1}
        onClick={prevPage}
        square
      >
        <Icon disabled={disabledLeft} icon="arrowRight" reverse />
      </Button>

      {pageRange.map((pageNumber, i) => (
        <Button
          data-test-id="page-numbers"
          key={pageNumber}
          mr={1}
          onClick={() => setPage(pageNumber)}
          selected={pageNumber === page}
          square
        >
          <Text secondary selected={pageNumber === page}>
            {pageNumber + 1}
          </Text>
        </Button>
      ))}

      <Button
        data-test-id="caret-right"
        disabled={disabledRight}
        onClick={nextPage}
        square
      >
        <Icon disabled={disabledRight} icon="arrowRight" />
      </Button>
      <Button
        data-test-id="arrow-end-right"
        disabled={disabledRight}
        ml={1}
        onClick={toLast}
        square
      >
        <Icon disabled={disabledRight} icon="arrowEnd" />
      </Button>
    </Root>
  )
}

export const Pagination = ({ children, ...props }) => (
  <Fragment>
    <PaginationComponent {...props} />
    {typeof children === 'function' && children(props)}
  </Fragment>
)

Pagination.propTypes = {
  /** Page current number. */
  page: PropTypes.number,
  /** Indicates if there are more pages to be displayed. */
  hasMore: PropTypes.bool,
  /** Maximum items displayed. */
  maxItems: PropTypes.number,
  /** Items displayed per page. */
  itemsPerPage: PropTypes.number,
  /** Change how many items should be displayed per page. */
  changeItemsPerPage: PropTypes.func,
}
Pagination.defaultProps = {
  page: 1,
  hasMore: false,
  maxItems: 23,
  itemsPerPage: 10,
  changeItemsPerPage: () => {},
}

export default PaginationComponent

// #region styles
const Root = styled.div`
  display: flex;
  justify-content: center;
`

// #endregion
