An expandable section.

```js
<Accordion label="Uncontrolled accordion here">
  <div>peek a boo</div>
</Accordion>
```
