import { useCallback, useEffect, useState } from 'react'

const getSize = el =>
  el
    ? el.getBoundingClientRect()
    : { bottom: 0, height: 0, left: 0, right: 0, top: 0, width: 0, x: 0, y: 0 }

const useClientRect = () => {
  const [node, setNode] = useState()
  const [rect, setRect] = useState(getSize(node))

  const handleResize = useCallback(() => setRect(getSize(node)), [node])

  useEffect(() => {
    if (node) {
      setRect(getSize(node))
    }
  }, [node])

  useEffect(() => {
    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  }, [handleResize])

  return [setNode, rect]
}

export default useClientRect
