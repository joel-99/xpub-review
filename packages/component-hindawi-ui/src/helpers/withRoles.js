import { compose, withProps } from 'recompose'

const roles = [
  { label: 'user', value: 'User' },
  { label: 'admin', value: 'Admin' },
  { label: 'triageEditor', value: 'Chief Editor' },
  { label: 'academicEditor', value: 'Academic Editor' },
  { label: 'editorialAssistant', value: 'Editorial Assistant' },
]

const titles = [
  { label: 'Mr', value: 'mr' },
  { label: 'Mrs', value: 'mrs' },
  { label: 'Miss', value: 'miss' },
  { label: 'Ms', value: 'ms' },
  { label: 'Dr', value: 'dr' },
  { label: 'Professor', value: 'prof' },
]

export default compose(
  withProps({
    titles,
    roles,
  }),
)
