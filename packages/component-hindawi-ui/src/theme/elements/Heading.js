import { get } from 'lodash'
import { space } from 'styled-system'
import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const ellipsis = props => {
  if (props.ellipsis) {
    return css`
      text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap;
    `
  }

  return css`
    white-space: ${props => get(props, 'whiteSpace', 'initial')};
  `
}

const heading = () => css`
  align-items: center;
  display: flex;
  justify-content: center;
  height: inherit;
  margin: 0;

  ${ellipsis};
`

export default {
  H1: css`
    color: ${th('mainTextColor')};
    font-family: ${th('defaultFont')};
    font-size: ${th('h1Size')};
    font-weight: 700;
    line-height: ${th('h1LineHeight')};

    ${heading};
    ${space};
  `,
  H2: css`
    color: ${th('mainTextColor')};
    font-family: ${th('defaultFont')};
    font-size: ${th('h2Size')};
    font-weight: 700;
    line-height: ${th('h2LineHeight')};

    ${heading};
    ${space};
  `,
  H3: css`
    color: ${th('mainTextColor')};
    font-family: ${th('defaultFont')};
    font-size: ${th('h3Size')};
    font-weight: 700;
    line-height: ${th('h3LineHeight')};

    ${heading};
    ${space};
  `,
  H4: css`
    color: ${th('heading.h4Color')};
    font-family: ${th('defaultFont')};
    font-weight: 700;

    ${heading};
    ${space};
  `,
}
