import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export default {
  MessageWrapper: css``,
  ErrorMessage: css`
    color: ${th('colorError')};
    font-family: ${th('defaultFont')};
  `,
}
