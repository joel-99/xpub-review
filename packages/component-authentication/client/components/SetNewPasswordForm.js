import React from 'react'
import { Formik } from 'formik'
import { Button, H2, Spinner } from '@pubsweet/ui'
import {
  Row,
  Text,
  ShadowedBox,
  PasswordValidation,
  passwordValidator,
} from '@hindawi/ui'

const SetNewPasswordForm = ({ isFetching, fetchingError, setPassword }) => (
  <Formik onSubmit={setPassword} validate={passwordValidator}>
    {({ handleSubmit, values, errors, touched }) => (
      <ShadowedBox center mt={10}>
        <H2>Set new password</H2>

        <PasswordValidation
          errors={errors}
          formValues={values}
          touched={touched}
        />

        {fetchingError && (
          <Row justify="flex-start" mt={2}>
            <Text error>{fetchingError}</Text>
          </Row>
        )}

        <Row mt={8}>
          {isFetching ? (
            <Spinner />
          ) : (
            <Button onClick={handleSubmit} primary>
              Confirm
            </Button>
          )}
        </Row>
      </ShadowedBox>
    )}
  </Formik>
)

export default SetNewPasswordForm
