const initialize = ({ tokenService, models: { User } }) => ({
  execute: async input => {
    const user = await User.find(input.userId, 'identities')
    const localIdentity = user.identities.find(
      identity => identity.type === 'local',
    )

    if (!localIdentity) {
      throw new Error('No local identity has been found.')
    }

    if (localIdentity.isConfirmed) {
      throw new ConflictError('User is already confirmed.')
    }

    if (user.confirmationToken !== input.token) {
      throw new Error('Invalid confirmation token.')
    }

    localIdentity.isConfirmed = true
    await localIdentity.save()

    user.confirmationToken = null

    await user.save()

    const token = tokenService.create({
      username: localIdentity.email,
      id: user.id,
    })

    return { token }
  },
})

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
