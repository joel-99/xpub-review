process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const { models, fixtures, services } = require('fixture-service')

const { User, Identity } = models
const chance = new Chance()
const localLoginCase = require('../src/use-cases/localLogin')

describe('local login use case', () => {
  it('returns success when the username and password are correct', async () => {
    const user = fixtures.generateUser({
      properties: {
        isActive: true,
      },
      User,
      Identity,
    })

    const result = await localLoginCase
      .initialize(services.tokenService, models)
      .execute({
        email: user.identities[0].email,
        password: 'password',
      })

    expect(result.token).toBeDefined()
  })

  it('returns an error when the email is wrong', async () => {
    const result = localLoginCase
      .initialize(services.tokenService, models)
      .execute({
        email: chance.email(),
        password: 'password',
      })

    return expect(result).rejects.toThrow('Wrong username or password.')
  })

  it('returns an error when the password is wrong', async () => {
    const user = fixtures.generateUser({
      properties: {
        isActive: true,
      },
      User,
      Identity,
    })

    const result = localLoginCase
      .initialize(services.tokenService, models)
      .execute({
        email: user.identities[0].email,
        password: 'wrong-password',
      })

    return expect(result).rejects.toThrow('Wrong username or password.')
  })

  it('returns an error if the user is deactivated', async () => {
    const user = fixtures.generateUser({
      properties: {
        isActive: false,
      },
      User,
      Identity,
    })

    const result = localLoginCase
      .initialize(services.tokenService, models)
      .execute({
        email: user.identities[0].email,
        password: 'password',
      })

    return expect(result).rejects.toThrow('Wrong username or password.')
  })
})
