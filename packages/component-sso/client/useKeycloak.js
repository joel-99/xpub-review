import { useContext } from 'react'
import KeycloakContext from './KeycloakContext'

function useKeycloak() {
  return useContext(KeycloakContext)
}

export default useKeycloak
