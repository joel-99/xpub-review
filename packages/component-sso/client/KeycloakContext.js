import { createContext } from 'react'

export default createContext({
  keycloak: null,
})
