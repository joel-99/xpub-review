import React from 'react'

import { MultiAction } from '@hindawi/ui'

const AcceptEQSModal = ({ handleApproveEQS, hideModal }) => (
  <MultiAction
    cancelText="CANCEL"
    confirmText="OK"
    hideModal={hideModal}
    onConfirm={handleApproveEQS}
    title="Accept manuscript"
  />
)

export default AcceptEQSModal
