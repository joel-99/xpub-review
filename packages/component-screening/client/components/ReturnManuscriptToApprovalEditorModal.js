import React from 'react'

import {
  Row,
  Item,
  Label,
  FormModal,
  Textarea,
  validators,
  ValidatedFormField,
} from '@hindawi/ui'

const ReturnManuscriptToApprovalEditorModal = ({
  handleReturnToApprovalEditor,
  hideModal,
}) => (
  <FormModal
    cancelText="CANCEL"
    confirmText="OK"
    content={() => (
      <Row mt={6}>
        <Item vertical>
          <Label required>Return reason</Label>
          <ValidatedFormField
            component={Textarea}
            data-test-id="return-reason"
            name="reason"
            validate={[validators.required]}
          />
        </Item>
      </Row>
    )}
    hideModal={hideModal}
    onSubmit={handleReturnToApprovalEditor}
    title="Reject manuscript"
  />
)

export default ReturnManuscriptToApprovalEditorModal
