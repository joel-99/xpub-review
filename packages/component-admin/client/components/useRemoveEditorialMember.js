import { useMutation } from 'react-apollo'
import { queries, mutations } from '../graphql'

const useRemoveEditorialMember = journalId => {
  const [removeEditorialMember] = useMutation(mutations.removeEditorialMember, {
    refetchQueries: [
      {
        query: queries.getEditorialBoard,
        variables: {
          journalId,
        },
      },
    ],
  })

  const handleRemoveEditorialMember = id => ({
    setFetching,
    setError,
    hideModal,
  }) => {
    setFetching(true)
    removeEditorialMember({
      variables: { teamMemberId: id },
    })
      .then(() => {
        setFetching(false)
        hideModal()
      })
      .catch(error => {
        setFetching(false)
        setError(error.message)
      })
  }

  return { handleRemoveEditorialMember }
}

export default useRemoveEditorialMember
