import { useState, useEffect } from 'react'
import { useLazyQuery } from 'react-apollo'
import { get } from 'lodash'
import { getUsersForAdminPanel } from '../graphql/queries'

const useUserPagination = itemsPerPage => {
  const [searchValue, setSearchValue] = useState('')
  const [page, setPage] = useState(0)

  const [getUsers, { data, loading, refetch }] = useLazyQuery(
    getUsersForAdminPanel,
    { fetchPolicy: 'network-only' },
  )

  useEffect(() => {
    getUsers({
      variables: { page, pageSize: itemsPerPage, searchValue },
    })
  }, [getUsers, itemsPerPage, page, searchValue])

  const totalUsers = get(data, 'getUsersForAdminPanel.totalCount', 0)
  const users = get(data, 'getUsersForAdminPanel.users', []).map(user => ({
    id: get(user, 'id'),
    givenNames: get(user, 'name.givenNames', ''),
    surname: get(user, 'name.surname', ''),
    aff: get(user, 'aff', ''),
    email: get(user, 'email', ''),
    isActive: get(user, 'isActive', false),
    isConfirmed: get(user, 'isConfirmed', false),
  }))

  const toFirst = () => {
    setPage(0)
  }

  const toLast = () => {
    const floor = Math.floor(totalUsers / itemsPerPage)
    setPage(totalUsers % itemsPerPage ? floor : floor - 1)
  }

  const nextPage = () => {
    setPage(page =>
      page * itemsPerPage + itemsPerPage < totalUsers ? page + 1 : page,
    )
  }

  const prevPage = () => {
    setPage(page => Math.max(0, page - 1))
  }

  return {
    users,
    totalUsers,
    loading,
    page,
    setPage,
    toLast,
    toFirst,
    prevPage,
    nextPage,
    refetchPage: refetch,
    searchValue,
    setSearchValue,
  }
}

export default useUserPagination
