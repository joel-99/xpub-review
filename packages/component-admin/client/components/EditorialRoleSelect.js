import React from 'react'
import { useLazyQuery } from 'react-apollo'
import { get, debounce } from 'lodash'
import { SearchableSelect } from '@hindawi/ui'

import { getUsersForEditorialAssignment } from '../graphql/queries'

const getName = identity =>
  `${get(identity, 'name.givenNames', '')} ${get(identity, 'name.surname', '')}`

const EditorialRoleSelect = ({
  disabled,
  onChange,
  value,
  validationStatus,
}) => {
  const handleChange = option => {
    onChange(option ? `${option.value}` : '')
  }
  const [
    getUsersForEditorialAssignmentMutation,
    { data, loading },
  ] = useLazyQuery(getUsersForEditorialAssignment)
  const editorsList = get(data, 'getUsersForEditorialAssignment', []).map(
    ({ id, identities }) => ({
      value: id,
      label: getName(identities[0]),
      sublabel: get(identities, '0.email', ''),
    }),
  )

  const onInputValueChange = value => {
    if (value) {
      getUsersForEditorialAssignmentMutation({ variables: { input: value } })
    }
  }
  const debouncedOnInputValueChange = debounce(onInputValueChange, 1000)

  return (
    <SearchableSelect
      disabled={disabled}
      isLoading={loading}
      onChange={handleChange}
      onInputValueChange={debouncedOnInputValueChange}
      options={editorsList}
      placeholder="Name or Email"
      validationStatus={validationStatus}
      value={value}
    />
  )
}

export default EditorialRoleSelect
