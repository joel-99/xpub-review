const initialize = ({ Team, eventsService }) => ({
  execute: async ({ user, id, input }) => {
    const ripeMember = user.getTeamMemberByRole(
      Team.Role.researchIntegrityPublishingEditor,
    )

    if (!input.isRIPE && ripeMember) {
      await ripeMember.delete()
    }

    if (input.isRIPE && !ripeMember) {
      const queryObject = {
        role: Team.Role.researchIntegrityPublishingEditor,
        manuscriptId: null,
        journalId: null,
        sectionId: null,
        specialIssueId: null,
      }
      const ripeTeam = await Team.findOrCreate({
        queryObject,
        eagerLoadRelations: 'members.[user.[identities]]',
        options: queryObject,
      })

      const newMember = ripeTeam.addMember(user, {
        userId: user.id,
        teamId: ripeTeam.id,
      })
      await newMember.save()

      eventsService.publishUserEvent({
        userId: user.id,
        eventName: 'UserUpdated',
      })
    }
  },
})

module.exports = {
  initialize,
}
