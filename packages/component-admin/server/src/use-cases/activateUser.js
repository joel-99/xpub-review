const initialize = ({ models: { User }, eventsService }) => ({
  execute: async id => {
    const user = await User.find(id)
    if (user.isActive) return

    user.updateProperties({ isActive: true })
    await user.save()

    eventsService.publishUserEvent({
      userId: user.id,
      eventName: 'UserActivated',
    })

    return User.findOneWithDefaultIdentity(user.id)
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
