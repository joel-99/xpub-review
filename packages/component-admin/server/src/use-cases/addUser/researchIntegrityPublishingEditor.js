const initialize = ({ models: { Team }, eventsService }) => ({
  execute: async ({ user }) => {
    const queryObject = {
      role: Team.Role.researchIntegrityPublishingEditor,
      manuscriptId: null,
      journalId: null,
      sectionId: null,
      specialIssueId: null,
    }
    const ripeTeam = await Team.findOrCreate({
      queryObject,
      eagerLoadRelations: 'members',
      options: queryObject,
    })

    const newRIPE = ripeTeam.addMember(user, { userId: user.id })
    await newRIPE.save()
    await ripeTeam.save()

    eventsService.publishUserEvent({
      userId: user.id,
      eventName: 'UserAdded',
    })
  },
})

module.exports = {
  initialize,
}
