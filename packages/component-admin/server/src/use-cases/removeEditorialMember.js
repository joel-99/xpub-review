const initialize = ({ models: { TeamMember }, eventsService }) => ({
  execute: async teamMemberId => {
    const teamMember = await TeamMember.find(teamMemberId)
    if (!teamMember) {
      throw new Error('Cannot remove editor.')
    }
    await teamMember.delete()
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
