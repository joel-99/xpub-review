const initialize = ({ Journal }) => ({
  execute: async () => {
    const journals = await Journal.findAll({
      orderByField: 'created',
      order: 'desc',
      eagerLoadRelations: '[journalArticleTypes.articleType, peerReviewModel]',
    })
    return journals.map(journal => journal.toDTO())
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
