const initialize = ({
  Team,
  TeamMember,
  SpecialIssue,
  Section,
  eventsService,
}) => ({
  execute: async ({ user, role, specialIssueId }) => {
    const existingUserSpecialIssueMember = await TeamMember.findOneBySpecialIssueAndUser(
      { userId: user.id, specialIssueId },
    )
    if (existingUserSpecialIssueMember) {
      throw new ValidationError(
        `User already has an editorial role on this special issue.`,
      )
    }
    const queryObject = {
      specialIssueId,
      role,
    }
    const team = await Team.findOrCreate({
      queryObject,
      options: queryObject,
      eagerLoadRelations: 'members',
    })

    const teamMember = team.addMember(user, {
      teamId: team.id,
      userId: user.id,
    })
    await teamMember.save()
    await team.save()

    const eventObject = { journalId: '', eventName: '' }
    const specialIssue = await SpecialIssue.find(specialIssueId)

    if (specialIssue.sectionId) {
      const section = await Section.find(specialIssue.sectionId)
      eventObject.journalId = section.journalId
      eventObject.eventName = 'JournalSectionSpecialIssueEditorAssigned'
    } else {
      eventObject.journalId = specialIssue.journalId
      eventObject.eventName = 'JournalSpecialIssueEditorAssigned'
    }
    eventsService.publishJournalEvent(eventObject)
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
