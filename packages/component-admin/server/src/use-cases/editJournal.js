const { isDateToday } = require('../dateService/dateService')
const setJournalProps = require('./setJournalProps')

const initialize = ({
  models: { User, Journal, Team, JournalArticleType, Job, TeamMember },
  jobsService,
  eventsService,
}) => ({
  execute: async ({ input, id, reqUserId }) => {
    const journal = await Journal.findOneBy({
      queryObject: { id },
      eagerLoadRelations: ['jobs', 'journalArticleTypes'],
    })
    const { articleTypes } = input
    delete input.articleTypes

    if (journal.isActive) {
      delete input.activationDate
      journal.updateProperties(input)
      await journal.save()
    } else {
      const user = await User.find(reqUserId, 'teamMemberships.team')
      const admin = user.teamMemberships.find(
        tm => tm.team.role === Team.Role.admin,
      )
      await setJournalProps.handleActivationDate({
        Job,
        input,
        journal,
        jobsService,
        isDateToday,
        adminId: admin.id,
      })
    }

    await setJournalProps.setJournalArticleTypes({
      journal,
      articleTypes,
      JournalArticleType,
    })

    eventsService.publishJournalEvent({
      journalId: id,
      eventName: 'JournalUpdated',
    })
  },
})

const authsomePolicies = ['admin']
module.exports = {
  initialize,
  authsomePolicies,
}
