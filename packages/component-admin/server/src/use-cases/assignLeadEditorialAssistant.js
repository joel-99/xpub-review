const initialize = ({ models: { Journal, TeamMember } }) => ({
  execute: async ({ id }) => {
    const journal = await Journal.findJournalByTeamMember(id)

    const correspondingEditorialAssistant = await TeamMember.findCorrespondingEditorialAssistant(
      journal.id,
    )
    if (correspondingEditorialAssistant) {
      correspondingEditorialAssistant.updateProperties({
        isCorresponding: false,
      })
      await correspondingEditorialAssistant.save()
    }

    const editorialAssistant = await TeamMember.find(id)
    editorialAssistant.updateProperties({
      isCorresponding: true,
    })
    await editorialAssistant.save()
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
