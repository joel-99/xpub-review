const initialize = ({ Team, TeamMember, eventsService }) => ({
  execute: async ({ user, journalId, role }) => {
    const existingUserJournalMember = await TeamMember.findOneByJournalAndUser({
      userId: user.id,
      journalId,
    })
    if (existingUserJournalMember) {
      throw new ValidationError(
        `User already has an editorial role on this journal.`,
      )
    }

    const queryObject = {
      journalId,
      role,
    }

    const team = await Team.findOrCreate({
      queryObject,
      options: queryObject,
      eagerLoadRelations: 'members',
    })

    let isCorresponding
    if (role === Team.Role.editorialAssistant) {
      const correspondingEditorialAssistant = await TeamMember.findCorrespondingEditorialAssistant(
        journalId,
      )
      if (!correspondingEditorialAssistant) isCorresponding = true
      else isCorresponding = false
    }

    const teamMember = team.addMember(user, {
      teamId: team.id,
      userId: user.id,
      isCorresponding,
    })
    await teamMember.save()
    await team.save()

    eventsService.publishJournalEvent({
      journalId,
      eventName: 'JournalEditorAssigned',
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
