const { Promise } = require('bluebird')
const { isNil } = require('lodash')

module.exports = {
  async setJournalArticleTypes({ articleTypes, JournalArticleType, journal }) {
    await Promise.each(journal.journalArticleTypes, async jat => jat.delete())
    await Promise.each(articleTypes, async articleType => {
      const journalArticleType = new JournalArticleType({
        articleTypeId: articleType,
        journalId: journal.id,
      })
      await journalArticleType.save()
    })
  },
  async handleActivationDate({
    Job,
    input,
    journal,
    adminId,
    jobsService,
    isDateToday,
  }) {
    if (
      (isNil(input.activationDate) && isNil(journal.activationDate)) ||
      input.activationDate === new Date(journal.activationDate).toISOString()
    ) {
      journal.updateProperties({
        ...input,
      })
      await journal.save()

      return
    }

    await Promise.each(journal.jobs, async job => {
      await Job.cancel(job.id)

      return job.delete()
    })

    if (!input.activationDate && journal.activationDate) {
      delete input.activationDate
      journal.updateProperties({ ...input, activationDate: null })
      await journal.save()

      return
    }

    journal.updateProperties({
      activationDate: input.activationDate,
      ...input,
    })
    await journal.save()

    if (isDateToday(input.activationDate)) {
      journal.updateProperties({
        isActive: true,
      })
      await journal.save()

      return
    }

    const job = await jobsService.scheduleJournalActivation({
      journal,
      teamMemberId: adminId,
    })
    journal.jobs.push(job)
  },
}
