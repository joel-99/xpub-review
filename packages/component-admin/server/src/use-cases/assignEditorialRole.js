const initialize = ({
  models: { Team, TeamMember, User, SpecialIssue, Section },
  eventsService,
  useCases,
}) => ({
  execute: async ({ input }) => {
    const { userId, role, journalId, sectionId, specialIssueId } = input

    const user = await User.find(userId, 'identities')
    let useCase = 'assignEditorialRoleOnJournalUseCase'
    if (specialIssueId) {
      useCase = 'assignEditorialRoleOnSpecialIssueUseCase'
    }
    if (sectionId) {
      useCase = 'assignEditorialRoleOnSectionUseCase'
    }

    await useCases[useCase]
      .initialize({ Team, TeamMember, SpecialIssue, Section, eventsService })
      .execute({ user, role, journalId, sectionId, specialIssueId })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
