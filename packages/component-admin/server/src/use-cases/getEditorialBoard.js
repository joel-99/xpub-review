const initialize = ({ Journal, Section, Team, SpecialIssue }) => ({
  execute: async journalId => {
    const journal = await Journal.find(journalId)

    const teamReducer = (acc, team) => {
      const members = team.members.map(member => ({
        id: member.id,
        role: team.role,
        sectionName: team.sectionName,
        specialIssueName: team.specialIssueName,
        email: member.alias.email,
        isCorresponding: member.isCorresponding,
        fullName: {
          surname: member.alias.surname,
          givenNames: member.alias.givenNames,
          title: member.alias.title,
        },
      }))

      return [...acc, ...members]
    }

    return (await Team.findAllByJournal(journal.id))
      .filter(
        team =>
          [
            Team.Role.triageEditor,
            Team.Role.academicEditor,
            Team.Role.editorialAssistant,
          ].includes(team.role) && !team.manuscriptId,
      )
      .reduce(teamReducer, [])
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
